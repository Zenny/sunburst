use serde::{Deserialize, Serialize};
use std::io::{Read, Seek, SeekFrom, Write};

use crate::{
    bytes::read_u8,
    gc::{
        bmd::error::BmdWriteError,
        error::ReadError,
        types::{
            indirect::{IndirectAlpha, IndirectBias, IndirectFormat, IndirectMatrix, IndirectWrap},
            tev::TevStageId,
        },
    },
    try_from_repr, w, wbool, wraw,
};

#[derive(Debug, Copy, Clone, Default, Serialize, Deserialize)]
pub struct IndirectTevStage {
    pub stage: TevStageId,
    pub tex_format: IndirectFormat,
    pub tex_bias_sel: IndirectBias,
    pub tex_mat_id: IndirectMatrix,
    pub tex_wrap_s: IndirectWrap,
    pub tex_wrap_t: IndirectWrap,
    pub add_prev: bool,
    pub utc_lod: bool,
    pub tex_alpha_sel: IndirectAlpha,
}

impl IndirectTevStage {
    /// Advances position
    pub fn read<R: Read + Seek>(buf: &mut R) -> Result<Self, ReadError> {
        let stage = read_u8(buf)?;
        let tex_format = read_u8(buf)?;
        let tex_bias = read_u8(buf)?;
        let tex_mat = read_u8(buf)?;
        let tex_wrap_s = read_u8(buf)?;
        let tex_wrap_t = read_u8(buf)?;
        let add_prev = read_u8(buf)? != 0;
        let utc_lod = read_u8(buf)? != 0;
        let tex_alpha = read_u8(buf)?;

        // Skip padding (probably byte + short)
        buf.seek(SeekFrom::Current(3))?;

        Ok(Self {
            stage: try_from_repr!(stage => TevStageId)?,
            tex_format: try_from_repr!(tex_format => IndirectFormat)?,
            tex_bias_sel: try_from_repr!(tex_bias => IndirectBias)?,
            tex_mat_id: try_from_repr!(tex_mat => IndirectMatrix)?,
            tex_wrap_s: try_from_repr!(tex_wrap_s => IndirectWrap)?,
            tex_wrap_t: try_from_repr!(tex_wrap_t => IndirectWrap)?,
            add_prev,
            utc_lod,
            tex_alpha_sel: try_from_repr!(tex_alpha => IndirectAlpha)?,
        })
    }

    pub fn write_bmd<W: Write + Seek>(&self, w: &mut W) -> Result<(), BmdWriteError> {
        w!(w, self.stage as u8)?;
        w!(w, self.tex_format as u8)?;
        w!(w, self.tex_bias_sel as u8)?;
        w!(w, self.tex_mat_id as u8)?;
        w!(w, self.tex_wrap_s as u8)?;
        w!(w, self.tex_wrap_t as u8)?;
        wbool!(w, self.add_prev)?;
        wbool!(w, self.utc_lod)?;
        w!(w, self.tex_alpha_sel as u8)?;

        wraw!(w, [0xFF, 0xFF, 0xFF])?;

        Ok(())
    }
}
