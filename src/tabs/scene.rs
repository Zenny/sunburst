use bevy_egui::egui::{Color32, Ui};
use egui_dock::TabStyle;

use crate::tabs::Tab;

pub const TITLE: &str = "Scene";

#[derive(Default, Debug, Clone, PartialEq)]
pub struct Scene;

impl Scene {
    pub fn ui(&mut self, ui: &mut Ui, res: Option<f64>) {
        if let Some(res) = res {
            ui.label(format!("FPS: {:.0}", 1.0 / res));
        }
    }
}

impl<'a> Tab for Scene {
    fn title(&self) -> &'static str {
        TITLE
    }

    fn style_override(&self, global_style: &TabStyle) -> Option<TabStyle> {
        let mut style = global_style.clone();
        style.bg_fill = Color32::TRANSPARENT;
        Some(style)
    }
}
