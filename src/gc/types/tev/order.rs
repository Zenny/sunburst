use std::io::{Read, Seek, SeekFrom, Write};

use serde::{Deserialize, Serialize};

use crate::{
    bytes::read_u8,
    gc::{
        bmd::error::BmdWriteError,
        error::ReadError,
        types::tex::{ColorChannelId, TexCoordId, TexMapId},
    },
    try_from_repr, w,
};

/// GXSetTevOrder(GXTevStageID stage, GXTexCoordID coord, GXTexMapID map, GXChannelID color)
/// Determines which texture and rasterize color inputs are available to each TEV
/// stage. GXSetTevColorIn/GXSetTevAlphaIn control how the inputs plug into each
/// TEV operation for each stage.
#[derive(Debug, Copy, Clone, Default, PartialEq, Serialize, Deserialize)]
pub struct TevOrder {
    pub tex_coord: TexCoordId,
    pub tex_map: TexMapId,
    pub channel_id: ColorChannelId,
}

impl TevOrder {
    /// Advances position
    pub fn read<R: Read + Seek>(buf: &mut R) -> Result<Self, ReadError> {
        let coord = read_u8(buf)?;
        let map = read_u8(buf)?;
        let chan = read_u8(buf)?;

        // Skip padding
        buf.seek(SeekFrom::Current(1))?;

        Ok(Self {
            tex_coord: try_from_repr!(coord => TexCoordId)?,
            tex_map: try_from_repr!(map => TexMapId)?,
            channel_id: try_from_repr!(chan => ColorChannelId)?,
        })
    }

    pub fn write_bmd<W: Write + Seek>(&self, w: &mut W) -> Result<(), BmdWriteError> {
        w!(w, self.tex_coord as u8)?;
        w!(w, self.tex_map as u8)?;
        w!(w, self.channel_id as u8)?;

        w!(w, 0xFFu8)?;

        Ok(())
    }
}
