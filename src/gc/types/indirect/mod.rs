//! Indirect TEV related types

use num_enum::TryFromPrimitive;
use serde::{Deserialize, Serialize};

pub mod tev_order;
pub mod tev_stage;
pub mod tex_info;
pub mod tex_matrix;
pub mod tex_scale;

#[derive(Debug, Copy, Clone, Default, TryFromPrimitive, Serialize, Deserialize)]
#[repr(u8)]
pub enum IndirectFormat {
    #[default]
    IF8 = 0, // 8 bit
    IF5 = 1, // 5 bit
    IF4 = 2, // 4 bit
    IF3 = 3, // 3 bit
}

#[derive(Debug, Copy, Clone, Default, TryFromPrimitive, Serialize, Deserialize)]
#[repr(u8)]
pub enum IndirectBias {
    #[default]
    None = 0,
    S = 1,
    T = 2,
    ST = 3,
    U = 4,
    SU = 5,
    TU = 6,
    STU = 7,
}

#[derive(Debug, Copy, Clone, Default, TryFromPrimitive, Serialize, Deserialize)]
#[repr(u8)]
pub enum IndirectAlpha {
    #[default]
    Off = 0,

    S = 1,
    T = 2,
    U = 3,
}

#[derive(Debug, Copy, Clone, Default, TryFromPrimitive, Serialize, Deserialize)]
#[repr(u8)]
pub enum IndirectMatrix {
    #[default]
    Off = 0,
    // todo are these ST?
    IM0 = 1,
    IM1 = 2,
    IM2 = 3,
    // 4 missing
    IMS0 = 5,
    IMS1 = 6,
    IMS2 = 7,
    // 8 missing
    IMT0 = 9,
    IMT1 = 10,
    IMT2 = 11,
}

#[derive(Debug, Copy, Clone, Default, TryFromPrimitive, Serialize, Deserialize)]
#[repr(u8)]
pub enum IndirectWrap {
    #[default]
    Off = 0,

    IW256 = 1,
    IW128 = 2,
    IW64 = 3,
    IW32 = 4,
    IW16 = 5,
    IW0 = 6,
}

#[derive(Debug, Copy, Clone, Default, TryFromPrimitive, Serialize, Deserialize)]
#[repr(u8)]
pub enum IndirectScale {
    #[default]
    IS1 = 0, // 1
    IS2 = 1,   // 1/2
    IS4 = 2,   // 1/4
    IS8 = 3,   // 1/8
    IS16 = 4,  // 1/16
    IS32 = 5,  // 1/32
    IS64 = 6,  // 1/64
    IS128 = 7, // 1/128
    IS256 = 8, // 1/256
}
