use displaydoc::Display;
use std::{io, path::PathBuf};
use thiserror::Error;

#[derive(Display, Error, Debug)]
pub enum CompanionError {
    /// Failed to convert to json: {0}
    SerdeJsonWrite(serde_json::Error),
    /// Failed to convert from json: {0}
    SerdeJsonRead(serde_json::Error),
    /// Failed to write file: {0}
    IoWriteError(io::Error),
    /// Failed to read file: {0}
    IoReadError(io::Error),
    /// File has invalid name: {0}
    InvalidFileName(PathBuf),
}
