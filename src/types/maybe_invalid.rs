use serde::{Deserialize, Serialize};
use std::fmt::{Debug, Formatter};

#[derive(Default, Serialize, Deserialize)]
pub enum MaybeInvalid<T> {
    Some(T),
    #[default]
    None,
    Invalid,
}

impl<T> MaybeInvalid<T> {
    pub fn none_as_invalid(value: Option<T>) -> Self {
        match value {
            None => Self::Invalid,
            Some(t) => Self::Some(t),
        }
    }

    pub fn is_some(&self) -> bool {
        match self {
            MaybeInvalid::Some(_) => true,
            _ => false,
        }
    }

    pub fn is_none(&self) -> bool {
        match self {
            MaybeInvalid::None => true,
            _ => false,
        }
    }

    pub fn is_invalid(&self) -> bool {
        match self {
            MaybeInvalid::Invalid => true,
            _ => false,
        }
    }
}

impl<T> Debug for MaybeInvalid<T>
where
    T: Debug,
{
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            MaybeInvalid::Some(t) => write!(f, "Some({:?})", t),
            MaybeInvalid::None => write!(f, "None"),
            MaybeInvalid::Invalid => write!(f, "Invalid"),
        }
    }
}

impl<T> Clone for MaybeInvalid<T>
where
    T: Clone,
{
    fn clone(&self) -> Self {
        match self {
            MaybeInvalid::Some(t) => Self::Some(t.clone()),
            MaybeInvalid::None => Self::None,
            MaybeInvalid::Invalid => Self::Invalid,
        }
    }
}

impl<T> Copy for MaybeInvalid<T> where T: Copy {}

impl<T> PartialEq for MaybeInvalid<T>
where
    T: PartialEq,
{
    fn eq(&self, other: &Self) -> bool {
        match self {
            MaybeInvalid::Some(t) => {
                if let Self::Some(o) = other {
                    t == o
                } else {
                    false
                }
            }
            MaybeInvalid::None => other.is_none(),
            MaybeInvalid::Invalid => other.is_invalid(),
        }
    }
}
