use std::io::{Read, Seek, SeekFrom, Write};

use num_enum::TryFromPrimitive;

use crate::{
    bytes::{read_u16, read_u32, read_u8},
    gc::{
        align::{write_align, AlignType},
        bmd::error::{BmdWriteError, GltfReadError},
        error::ReadError,
    },
    w, wat,
};

const DRW1: u32 = 0x44525731;

#[derive(Debug, Clone, PartialEq)]
pub struct MatrixEntry {
    pub kind: WeightKind,
    pub ind: u16,
}

#[derive(Debug, Copy, Clone, PartialEq, TryFromPrimitive)]
#[repr(u8)]
pub enum WeightKind {
    Bone = 0,
    Envelope = 1,
}

#[derive(Debug, Clone)]
pub struct Drw1 {
    pub len: u32,
    pub matrix_entries: Vec<MatrixEntry>,
}

impl Drw1 {
    pub fn new<R: Read + Seek>(buf: &mut R, drw1_offset: u32) -> Result<Self, ReadError> {
        let drw1 = read_u32(buf)?;
        // DRW1
        if drw1 != DRW1 {
            return Err(ReadError::ReadCheck("DRW1"));
        }

        let drw1_len = read_u32(buf)?;
        // num of envelope matrices
        let data_count = read_u16(buf)? as usize;

        // Skip padding
        buf.seek(SeekFrom::Current(2))?;

        let mut matrix_entries = Vec::with_capacity(data_count);
        let mut tmp_kinds = Vec::with_capacity(data_count);

        let kind_offset = read_u32(buf)?;
        let ind_offset = read_u32(buf)?;

        buf.seek(SeekFrom::Start((kind_offset + drw1_offset) as u64))?;

        for _ in 0..data_count {
            let kind = read_u8(buf)?;
            tmp_kinds.push(
                WeightKind::try_from_primitive(kind)
                    .map_err(|_| ReadError::InvalidWeightKind(kind))?,
            );
        }

        buf.seek(SeekFrom::Start((ind_offset + drw1_offset) as u64))?;

        for kind in tmp_kinds.into_iter() {
            matrix_entries.push(MatrixEntry {
                kind,
                ind: read_u16(buf)?,
            })
        }

        Ok(Self {
            len: drw1_len,
            matrix_entries,
        })
    }

    pub fn write_bmd<W: Write + Seek>(&self, w: &mut W) -> Result<(), BmdWriteError> {
        let start = w.stream_position()?;
        w!(w, DRW1)?;

        let length_offset = w.stream_position()?;
        w!(w, 0u32)?;

        w!(w, self.matrix_entries.len() as u16)?;
        // Padding - Doesn't seem to be an alignment?
        w!(w, 0xFFFFu16)?;

        let kind_offset = w.stream_position()?;
        w!(w, 0u32)?;

        let ind_offset = w.stream_position()?;
        w!(w, 0u32)?;

        let here = w.stream_position()?;
        wat!(w, (here - start) as u32, kind_offset);
        for mat in self.matrix_entries.iter() {
            w!(w, mat.kind as u8)?;
        }

        // todo half white ball has padding here, does 1 T, only other padding that makes sense is
        //   17. I think I fixed this but make sure
        write_align(w, 2, AlignType::String)?;

        let here = w.stream_position()?;
        wat!(w, (here - start) as u32, ind_offset);
        for mat in self.matrix_entries.iter() {
            w!(w, mat.ind)?;
        }

        write_align(w, 32, AlignType::String)?;

        let len = (w.stream_position()? - start) as u32;
        wat!(w, len, length_offset);

        Ok(())
    }

    /// Returns an ind into drw1.matrix_entries.
    /// The `ind` param should point to evp1.matrices (bone ind) for WeightKind::Bone, or
    /// evp1.weights for WeightKind::Envelope
    pub fn insert_matrix(&mut self, kind: WeightKind, ind: u16) -> Result<usize, GltfReadError> {
        let new_matrix = MatrixEntry { kind, ind };

        Ok(
            if let Some(pos) = self.matrix_entries.iter().position(|m| m == &new_matrix) {
                pos
            } else {
                let ind = self.matrix_entries.len();
                self.matrix_entries.push(new_matrix);
                ind
            },
        )
    }
}
