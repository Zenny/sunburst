use std::{
    collections::HashMap,
    ffi::OsString,
    fs::{copy, create_dir_all, read, read_dir, write, Metadata},
    io::{BufReader, Cursor},
    path::PathBuf,
};

use xxhash_rust::xxh3::xxh3_128;

use crate::companion::generic::GenericCompanion;
use crate::gc::error::IsoWriteError;
use crate::gc::iso::IsoWorker;
use crate::{
    companion::{bmd::BmdCompanion, bti::BtiCompanion, extend_extension, COMPANION_EXT},
    error::BuildError,
    gc::{bmd::Bmd, bti::Bti},
    PROJECT_FILE,
};

pub struct Project {
    pub proj_dir: PathBuf,
}

impl Project {
    pub fn new(proj_dir: PathBuf) -> Self {
        Self { proj_dir }
    }

    pub fn build(&self) -> Result<(), BuildError> {
        let build_dir = self.proj_dir.join("_build");
        let frag_dir = build_dir.join("frag");
        create_dir_all(&frag_dir)
            .map_err(|e| BuildError::IoError(frag_dir.clone(), self.proj_dir.clone(), e))?;

        let mut builder = Builder::new();
        builder.recur_build(self.proj_dir.clone(), frag_dir)?;
        builder.build_iso(self.proj_dir.clone(), build_dir)?;

        Ok(())
    }
}

struct Builder {
    cache: HashMap<u128, Vec<u8>>,
}

impl Builder {
    pub fn new() -> Self {
        Self {
            cache: HashMap::with_capacity(512),
        }
    }

    pub fn build_iso(
        &mut self,
        proj_dir: PathBuf,
        build_dir: PathBuf,
    ) -> Result<(), IsoWriteError> {
        let mut iso_data = Vec::with_capacity(1610612736);
        let mut iso_cur = Cursor::new(&mut iso_data);
        IsoWorker::write(&mut iso_cur, proj_dir)?;
        let iso = build_dir.join("game.iso");
        log::info!("Writing ISO file...");
        write(iso, iso_data)?;
        log::info!("Write complete");
        Ok(())
    }

    /// build_dir mirrors cur_dir
    pub fn recur_build(&mut self, cur_dir: PathBuf, build_dir: PathBuf) -> Result<(), BuildError> {
        for file in read_dir(&cur_dir)
            .map_err(|e| BuildError::IoError(cur_dir.clone(), build_dir.clone(), e))?
        {
            let file =
                file.map_err(|e| BuildError::IoError(cur_dir.clone(), build_dir.clone(), e))?;
            let path = file.path();
            let file_name = path.file_name().unwrap();
            let lossy_name = file_name.to_string_lossy();

            // Skip files and folders that skip with _ and . and the project file and meta files
            if lossy_name.starts_with("_")
                || lossy_name.starts_with(".")
                || lossy_name == PROJECT_FILE
                || lossy_name.ends_with(COMPANION_EXT)
            {
                continue;
            }

            log::debug!("Recursive build {:?}", path);

            let build_dir_equiv = map_build_extension(build_dir.join(&file_name));

            if path.is_dir() {
                if path.extension().is_some_and(|e| e == "szs") {
                    // We need to forward the szs companion data to the 2nd step for when they
                    // get built
                    let companion_name = format!("{}{}", lossy_name, COMPANION_EXT);
                    let companion_src = path.parent().unwrap().join(&companion_name);
                    let companion_dst = build_dir_equiv.parent().unwrap().join(&companion_name);
                    copy(companion_src.clone(), companion_dst.clone())
                        .map_err(|e| BuildError::IoError(companion_src, companion_dst, e))?;
                }
                create_dir_all(&build_dir_equiv)
                    .map_err(|e| BuildError::IoError(path.clone(), build_dir_equiv.clone(), e))?;
                self.recur_build(path, build_dir_equiv)?;
            } else {
                let meta = file
                    .metadata()
                    .map_err(|e| BuildError::IoError(path.clone(), build_dir_equiv.clone(), e))?;
                if build_dir_equiv.exists() {
                    let build_meta = build_dir_equiv.metadata().map_err(|e| {
                        BuildError::IoError(build_dir_equiv.clone(), path.clone(), e)
                    })?;
                    // If something was modified more recently
                    if build_meta.modified().map_err(|e| {
                        BuildError::IoError(build_dir_equiv.clone(), path.clone(), e)
                    })? < meta.modified().map_err(|e| {
                        BuildError::IoError(path.clone(), build_dir_equiv.clone(), e)
                    })? {
                        self.build(path, &build_dir_equiv, meta)?;
                    } else {
                        log::debug!("Up to date, skipping");
                    }
                } else {
                    self.build(path, &build_dir_equiv, meta)?;
                }
            }
        }

        Ok(())
    }

    fn build(
        &mut self,
        file_path: PathBuf,
        build_dir_equiv: &PathBuf,
        file_meta: Metadata,
    ) -> Result<(), BuildError> {
        let data = read(&file_path)
            .map_err(|e| BuildError::IoError(file_path.clone(), build_dir_equiv.clone(), e))?;

        let ext = file_path.extension().map(|v| v.to_ascii_lowercase());
        match ext.unwrap_or_else(|| OsString::new()).to_str().unwrap() {
            "gltf" => self.build_gltf(file_path, build_dir_equiv, file_meta, data)?,
            "png" => self.build_png(file_path, build_dir_equiv, data)?,
            _unimplemented => {
                log::info!("Compiling {:?}...", file_path);

                let meta_file = extend_extension(&file_path, COMPANION_EXT);
                if meta_file.exists() {
                    // We need the metadata file for rarc flags later if it's there
                    let meta_build = extend_extension(&build_dir_equiv, COMPANION_EXT);
                    copy(&meta_file, &meta_build).map_err(|e| {
                        BuildError::IoError(meta_file.clone(), meta_build.clone(), e)
                    })?;
                }

                // todo make a list of all possible unimplemented extensions to avoid random copies
                copy(&file_path, &build_dir_equiv)
                    .map_err(|e| BuildError::IoError(file_path, build_dir_equiv.clone(), e))?;
            }
        }

        Ok(())
    }

    fn build_png(
        &mut self,
        file_path: PathBuf,
        build_dir_equiv: &PathBuf,
        data: Vec<u8>,
    ) -> Result<(), BuildError> {
        let hash = xxh3_128(&data);
        if let Some(built_data) = self.cache.get(&hash) {
            let companion = BtiCompanion::read(&file_path)?;

            if let Some(data) = &companion.rarc_data {
                GenericCompanion::new(Some(data.clone())).write(&build_dir_equiv)?;
            }

            let build_path = build_dir_equiv.with_extension("bti");

            write(&build_path, built_data)
                .map_err(|e| BuildError::IoError(build_path.clone(), file_path.clone(), e))
        } else {
            let cursor = Cursor::new(data);
            let buf_data = BufReader::new(cursor);
            let bti = Bti::read_png(file_path.clone(), buf_data)?;

            if let Some(data) = &bti.rarc_data {
                GenericCompanion::new(Some(data.clone())).write(&build_dir_equiv)?;
            }
            let mut bti_vec = Vec::with_capacity(bti.predicted_bti_size());
            let mut cursor = Cursor::new(&mut bti_vec);

            bti.write_bti(&mut cursor)?;

            // cache the data for reuse
            self.cache.insert(hash, bti_vec.clone());

            let build_path = build_dir_equiv.with_extension("bti");

            write(&build_path, bti_vec)
                .map_err(|e| BuildError::IoError(build_path.clone(), file_path.clone(), e))
        }
    }

    fn build_gltf(
        &mut self,
        file_path: PathBuf,
        build_dir_equiv: &PathBuf,
        file_meta: Metadata,
        data: Vec<u8>,
    ) -> Result<(), BuildError> {
        let hash = xxh3_128(&data);
        if let Some(built_data) = self.cache.get(&hash) {
            let companion = BmdCompanion::read(&file_path)?;

            if let Some(data) = &companion.rarc_data {
                GenericCompanion::new(Some(data.clone())).write(&build_dir_equiv)?;
            }

            let build_path = build_dir_equiv.with_extension("bmd");

            write(&build_path, built_data)
                .map_err(|e| BuildError::IoError(build_path.clone(), file_path.clone(), e))
        } else {
            let cursor = Cursor::new(data);
            let buf_data = BufReader::new(cursor);
            let bmd = Bmd::read_gltf(file_path.clone(), buf_data)?;

            if let Some(data) = &bmd.rarc_data {
                GenericCompanion::new(Some(data.clone())).write(&build_dir_equiv)?;
            }

            let mut bmd_vec = Vec::with_capacity(file_meta.len() as usize);
            let mut cursor = Cursor::new(&mut bmd_vec);
            bmd.write_bmd(&mut cursor)?;

            // cache the data for reuse
            self.cache.insert(hash, bmd_vec.clone());

            let build_path = build_dir_equiv.with_extension("bmd");

            write(&build_path, bmd_vec)
                .map_err(|e| BuildError::IoError(build_path.clone(), file_path.clone(), e))
        }
    }
}

fn map_build_extension(mut path: PathBuf) -> PathBuf {
    let ext = path.extension().map(|v| v.to_ascii_lowercase());
    match ext.unwrap_or_else(|| OsString::new()).to_str().unwrap() {
        "gltf" => {
            path.set_extension("bmd");
        }
        "png" => {
            path.set_extension("bti");
        }
        _unimplemented => {}
    }
    path
}
