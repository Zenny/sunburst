use crate::{companion, companion::rarc_entry::RarcEntryCompanion};

companion!(GenericCompanion {
    #[serde(skip_serializing_if = "Option::is_none", default)]
    rarc_data: Option<RarcEntryCompanion>
});
