use std::{
    collections::HashSet,
    io::{BufRead, Read, Seek, SeekFrom, Write},
    path::PathBuf,
};

use bevy::utils::HashMap;

use crate::{
    bytes::{read_u16, read_u32},
    gc::{
        align::{write_align, AlignType},
        bmd::{
            error::BmdWriteError,
            jnt1::{read_name_table, write_name_table},
        },
        bti::Bti,
        error::ReadError,
    },
    w, wat, wraw,
};

const TEX1: u32 = 0x54455831;

#[derive(Debug, Clone)]
pub struct Tex1 {
    pub len: u32,
    pub texs: Vec<Bti>,
}

impl Tex1 {
    pub fn new<R: Read + Seek + BufRead>(buf: &mut R, tex1_offset: u32) -> Result<Self, ReadError> {
        let tex1 = read_u32(buf)?;
        // TEX1
        if tex1 != TEX1 {
            return Err(ReadError::ReadCheck("TEX1"));
        }

        let tex1_len = read_u32(buf)?;
        let tex_count = read_u16(buf)? as usize;

        // Skip padding
        buf.seek(SeekFrom::Current(2))?;

        // relative to tex1 header start
        let entry_offset = read_u32(buf)?;
        let name_offset = read_u32(buf)?;
        let names = read_name_table(buf, (tex1_offset + name_offset) as u64)?;

        if names.len() != tex_count {
            panic!("Names are weird len {}/{}", names.len(), tex_count);
        }

        let mut texs = Vec::with_capacity(tex_count);

        for i in 0..tex_count {
            let header_offset = (tex1_offset + entry_offset) as u64 + (0x20 * i as u64);
            let mut name_path = PathBuf::from(&names[i]);
            name_path.set_extension("bti");
            // Embedded textures don't have rarc flags
            let tex = Bti::new(name_path, buf, header_offset, None)?;
            texs.push(tex);
        }

        Ok(Self {
            len: tex1_len,
            texs,
        })
    }

    pub fn write_bmd<W: Write + Seek>(&self, w: &mut W) -> Result<(), BmdWriteError> {
        let start = w.stream_position()?;
        w!(w, TEX1)?;

        let length_offset = w.stream_position()?;
        w!(w, 0u32)?;

        w!(w, self.texs.len() as u16)?;

        w!(w, 0xFFFFu16)?;

        // BTI data - always 32
        w!(w, 32u32)?;

        let name_table_offset = w.stream_position()?;
        w!(w, 0u32)?;

        write_align(w, 32, AlignType::String)?;

        let mut header_map = HashMap::with_capacity(self.texs.len());
        let mut names = Vec::with_capacity(self.texs.len());

        // Do EVERY header, they might have the same image data but we want every header
        for bti in self.texs.iter() {
            if bti.palette_data.len() == 0 && bti.data.len() == 0 {
                continue;
            }

            let file_name = bti.path.file_name().expect("file name should never fail");

            let name_str = bti
                .path
                .with_extension("")
                .file_name()
                .expect("file name should never fail")
                .to_string_lossy()
                .to_string();
            names.push(name_str);

            let entry = header_map
                .entry(file_name)
                .or_insert_with(|| Vec::with_capacity(self.texs.len()));
            entry.push((w.stream_position()?, bti.header.write_bti(w)?));
        }

        let mut done = HashSet::with_capacity(self.texs.len());
        // Palettes
        for bti in self.texs.iter() {
            let file_name = bti.path.file_name().expect("file name should never fail");
            if done.contains(file_name) || !header_map.contains_key(file_name) {
                continue;
            }

            let here = w.stream_position()?;
            for (header_pos, offsets) in header_map
                .get(file_name)
                .expect("Somehow a header with a weird name appeared")
            {
                wat!(w, (here - header_pos) as u32, offsets.palette_offset);
            }

            // For some reason the initial implementation does not encapsulate the offset
            // calculation within this if (even though offset is useless when there's no data)
            if bti.palette_data.len() > 0 {
                wraw!(w, bti.palette_data)?;

                // todo investigate if padding is needed? superbmd has it
            }

            done.insert(file_name);
        }

        done.clear();
        // Image
        for bti in self.texs.iter() {
            let file_name = bti.path.file_name().expect("file name should never fail");
            if done.contains(file_name) || !header_map.contains_key(file_name) {
                continue;
            }

            let here = w.stream_position()?;
            for (header_pos, offsets) in header_map.get(file_name).expect("never fails") {
                wat!(w, (here - header_pos) as u32, offsets.tex_data_offset);
            }

            // For some reason the initial implementation does not encapsulate the offset
            // calculation within this if (even though offset is useless when there's no data)
            if bti.data.len() > 0 {
                wraw!(w, bti.data)?;
            }

            done.insert(file_name);
        }

        let here = w.stream_position()?;
        wat!(w, (here - start) as u32, name_table_offset);

        write_name_table(w, names)?;

        write_align(w, 32, AlignType::String)?;

        let len = (w.stream_position()? - start) as u32;
        wat!(w, len, length_offset);

        Ok(())
    }
}
