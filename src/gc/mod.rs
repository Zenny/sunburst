//! Gamecube is big endian

pub mod align;
pub mod bmd;
pub mod bti;
pub mod error;
pub mod file;
pub mod iso;
pub mod rarc;
pub mod types;
