//! Companion files (or meta files) are used generally as a kind of parameter file.
//! The adjustable parameters are then fed in during conversion from the modern file types
//! to the GCN filetypes when it may not normally be possible to discern the data from the
//! modern file type.
//!
//! A good example of this is BMD materials. The GCN's material system is massive, and
//! there's no way GLTF or even any modern 3D editor will support this (without a lot
//! of work). So we write them to these files as they don't really fit elsewhere.

use std::ffi::OsString;
use std::path::PathBuf;

pub mod bmd;
pub mod bti;
pub mod error;
pub mod generic;
pub mod rarc;
pub mod rarc_entry;

pub const COMPANION_EXT: &str = ".meta.json";

#[macro_export]
macro_rules! companion {
    ($name:ident { $($(#[$attr:meta])* $field:ident : $val:ty),+ }) => {
        use std::{
            fs::{read, write},
            io::Cursor,
            path::PathBuf
        };
        use serde::{Deserialize, Serialize};

        use crate::{
            companion::{
                extend_extension,
                COMPANION_EXT,
                error::{
                    CompanionError::{
                        self,
                        *
                    }
                }
            }
        };

        #[derive(Debug, Clone, Serialize, Deserialize)]
        pub struct $name {
            $(
                $(#[$attr])*
                pub $field: $val
            ),+
        }

        impl $name {
            pub fn new($($field: $val),+) -> Self {
                Self {
                    $(
                        $field
                    ),+
                }
            }

            pub fn read(parent_file: &PathBuf) -> Result<Self, CompanionError> {
                let json_path = extend_extension(parent_file, COMPANION_EXT);
                let data = read(json_path).map_err(IoReadError)?;
                let this: Self = serde_json::from_reader(Cursor::new(data)).map_err(SerdeJsonRead)?;

                Ok(this)
            }

            /// Writes json to a file equivalent to parent_file,
            /// but with the companion extension.
            pub fn write(&self, parent_file: &PathBuf) -> Result<(), CompanionError> {
                let json_path = extend_extension(parent_file, COMPANION_EXT);
                log::debug!("Writing metadata file {:?}...", json_path);

                write(json_path, serde_json::to_string(self).map_err(SerdeJsonWrite)?).map_err(IoWriteError)?;

                Ok(())
            }
        }

    }
}

pub fn extend_extension(path: &PathBuf, ext: &str) -> PathBuf {
    let mut new_ext = path
        .extension()
        .map(|v| v.to_ascii_lowercase())
        .unwrap_or_else(|| OsString::new());
    new_ext.push(ext);
    path.with_extension(new_ext)
}
