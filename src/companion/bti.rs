use crate::{companion, companion::rarc_entry::RarcEntryCompanion, gc::bti::Header};

companion!(BtiCompanion {
    #[serde(skip_serializing_if = "Option::is_none", default)]
    rarc_data: Option<RarcEntryCompanion>,
    header: Header
});
