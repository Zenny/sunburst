use bevy_egui::egui::Ui;

use crate::tabs::Tab;

pub const TITLE: &str = "Objects";

#[derive(Default, Debug, Clone, PartialEq)]
pub struct SceneHierarchy;

impl SceneHierarchy {
    pub fn ui(&mut self, _ui: &mut Ui, _res: ()) {}
}

impl Tab for SceneHierarchy {
    fn title(&self) -> &'static str {
        TITLE
    }
}
