use std::collections::HashMap;

use crate::{
    companion,
    companion::rarc_entry::RarcEntryCompanion,
    gc::{
        bmd::{
            mat3::{HardMaterialParams, Material},
            vtx1::AttributeFormat,
        },
        bti::Header,
        types::{indirect::tex_info::IndirectTexInfo, mesh::GxAttribute},
    },
};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct TextureProperties {
    /// Texture name without any path but with an extension
    pub tex_name: String,
    #[serde(flatten)]
    pub header: Header,
}

companion!(
    BmdCompanion {
        #[serde(skip_serializing_if = "Option::is_none", default)]
        rarc_data: Option<RarcEntryCompanion>,
        textures: Vec<TextureProperties>,
        vertex_data: HashMap<GxAttribute, AttributeFormat>,
        material_tables: HardMaterialParams,
        materials: Vec<Material>,
        indirect_tex_info: Vec<IndirectTexInfo>
    }
);
