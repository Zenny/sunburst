use bevy_egui::egui::Ui;

use crate::tabs::Tab;

pub const TITLE: &str = "Tables";

#[derive(Default, Debug, Clone, PartialEq)]
pub struct Tables;

impl Tables {
    pub fn ui(&mut self, _ui: &mut Ui, _res: ()) {}
}

impl Tab for Tables {
    fn title(&self) -> &'static str {
        TITLE
    }
}
