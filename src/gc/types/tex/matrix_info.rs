use std::io::{Read, Seek, SeekFrom, Write};

use bevy::math::{Mat4, Vec2, Vec3};
use serde::{Deserialize, Serialize};

use crate::{
    bytes::{read_f32, read_i16, read_u8},
    gc::{bmd::error::BmdWriteError, error::ReadError, types::tex::TexMatrixKind},
    try_from_repr,
    util::{get_mat4_rows, new_mat4_rows, DEG180_TO_I16ROT, I16ROT_TO_DEG180},
    w,
};

#[derive(Debug, Copy, Clone, Default, PartialEq, Serialize, Deserialize)]
pub struct TexMatrixInfo {
    pub proj: TexMatrixKind,
    // some sort of type?
    // 0 = rot translate scale?
    // 2 = use proj matrix?
    pub unk: u8,
    pub effect_translation: Vec3,
    pub scale: Vec2,
    /// from a -180.0 to 180.0 i16 format
    pub rotation: f32,
    pub translation: Vec2,
    pub proj_matrix: Mat4,
}

impl TexMatrixInfo {
    /// Advances position
    pub fn read<R: Read + Seek>(buf: &mut R) -> Result<Self, ReadError> {
        let kind = read_u8(buf)?;
        let unk = read_u8(buf)?;

        // Skip padding
        buf.seek(SeekFrom::Current(2))?;

        let effect_translation = Vec3::new(read_f32(buf)?, read_f32(buf)?, read_f32(buf)?);
        let scale = Vec2::new(read_f32(buf)?, read_f32(buf)?);
        let rotation = (read_i16(buf)? as f32 * I16ROT_TO_DEG180).to_radians();

        // Skip padding
        buf.seek(SeekFrom::Current(2))?;

        let translation = Vec2::new(read_f32(buf)?, read_f32(buf)?);

        #[rustfmt::skip]
        let proj_matrix = new_mat4_rows([
            read_f32(buf)?, read_f32(buf)?, read_f32(buf)?, read_f32(buf)?,
            read_f32(buf)?, read_f32(buf)?, read_f32(buf)?, read_f32(buf)?,
            read_f32(buf)?, read_f32(buf)?, read_f32(buf)?, read_f32(buf)?,
            read_f32(buf)?, read_f32(buf)?, read_f32(buf)?, read_f32(buf)?
        ]);

        Ok(Self {
            proj: try_from_repr!(kind => TexMatrixKind)?,
            unk,
            effect_translation,
            scale,
            rotation,
            translation,
            proj_matrix,
        })
    }

    pub fn write_bmd<W: Write + Seek>(&self, w: &mut W) -> Result<(), BmdWriteError> {
        w!(w, self.proj as u8)?;
        w!(w, self.unk)?;

        w!(w, 0xFFFFu16)?;

        w!(w, self.effect_translation.x)?;
        w!(w, self.effect_translation.y)?;
        w!(w, self.effect_translation.z)?;

        w!(w, self.scale.x)?;
        w!(w, self.scale.y)?;

        w!(w, (self.rotation.to_degrees() * DEG180_TO_I16ROT) as i16)?;

        w!(w, 0xFFFFu16)?;

        w!(w, self.translation.x)?;
        w!(w, self.translation.y)?;

        for r in get_mat4_rows(&self.proj_matrix) {
            w!(w, r)?;
        }

        Ok(())
    }
}
