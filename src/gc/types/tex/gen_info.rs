use std::io::{Read, Seek, SeekFrom, Write};

use serde::{Deserialize, Serialize};

use crate::{
    bytes::read_u8,
    gc::{
        bmd::error::BmdWriteError,
        error::ReadError,
        types::tex::{TexGenKind, TexGenSrc, TexMatrix},
    },
    try_from_repr, w,
};

/// Specifies how texture coordinates are generated.
///
/// Output texture coordinates are usually the result of some transform of an input attribute;
/// either position, normal, or texture coordinate.
/// You can also generate texture coordinates from the output color channel of the per-vertex
/// lighting calculations.
///
/// TexMatrix idenitfies a default set of texture matrix names that can be supplied.
#[derive(Debug, Copy, Clone, Default, PartialEq, Serialize, Deserialize)]
pub struct TexGenInfo {
    pub kind: TexGenKind,
    pub src: TexGenSrc,
    pub matrix: TexMatrix,
}

impl TexGenInfo {
    /// Advances position
    pub fn read<R: Read + Seek>(buf: &mut R) -> Result<Self, ReadError> {
        let kind = read_u8(buf)?;
        let src = read_u8(buf)?;
        let matrix = read_u8(buf)?;

        // Skip padding
        buf.seek(SeekFrom::Current(1))?;

        Ok(Self {
            kind: try_from_repr!(kind => TexGenKind)?,
            src: try_from_repr!(src => TexGenSrc)?,
            matrix: try_from_repr!(matrix => TexMatrix)?,
        })
    }

    pub fn write_bmd<W: Write + Seek>(&self, w: &mut W) -> Result<(), BmdWriteError> {
        w!(w, self.kind as u8)?;
        w!(w, self.src as u8)?;
        w!(w, self.matrix as u8)?;

        w!(w, 0xFFu8)?;

        Ok(())
    }
}
