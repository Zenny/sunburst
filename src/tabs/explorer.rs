use std::{fs, path::PathBuf};

use bevy_egui::egui::{
    self, epaint::Shadow, Color32, Margin, PointerButton, Rect, Rounding, Stroke, Ui, Vec2,
};
use egui_dock::TabStyle;

use crate::{
    gc::file::{Focused, UiFile},
    tabs::Tab,
    ui_data::*,
    util::sort_priority_dir,
};

pub const TITLE: &str = "Explorer";

const IMAGE_SIZE: Vec2 = Vec2::new(64.0, 64.0);
const GRID_SIZE: f32 = 68.0;
const GRID_DIV: f32 = 1.0 / 72.0;

#[derive(Default, Debug, Clone, PartialEq)]
pub struct Explorer;

impl Explorer {
    pub fn ui(&self, ui: &mut Ui, ui_data: &mut UiData) {
        let path_frame = egui::containers::Frame {
            inner_margin: Margin {
                left: 4.0,
                right: 4.0,
                top: 4.0,
                bottom: 0.0,
            },
            outer_margin: Default::default(),
            rounding: Rounding::none(),
            shadow: Shadow::NONE,
            fill: ui.visuals().widgets.noninteractive.bg_fill,
            stroke: Stroke::NONE,
        };
        egui::TopBottomPanel::top("explorer_path")
            .frame(path_frame)
            .show_inside(ui, |ui| {
                ui.label(format!(
                    "/{}",
                    ui_data.cur_path.to_string_lossy().replace("\\", "/")
                )); //res.cur_path.to_str().unwrap()
            });
        if let Some(proj) = ui_data
            .proj
            .as_ref()
            .map(|v| v.proj_dir.join(&ui_data.cur_path))
        {
            if ui_data.rendered_path.path != proj {
                ui_data.rendered_path.path = proj.clone();
                ui_data.rendered_path.files = fs::read_dir(&proj)
                    .expect("Failed to read project dir")
                    .into_iter()
                    .filter_map(|r| {
                        r.map(|f| UiFile {
                            path: f.path(),
                            ..Default::default()
                        })
                        .ok()
                    })
                    .collect::<Vec<_>>();
                if ui_data.cur_path.parent().is_some() {
                    ui_data.rendered_path.files.push(UiFile {
                        path: proj.join(".."),
                        ..Default::default()
                    });
                }
                ui_data
                    .rendered_path
                    .files
                    .sort_unstable_by(sort_priority_dir);
                log::info!("RENDER {:?}", ui_data.rendered_path);
            }
            self.render_grid(ui, ui_data);
        }
    }

    fn render_grid(&self, ui: &mut Ui, ui_data: &mut UiData) {
        let top_limit = ui.next_widget_position();
        ui.allocate_space(Vec2::new(ui.available_width(), 1.0));
        let num_per_row = (ui.available_width() * GRID_DIV) as usize - 1;

        egui::Grid::new("file_grid")
            .min_col_width(GRID_SIZE)
            .min_row_height(GRID_SIZE)
            .max_col_width(GRID_SIZE)
            .show(ui, |ui| {
                let mut clicked_new = None;
                let mut i = 0;
                for (ind, item) in ui_data.rendered_path.files.iter_mut().enumerate() {
                    let file_name = match item.path.file_name() {
                        Some(s) => s.to_string_lossy().to_string(),
                        None => {
                            if item.path.ends_with("..") {
                                String::from("..")
                            } else {
                                // Ignore items that are beyond comprehension
                                continue;
                            }
                        }
                    };

                    // If we were hovered or focused last frame, draw a box
                    if item.hovered || item.focused == Focused::Yes {
                        let painter = ui.painter();
                        let next = ui.next_widget_position();
                        let half_w = (ui.available_width() + 4.0) * 0.5;
                        let half_h = (ui_data.grid_height + 4.0) * 0.5;
                        let mut min = next - Vec2::new(half_w, half_h);
                        min.y = min.y.max(top_limit.y);
                        let rect = Rect::from_min_max(min, next + Vec2::new(half_w, half_h));
                        painter.rect_filled(rect, Rounding::none(), Color32::from_gray(64));
                    }

                    let resp = ui
                        .vertical_centered(|ui| {
                            ui.image(
                                ui_data.textures.get(file_to_image_key(&item.path)).unwrap(),
                                IMAGE_SIZE,
                            );
                            ui.label(file_name.clone());
                        })
                        .response;

                    ui_data.grid_height = ui_data.grid_height.max(ui.available_height());

                    if resp.hovered() {
                        item.hovered = true;
                        ui.ctx().input(|i| {
                            if i.pointer.button_pressed(PointerButton::Primary) {
                                item.focused = Focused::Maybe
                            } else if i.pointer.button_down(PointerButton::Primary) {
                                item.hovered = false;
                            } else if i.pointer.button_released(PointerButton::Primary) {
                                if item.focused == Focused::Maybe {
                                    item.focused = Focused::Yes;
                                }
                                clicked_new = Some(ind);
                            }
                            if i.pointer.button_double_clicked(PointerButton::Primary) {
                                if item.path.is_dir()
                                    || item.path.extension().is_none()
                                    || item.path.extension().is_some_and(|e| e == "szs")
                                {
                                    ui_data.cur_path = if file_name == ".." {
                                        ui_data
                                            .cur_path
                                            .parent()
                                            .map(|p| p.to_path_buf())
                                            .unwrap_or(PathBuf::new())
                                    } else {
                                        ui_data.cur_path.join(item.path.file_name().unwrap())
                                    };
                                    // path is changing which means grid will change so height
                                    // will too
                                    ui_data.grid_height = 0.0;
                                }
                            }
                        });
                    } else {
                        item.hovered = false;
                    }

                    if i == num_per_row {
                        i = 0;
                        ui.end_row();
                    } else {
                        i += 1;
                    }
                }

                // Unfocus all others if we clicked a new one
                if let Some(clicked) = clicked_new {
                    for (ind, item) in ui_data.rendered_path.files.iter_mut().enumerate() {
                        if ind != clicked {
                            item.focused = Focused::No;
                        }
                    }
                }
            });
    }
}

fn file_to_image_key(file: &PathBuf) -> &'static str {
    if file.extension().is_none() {
        return FOLDER_KEY;
    }

    if file.ends_with("scene.bin") {
        return SCENE_KEY;
    }

    if file
        .extension()
        .is_some_and(|e| e == "bmd" || e == "bdl" || e == "jpc" || e == "rarc" || e == "arc")
    {
        return BOX_KEY;
    }

    if file.extension().is_some_and(|e| e == "szs") {
        return PACKAGE_KEY;
    }

    if file.extension().is_some_and(|e| e == "aw") {
        return MUSIC_KEY;
    }

    if file.extension().is_some_and(|e| e == "thp") {
        return VIDEO_KEY;
    }

    if file.extension().is_some_and(|e| e == "col") {
        return COLLISION_KEY;
    }

    if file.extension().is_some_and(|e| e == "bti" || e == "bmp") {
        return TEXTURE_KEY;
    }

    if file.extension().is_some_and(|e| e == "bmg") {
        return TEXT_KEY;
    }

    if file.extension().is_some_and(|e| e == "tpl") {
        return BRUSH_KEY;
    }

    if file.extension().is_some_and(|e| e == "jpa") {
        return PARTICLE_KEY;
    }

    if file.extension().is_some_and(|e| e == "bck") {
        return ANIM_KEY;
    }

    if file.extension().is_some_and(|e| e == "bas") {
        return SOUND_KEY;
    }

    if file.extension().is_some_and(|e| e == "ral") {
        return RAIL_KEY;
    }

    FILE_KEY
}

impl Tab for Explorer {
    fn title(&self) -> &'static str {
        TITLE
    }

    fn style_override(&self, global_style: &TabStyle) -> Option<TabStyle> {
        let mut style = global_style.clone();
        style.inner_margin = Margin::same(0.0);
        Some(style)
    }
}
