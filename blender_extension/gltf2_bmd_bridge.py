import os

import bpy
from io_scene_gltf2.io.com.gltf2_io_extensions import Extension

bl_info = {
    "name": "glTF 2.0 BMD Bridge",
    "category": "Generic",
    "version": (0, 1, 0),
    "blender": (2, 80, 0),
    'location': 'File > Import > glTF 2.0, File > Export > glTF 2.0',
    'description': 'Bridges some gaps between Sunburst glTF files and Blender.',
    'tracker_url': "https://gitlab.com/Zenny/sunburst/-/issues",
    'isDraft': True,
    'developer': "Zenny",
    'url': 'https://zenny3d.com/',
}

# glTF extensions are named following a convention with known prefixes.
# See: https://github.com/KhronosGroup/glTF/tree/main/extensions#about-gltf-extensions
# also: https://github.com/KhronosGroup/glTF/blob/main/extensions/Prefixes.md
glTF_extension_name = "ZENNY_BMD_Bridge"

def find_node(gltf, name):
    for n in gltf.data.nodes:
        if n.name == name:
            return n
    return None


class BmdBridgeImporterExtensionProperties(bpy.types.PropertyGroup):
    enabled: bpy.props.BoolProperty(
        name=bl_info["name"],
        description='Run this extension while importing glTF file.',
        default=True
    )

    skel_front: bpy.props.BoolProperty(
       name='Show Bones in Front',
       description='Show the armature bones in front of the mesh.',
       default=True
    )

    hide_bb: bpy.props.BoolProperty(
        name='Hide Bounding Boxes',
        description='Hide the bounding boxes and spheres on import.',
        default=True
    )
    # float_property: bpy.props.FloatProperty(
    #    name='Sample FloatProperty',
    #    description='This is an example of a FloatProperty used by a UserExtension.',
    #    default=1.0
    # )


class GLTF_PT_BmdImportPanel(bpy.types.Panel):
    bl_space_type = 'FILE_BROWSER'
    bl_region_type = 'TOOL_PROPS'
    bl_label = "Enabled"
    bl_parent_id = "GLTF_PT_import_user_extensions"
    bl_options = {'DEFAULT_CLOSED'}

    @classmethod
    def poll(cls, context):
        sfile = context.space_data
        operator = sfile.active_operator
        return operator.bl_idname == "IMPORT_SCENE_OT_gltf"

    def draw_header(self, context):
        props = bpy.context.scene.BmdBridgeImporterExtensionProperties
        self.layout.prop(props, 'enabled')

    def draw(self, context):
        layout = self.layout
        layout.use_property_split = True
        layout.use_property_decorate = False  # No animation.

        props = bpy.context.scene.BmdBridgeImporterExtensionProperties
        layout.active = props.enabled

        # layout.prop(props, 'float_property', text="Some float value")
        layout.prop(props, 'skel_front', text="Show Bones in Front")
        layout.prop(props, 'hide_bb', text="Hide Bounding Boxes")


class glTF2ImportUserExtension:

    def __init__(self):
        self.obj_name = ''
        self.properties = bpy.context.scene.BmdBridgeImporterExtensionProperties
        # Extension(name="TEST_extension1", extension={}, required=True), Extension(name="TEST_extension2", extension={}, required=False)
        self.extensions = []

    def gather_import_node_before_hook(self, vnode, gltf_node, gltf):
        if self.properties.enabled:
            pass

    def gather_import_node_after_hook(self, vnode, gltf_node, blender_object, gltf):
        if self.properties.enabled:
            if blender_object.type == 'ARMATURE':
                # Mark the object that is the root of all
                self.obj_name = blender_object.name

    def gather_import_scene_after_nodes_hook(self, gltf_scene, blender_scene, gltf):
        # for each object under our newly imported
        # if the parent is none or has a name that is the gltf name or starts with the gltf name + '.'

        if self.properties.enabled:
            (path, filename) = os.path.split(gltf.filename)
            tex_folder = os.path.join(path, '_' + filename[:-5] + '_tex')

            for obj in blender_scene.objects:
                # Find selected object (armature)
                if obj.name == self.obj_name:
                    obj.show_in_front = self.properties.skel_front

                    for child in obj.children:
                        if hasattr(child.data, 'materials'):
                            for mat in child.data.materials:
                                mat.specular_intensity = 0

                                # Load our local texture for this mat
                                tex_name = mat.get('tex', None)
                                tex_path = os.path.join(tex_folder, tex_name)
                                bpy.ops.image.open(filepath=tex_path)
                                image = bpy.data.images[tex_name]
                                texture = bpy.data.textures.new(name='Texture', type='IMAGE')
                                texture.image = image
                                texture_node = mat.node_tree.nodes.new(type='ShaderNodeTexImage')
                                texture_node.image = image
                                params = mat.get('params', None)
                                if params is not None:
                                    if params['mag_filter'] == 'Linear':
                                        texture_node.interpolation = 'Linear'
                                    elif params['mag_filter'] == 'Near':
                                        texture_node.interpolation = 'Closest'

                                    # Don't need to set repeat as that's default
                                    if params['wrap'][0] == 'Clamp':
                                        texture_node.extension = 'EXTEND'
                                    elif params['wrap'][0] == 'MIRROR':
                                        texture_node.extension = 'MIRROR'
                                for n in mat.node_tree.nodes:
                                    if n.type == 'BSDF_PRINCIPLED':
                                        n.inputs['Specular'].default_value = 0
                                        mat.node_tree.links.new(texture_node.outputs['Color'], n.inputs['Base Color'])
                                        break

                        if hasattr(child.data, 'use_auto_smooth'):
                            # Disable auto smoothing which makes the normals look weird, we have our own
                            child.data.use_auto_smooth = 0

                    for bone in obj.data.bones:
                        # Get our inherit_scale custom property (Not to be confused with blender's)
                        # or none if not there (Shape instead of real joint)
                        inherit_prop = bone.get('compensate_scale', None)
                        if inherit_prop is not None and inherit_prop == 0:
                            # In blender, this inherit scale is applied to the child bones
                            for child in bone.children:
                                if hasattr(child, 'inherit_scale'):
                                    bone.inherit_scale = 'NONE'
                        bbox = bone.get('bounding_box', None)
                        if bbox is not None:
                            empty = bpy.data.objects.new("bbox_{}".format(bone.name), None)
                            # setup parenting to bone
                            empty.parent = obj
                            empty.parent_type = 'BONE'
                            empty.parent_bone = bone.name
                            # setup TRS
                            center = bbox.get('center', None)
                            if center is not None:
                                # flip because sms is y-up
                                # todo this is technically wrong because of bone rotations or something, and maybe more
                                empty.location = (center.get('x', 0.0) if bone.head_local.x >= 0.0 else -center.get('x', 0.0), -center.get('z', 0.0), center.get('y', 0.0))
                            size = bbox.get('size', None)
                            if size is not None:
                                # flip because sms is y-up
                                empty.scale = (size.get('x', 1.0), size.get('z', 1.0), size.get('y', 1.0))
                            else:
                                empty.scale = (1.0, 1.0, 1.0)
                            # add to scene
                            blender_scene.collection.objects.link(empty)
                            # setup display props
                            empty.empty_display_size = 1
                            empty.hide_set(self.properties.hide_bb)
                            empty.empty_display_type = 'CUBE'
                        bsphere = bone.get('bounding_sphere_radius', None)
                        if bsphere is not None:
                            empty = bpy.data.objects.new("bsphere_{}".format(bone.name), None)
                            # setup parenting to bone
                            empty.parent = obj
                            empty.parent_type = 'BONE'
                            empty.parent_bone = bone.name
                            # setup TRS
                            empty.scale = (bsphere, bsphere, bsphere)
                            # add to scene
                            blender_scene.collection.objects.link(empty)
                            # setup display props
                            empty.empty_display_size = 1
                            empty.hide_set(self.properties.hide_bb)
                            empty.empty_display_type = 'SPHERE'
                    #if bpy.context.mode != 'EDIT_ARMATURE':
                    #    bpy.ops.object.mode_set(mode='EDIT')
                    #for bone in obj.data.edit_bones:
                    #    bone.use_connect = True
                    break


# =================================
# Exporter
# =================================


class BmdBridgeExporterExtensionProperties(bpy.types.PropertyGroup):
    enabled: bpy.props.BoolProperty(
        name=bl_info["name"],
        description='Run this extension while exporting glTF file.',
        default=True
    )
    # float_property: bpy.props.FloatProperty(
    #    name='Sample FloatProperty',
    #    description='This is an example of a FloatProperty used by a UserExtension.',
    #    default=1.0
    # )


class GLTF_PT_BmdExportPanel(bpy.types.Panel):
    bl_space_type = 'FILE_BROWSER'
    bl_region_type = 'TOOL_PROPS'
    bl_label = "Enabled"
    bl_parent_id = "GLTF_PT_export_user_extensions"
    bl_options = {'DEFAULT_CLOSED'}


    @classmethod
    def poll(cls, context):
        sfile = context.space_data
        operator = sfile.active_operator
        return operator.bl_idname == "EXPORT_SCENE_OT_gltf"

    def draw_header(self, context):
        props = bpy.context.scene.BmdBridgeExporterExtensionProperties
        self.layout.prop(props, 'enabled')

    def draw(self, context):
        layout = self.layout
        layout.use_property_split = True
        layout.use_property_decorate = False  # No animation.

        props = bpy.context.scene.BmdBridgeExporterExtensionProperties
        layout.active = props.enabled

        # layout.prop(props, 'float_property', text="Some float value")


class glTF2ExportUserExtension:

    def __init__(self):
        self.properties = bpy.context.scene.BmdBridgeExporterExtensionProperties
        # Extension(name="TEST_extension1", extension={}, required=True), Extension(name="TEST_extension2", extension={}, required=False)
        self.extensions = []

    def gather_joint_hook(self, gltf2_node, blender_bone, export_settings):
        # todo export inherit_scale
        armature = blender_bone.id_data
        found = 0
        for node in armature.children:
            if hasattr(node, 'empty_display_type') and \
                    node.parent_bone == blender_bone.name:
                if node.empty_display_type == 'SPHERE':
                    gltf2_node.extras['bounding_sphere_radius'] = node.scale.x
                elif node.empty_display_type == 'CUBE':
                    gltf2_node.extras['bounding_box']['size']['x'] = node.scale.x
                    gltf2_node.extras['bounding_box']['size']['y'] = node.scale.y
                    gltf2_node.extras['bounding_box']['size']['z'] = node.scale.z

                    # flip because sms is y-up
                    # an inverse of the location set
                    # todo this is technically wrong because of bone rotations or something, and maybe more
                    # empty.location = (center.get('x', 0.0) if bone.head_local.x >= 0.0 else -center.get('x', 0.0), -center.get('z', 0.0), center.get('y', 0.0))
                    gltf2_node.extras['bounding_box']['center']['x'] = node.location.x if blender_bone.bone.head_local.x >= 0.0 else -node.location.x
                    gltf2_node.extras['bounding_box']['center']['y'] = node.location.z
                    gltf2_node.extras['bounding_box']['center']['z'] = -node.location.y
                found += 1
                # at most there is a sphere and a cube per bone, so 2 is the max to speed things up
                if found == 2:
                    break

    def gather_asset_hook(self, gltf2_asset, export_settings):
        gltf2_asset.generator = "{}, BMD Bridge v{}".format(gltf2_asset.generator, '.'.join(map(str, bl_info['version'])))


def register():
    bpy.utils.register_class(BmdBridgeImporterExtensionProperties)
    bpy.types.Scene.BmdBridgeImporterExtensionProperties = bpy.props.PointerProperty(
        type=BmdBridgeImporterExtensionProperties)
    bpy.utils.register_class(BmdBridgeExporterExtensionProperties)
    bpy.types.Scene.BmdBridgeExporterExtensionProperties = bpy.props.PointerProperty(
        type=BmdBridgeExporterExtensionProperties)


def unregister():
    unregister_panel()
    bpy.utils.unregister_class(BmdBridgeImporterExtensionProperties)
    del bpy.types.Scene.BmdBridgeImporterExtensionProperties
    bpy.utils.unregister_class(BmdBridgeExporterExtensionProperties)
    del bpy.types.Scene.BmdBridgeExporterExtensionProperties


def register_panel():
    # Register the panel on demand, we need to be sure to only register it once
    # This is necessary because the panel is a child of the extensions panel,
    # which may not be registered when we try to register this extension
    try:
        bpy.utils.register_class(GLTF_PT_BmdImportPanel)
        bpy.utils.register_class(GLTF_PT_BmdExportPanel)
    except Exception:
        pass

    # If the glTF importer is disabled, we need to unregister the extension panel
    # Just return a function to the importer so it can unregister the panel
    return unregister_panel


def unregister_panel():
    # Since panel is registered on demand, it is possible it is not registered
    try:
        bpy.utils.unregister_class(GLTF_PT_BmdImportPanel)
        bpy.utils.unregister_class(GLTF_PT_BmdExportPanel)
    except Exception:
        pass
