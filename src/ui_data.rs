use std::{
    collections::HashMap,
    fmt::{Display, Formatter},
    path::PathBuf,
    thread::JoinHandle,
};

use bevy::prelude::Resource;
use bevy_egui::{egui, egui::Context};

use crate::gc::file::UiFile;
use crate::project::Project;

pub const SCENE_KEY: &str = "scene";
pub const PACKAGE_KEY: &str = "package";
pub const MUSIC_KEY: &str = "music";
pub const FILE_KEY: &str = "file";
pub const TEXT_KEY: &str = "text";
pub const DATABASE_KEY: &str = "database";
pub const BOX_KEY: &str = "box";
pub const LOGO_SM_KEY: &str = "blueprint_rocket_sm";
pub const LOGO_KEY: &str = "blueprint_rocket";
pub const FOLDER_KEY: &str = "folder";
pub const COLLISION_KEY: &str = "collision";
pub const VIDEO_KEY: &str = "video";
pub const TEXTURE_KEY: &str = "texture";
pub const BRUSH_KEY: &str = "brush";
pub const PARTICLE_KEY: &str = "particles";
pub const ANIM_KEY: &str = "anim";
pub const SOUND_KEY: &str = "sound";
pub const RAIL_KEY: &str = "rail";

const TEXTURES: [(&str, &str); 18] = [
    (LOGO_KEY, "blueprint_rocket.png"),
    (LOGO_SM_KEY, "blueprint_rocket_sm.png"),
    (BOX_KEY, "box.png"),
    (DATABASE_KEY, "database.png"),
    (FILE_KEY, "file.png"),
    (TEXT_KEY, "text.png"),
    (FOLDER_KEY, "folder.png"),
    (MUSIC_KEY, "music.png"),
    (PACKAGE_KEY, "package.png"),
    (SCENE_KEY, "scene.png"),
    (VIDEO_KEY, "film.png"),
    (COLLISION_KEY, "collision.png"),
    (TEXTURE_KEY, "texture.png"),
    (BRUSH_KEY, "brush.png"),
    (PARTICLE_KEY, "particles.png"),
    (ANIM_KEY, "anim.png"),
    (SOUND_KEY, "sound.png"),
    (RAIL_KEY, "rail.png"),
];

#[derive(Default)]
pub struct BlockingWin {
    pub action: Option<BlockingAction>,
    pub handle: Option<JoinHandle<()>>,
}

#[derive(Debug, Clone)]
pub enum BlockingAction {
    CreateProj,
}

impl Display for BlockingAction {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                BlockingAction::CreateProj => "Creating Project...\n(Unpacking a lot of stuff)",
            }
        )
    }
}

#[derive(Debug, Clone)]
pub struct RenderedPath {
    pub files: Vec<UiFile>,
    pub path: PathBuf,
}

impl Default for RenderedPath {
    fn default() -> Self {
        Self {
            files: Vec::new(),
            path: PathBuf::from(".placeholder_that_wont_exist"),
        }
    }
}

#[derive(Resource, Default)]
pub struct UiData {
    pub show_fps: bool,
    pub show_about: bool,
    pub blocking: BlockingWin,
    pub rendered_path: RenderedPath,

    // new proj
    pub show_new: bool,
    pub new_err: Option<String>,
    pub sel_iso: PathBuf,
    pub sel_folder: PathBuf,

    /// None = no file loaded
    pub proj: Option<Project>,
    pub cur_path: PathBuf,
    pub textures: HashMap<&'static str, egui::TextureHandle>,
    pub grid_height: f32,
}

impl UiData {
    pub fn init_textures(&mut self, ctx: &Context) {
        let path = PathBuf::from("resources/");
        for (name, file) in TEXTURES.iter() {
            if !self.textures.contains_key(name) {
                let (rgba, width, height) = {
                    let file_path = path.join(file);
                    let image = image::open(file_path)
                        .expect(&format!("Failed to init texture at path {:?}", file))
                        .into_rgba8();
                    let (width, height) = image.dimensions();
                    let rgba = image.into_raw();
                    (rgba, width as usize, height as usize)
                };
                // Load the texture only once.
                let handle = ctx.load_texture(
                    *name,
                    egui::ColorImage::from_rgba_unmultiplied([width, height], &rgba),
                    Default::default(),
                );
                self.textures.insert(name, handle);
            }
        }
    }
}
