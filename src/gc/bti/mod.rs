use std::{
    collections::HashSet,
    io::{BufRead, Cursor, Read, Seek, SeekFrom, Write},
    mem,
    path::PathBuf,
};

use exoquant::{ditherer, optimizer};
use image::{
    imageops::{resize, FilterType},
    save_buffer_with_format, DynamicImage, GenericImageView, ImageFormat,
};
use serde::{Deserialize, Serialize};

use crate::{
    bytes::{read_u16, read_u32, read_u8},
    companion::{bti::BtiCompanion, rarc_entry::RarcEntryCompanion},
    errify,
    gc::{
        bti::{
            convert::ColorData,
            decode::*,
            encode::*,
            error::{BtiWriteError, PngReadError, PngWriteError},
        },
        error::ReadError,
        types::bti::{
            Anisotropy, JUTTransparency, PaletteFormat, TextureFilter, TextureFormat, TextureWrap,
        },
    },
    try_from_repr, w, wbool, wpos, wraw,
};

mod convert;
mod decode;
mod encode;
pub mod error;

pub struct HeaderOffsets {
    pub palette_offset: u64,
    pub tex_data_offset: u64,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Header {
    pub format: TextureFormat,
    /// Alpha enabled
    pub alpha: JUTTransparency,
    #[serde(default, skip_serializing)]
    pub width: u16,
    #[serde(default, skip_serializing)]
    pub height: u16,
    /// Wrap S and wrap T
    pub wrap: (TextureWrap, TextureWrap),
    // todo user probably shouldn't set this
    /// Must be set to true for the palette to be used correctly
    pub palette: bool,
    /// Only useful for formats that use palette
    pub palette_format: PaletteFormat,
    /// The number of entries in the palette data at offset (num of colors)
    #[serde(default, skip_serializing)]
    pub palette_count: u16,
    /// The offset to the palette data, relative to the start of this header
    #[serde(default, skip_serializing)]
    pub palette_offset: u32,
    /// Mipmap enabled
    pub mipmap: bool,
    /// compute LOD using adjacent texels
    pub edge_lod: bool,
    /// clamp (LOD+lodbias) so that it is never less than the minimum extent of the pixel projected
    /// in texture space
    pub bias_clamp: bool,
    pub max_anisotropy: Anisotropy,
    /// Texture filter type to use when the texel/pixel ratio is >= 1.0
    pub min_filter: TextureFilter,
    /// Texture filter type to use when the texel/pixel ratio is < 1.0; use only GX_NEAR or
    /// GX_LINEAR
    pub mag_filter: TextureFilter,
    /// The minimum LOD clamp, scaled by 8, but usually 0
    pub min_lod: u8,
    /// The maximum LOD clamp, scaled by 8 ((mipmap_count - 1) * 8)
    pub max_lod: u8,
    /// Unused
    pub mipmap_count: u8,
    pub unknown: u8,
    /// The LOD bias of the texture, scaled by 100
    pub lod_bias: u16,
    /// The offset to the texture data, relative to the start of this header
    #[serde(default, skip_serializing)]
    pub tex_data_offset: u32,
}

impl Header {
    /// Advances position
    pub fn read<R: Read + Seek>(buf: &mut R) -> Result<Self, ReadError> {
        let format = read_u8(buf)?;
        let alpha = read_u8(buf)?;

        let width = read_u16(buf)?;
        let height = read_u16(buf)?;

        let wrap_s = read_u8(buf)?;
        let wrap_t = read_u8(buf)?;

        let palette = read_u8(buf)? != 0;
        let palette_format = read_u8(buf)?;
        let palette_count = read_u16(buf)?;
        let palette_offset = read_u32(buf)?;

        let mipmap = read_u8(buf)? != 0;
        let edge_lod = read_u8(buf)? != 0;
        let bias_clamp = read_u8(buf)? != 0;

        let max_anisotropy = read_u8(buf)?;

        let min_filter = read_u8(buf)?;
        let mag_filter = read_u8(buf)?;

        let min_lod = read_u8(buf)?;
        let max_lod = read_u8(buf)?;

        let mipmap_count = read_u8(buf)?;
        let unk = read_u8(buf)?;
        let lod_bias = read_u16(buf)?;
        let tex_data_offset = read_u32(buf)?;

        Ok(Self {
            format: try_from_repr!(format => TextureFormat)?,
            alpha: try_from_repr!(alpha => JUTTransparency)?,
            width,
            height,
            wrap: (
                try_from_repr!(wrap_s => TextureWrap)?,
                try_from_repr!(wrap_t => TextureWrap)?,
            ),
            palette,
            palette_format: try_from_repr!(palette_format => PaletteFormat)?,
            palette_count,
            palette_offset,
            mipmap,
            edge_lod,
            bias_clamp,
            max_anisotropy: try_from_repr!(max_anisotropy => Anisotropy)?,
            min_filter: try_from_repr!(min_filter => TextureFilter)?,
            mag_filter: try_from_repr!(mag_filter => TextureFilter)?,
            min_lod,
            max_lod,
            mipmap_count,
            unknown: unk,
            lod_bias,
            tex_data_offset,
        })
    }

    /// returns palette offset and tex_data_offset reserved to have correct location written
    pub fn write_bti<W: Write + Seek>(&self, w: &mut W) -> Result<HeaderOffsets, BtiWriteError> {
        w!(w, self.format as u8)?;
        w!(w, self.alpha as u8)?;
        w!(w, self.width)?;
        w!(w, self.height)?;
        w!(w, self.wrap.0 as u8)?;
        w!(w, self.wrap.1 as u8)?;
        wbool!(w, self.palette)?;
        w!(w, self.palette_format as u8)?;
        w!(w, self.palette_count)?;
        let palette_offset = w.stream_position()?;
        w!(w, 0u32)?;
        wbool!(w, self.mipmap)?;
        wbool!(w, self.edge_lod)?;
        wbool!(w, self.bias_clamp)?;
        w!(w, self.max_anisotropy as u8)?;
        w!(w, self.min_filter as u8)?;
        w!(w, self.mag_filter as u8)?;
        w!(w, self.min_lod)?;
        w!(w, self.max_lod)?;
        w!(w, self.mipmap_count)?;
        w!(w, self.unknown)?;
        w!(w, self.lod_bias)?;
        let tex_data_offset = w.stream_position()?;
        w!(w, 0u32)?;

        Ok(HeaderOffsets {
            palette_offset,
            tex_data_offset,
        })
    }
}

#[derive(Debug, Clone)]
pub struct Bti {
    /// Path relative to the project dir or the model file it came in
    pub path: PathBuf,
    pub header: Header,
    /// Will hold BE data
    pub data: Vec<u8>,
    /// Will hold BE data
    pub palette_data: Vec<u8>,
    /// Rarc datas are only available if the file is stored within a Rarc file
    pub rarc_data: Option<RarcEntryCompanion>,
}

impl Bti {
    pub fn new<R: Read + Seek + BufRead>(
        path: PathBuf,
        buf: &mut R,
        header_offset: u64,
        rarc_data: Option<RarcEntryCompanion>,
    ) -> Result<Self, ReadError> {
        log::info!("Reading {:?}", path);
        // Start over just in case
        buf.seek(SeekFrom::Start(header_offset))?;

        let header = Header::read(buf)?;

        let blocks_wide =
            (header.width as u32 + header.format.block_width() - 1) / header.format.block_width();
        let blocks_tall = (header.height as u32 + header.format.block_height() - 1)
            / header.format.block_height();

        let mut data_size = blocks_wide * blocks_tall * header.format.block_data_size();

        // Do all mips except one (the one we have)
        let mut mips = header.mipmap_count - 1;
        let mut cur_mip_size = data_size;
        while mips > 0 {
            // do first to skip the largest size (the one we have)
            cur_mip_size /= 4;
            data_size += cur_mip_size;
            mips -= 1;
        }

        buf.seek(SeekFrom::Start(
            header_offset + header.tex_data_offset as u64,
        ))?;

        let mut data = vec![0; data_size as usize];
        buf.read_exact(&mut data)?;

        buf.seek(SeekFrom::Start(
            header_offset + header.palette_offset as u64,
        ))?;

        let mut palette_data = vec![0; header.palette_count as usize * 2];
        buf.read_exact(&mut palette_data)?;

        Ok(Self {
            path,
            header,
            data,
            palette_data,
            rarc_data,
        })
    }

    pub fn write_png(&self, dir: &PathBuf) -> Result<(), PngWriteError> {
        log::info!("Converting file {:?}...", self.path);

        let png_path = self.path.with_extension("png");
        let filename = errify!(
            png_path.file_name(),
            PngWriteError::InvalidFileName(png_path.clone())
        )?;
        let png_path = dir.join(filename);

        let mut palette_data = Cursor::new(&self.palette_data);
        let palette_colors = self.decode_palette(&mut palette_data)?;

        let mut image_data = Cursor::new(&self.data);

        let width = self.header.width as usize;
        let height = self.header.height as usize;

        let block_width = self.header.format.block_width() as usize;
        let block_height = self.header.format.block_height() as usize;

        let mut block_x = 0;
        let mut block_y = 0;

        let mut pixels = vec![ColorData::Gray(0); width * height];

        while block_y < height {
            let decoded = self.decode_block(&mut image_data, &palette_colors)?;
            for (i, color) in decoded.into_iter().enumerate() {
                let rel_x = i % block_width;
                let rel_y = i / block_width;
                let px_x = block_x + rel_x;
                let px_y = block_y + rel_y;
                if px_x >= width || px_y >= height {
                    continue;
                }

                pixels[px_x + (px_y * width)] = color;
            }
            block_x += block_width;
            if block_x >= width {
                block_x = 0;
                block_y += block_height;
            }
        }

        let image_type = pixels[0].conversion_type();

        let buf = pixels
            .clone()
            .into_iter()
            .map(|c| {
                if c.conversion_type() != image_type {
                    panic!(
                        "mixing and matching data {:?} {:?} {:?} {:#?} {:#?}",
                        image_type,
                        c.conversion_type(),
                        self.path,
                        pixels,
                        self.header
                    );
                }
                match c {
                    ColorData::Rgba(b) => b.to_vec(),
                    ColorData::Rgb(b) => b.to_vec(),
                    ColorData::Gray(b) => vec![b],
                    ColorData::GrayAlpha(b) => b.to_vec(),
                }
            })
            .flatten()
            .collect::<Vec<_>>();
        // todo maybe rename or split into nicer parts
        BtiCompanion::new(self.rarc_data.clone(), self.header.clone()).write(&png_path)?;

        save_buffer_with_format(
            png_path,
            &buf,
            self.header.width as u32,
            self.header.height as u32,
            image_type,
            ImageFormat::Png,
        )?;

        Ok(())
    }

    pub fn read_png<R: BufRead + Seek>(path: PathBuf, data: R) -> Result<Self, PngReadError> {
        log::info!("Compiling {:?}...", path);

        let mut companion = BtiCompanion::read(&path)?;

        let mut img = image::load(data, ImageFormat::Png)?;

        companion.header.width = img.width() as u16;
        companion.header.height = img.height() as u16;

        let mut est_size = companion
            .header
            .format
            .calc_data_size(img.width(), img.height()) as usize;
        for i in 1..companion.header.mipmap_count as u32 {
            est_size += companion
                .header
                .format
                .calc_data_size(img.width() / (2 * i), img.height() / (2 * i))
                as usize;
        }

        let (palette_data, palette_inds) = if companion.header.format == TextureFormat::C8 {
            let mut unique_colors =
                HashSet::with_capacity(companion.header.format.num_colors() + 4);

            let colors = img
                .pixels()
                .map(|(_, _, p)| unsafe {
                    if unique_colors.len() <= companion.header.format.num_colors() + 4 {
                        unique_colors.insert(p);
                    }
                    mem::transmute(p.0)
                })
                .collect::<Vec<_>>();

            let (palette, inds) = if unique_colors.len() <= companion.header.format.num_colors() + 4
            {
                exoquant::convert_to_indexed(
                    &colors,
                    img.width() as usize,
                    companion.header.format.num_colors(),
                    &optimizer::KMeans,
                    &ditherer::None,
                )
            } else {
                exoquant::convert_to_indexed(
                    &colors,
                    img.width() as usize,
                    companion.header.format.num_colors(),
                    &optimizer::KMeans,
                    &ditherer::FloydSteinberg::new(),
                )
            };

            let encoded_palette = palette
                .into_iter()
                .map(|c| {
                    let c = unsafe { ColorData::Rgba(mem::transmute(c)) };
                    match companion.header.palette_format {
                        PaletteFormat::IA8 => c.to_ia8(),
                        PaletteFormat::RGB565 => c.to_rgb565().to_be_bytes(),
                        PaletteFormat::RGB5A3 => c.to_rgb5a3(companion.header.alpha).to_be_bytes(),
                    }
                })
                .flatten()
                .collect::<Vec<_>>();

            companion.header.palette = true;
            companion.header.palette_count = companion.header.format.num_colors() as u16;

            (encoded_palette, inds)
        } else {
            (Vec::new(), Vec::new())
        };

        let mut img_data = Vec::with_capacity(est_size);

        for i in 0..companion.header.mipmap_count {
            // Avoid resize if only 1 mipmap
            if i != 0 {
                img = DynamicImage::from(resize(
                    &img,
                    img.width() / 2,
                    img.height() / 2,
                    FilterType::Nearest,
                ));
            }

            img_data.extend(Self::encode_image(&img, &mut companion, &palette_inds));
        }

        Ok(Self {
            path: path.with_extension("bti"),
            header: companion.header,
            data: img_data,
            palette_data,
            rarc_data: companion.rarc_data,
        })
    }

    /// Buf is palette data
    pub fn decode_palette<R: Read>(&self, buf: &mut R) -> Result<Vec<ColorData>, ReadError> {
        if !self.header.format.use_palette() || !self.header.palette {
            return Ok(vec![]);
        }

        // num of colors in the palette
        let mut out = Vec::with_capacity(self.header.palette_count as usize);

        for _ in 0..self.header.palette_count {
            let color = self.decode_palette_color(read_u16(buf)?);
            out.push(color);
        }

        Ok(out)
    }

    pub fn decode_palette_color(&self, color: u16) -> ColorData {
        match self.header.palette_format {
            PaletteFormat::IA8 => ColorData::from_ia8(color),
            PaletteFormat::RGB565 => ColorData::from_rgb565(color),
            PaletteFormat::RGB5A3 => ColorData::from_rgb5a3(color),
        }
    }

    /// Buf is image data
    pub fn decode_block<R: Read>(
        &self,
        buf: &mut R,
        palette_colors: &Vec<ColorData>,
    ) -> Result<Vec<ColorData>, ReadError> {
        let num_iters = self.header.format.num_iters();
        let mut out = Vec::with_capacity(self.header.format.block_data_size() as usize);
        for _ in 0..num_iters {
            match self.header.format {
                TextureFormat::I4 => {
                    decode_i4(buf, &mut out)?;
                }
                TextureFormat::I8 => {
                    decode_i8(buf, &mut out)?;
                }
                TextureFormat::IA4 => {
                    decode_ia4(buf, &mut out)?;
                }
                TextureFormat::IA8 => {
                    decode_ia8(buf, &mut out)?;
                }
                TextureFormat::RGB565 => {
                    decode_rgb565(buf, &mut out)?;
                }
                TextureFormat::RGB5A3 => {
                    decode_rgb5a3(buf, &mut out)?;
                }
                TextureFormat::RGBA8 => {
                    decode_rgba8(buf, &mut out)?;
                }
                TextureFormat::C8 => {
                    decode_c8(buf, &mut out, palette_colors)?;
                }
                TextureFormat::Compressed => {
                    decode_cmpr(buf, &mut out, self.header.alpha)?;
                    // this one has special data needs todo refactor
                    break;
                }
                f => unimplemented!("BTI format {:?} is not implemented", f),
            }
        }

        Ok(out)
    }

    /// Returns (Palette data, Image data)
    pub fn encode_image(
        img: &DynamicImage,
        companion: &BtiCompanion,
        palette_inds: &Vec<u8>,
    ) -> Vec<u8> {
        let data_size = companion
            .header
            .format
            .calc_data_size(img.width(), img.height()) as usize;

        let mut out = vec![0u8; data_size];

        let mut block_x = 0;
        let mut block_y = 0;

        let block_width = companion.header.format.block_width();
        let block_height = companion.header.format.block_height();

        let width = img.width();
        let height = img.height();

        let mut offset = 0;

        while block_y < height {
            Self::encode_block(
                img,
                companion,
                offset,
                block_x,
                block_y,
                palette_inds,
                &mut out,
            );

            offset += companion.header.format.block_data_size() as usize;
            block_x += block_width;
            if block_x >= width {
                block_x = 0;
                block_y += block_height;
            }
        }

        out
    }

    pub fn encode_block(
        img: &DynamicImage,
        companion: &BtiCompanion,
        offset: usize,
        block_x: u32,
        block_y: u32,
        palette_inds: &Vec<u8>,
        out: &mut Vec<u8>,
    ) {
        match companion.header.format {
            TextureFormat::I4 => {
                encode_i4(
                    img,
                    img.width(),
                    img.height(),
                    offset,
                    block_x,
                    block_y,
                    out,
                );
            }
            TextureFormat::I8 => {
                encode_i8(
                    img,
                    img.width(),
                    img.height(),
                    offset,
                    block_x,
                    block_y,
                    out,
                );
            }
            TextureFormat::IA4 => {
                encode_ia4(
                    img,
                    img.width(),
                    img.height(),
                    offset,
                    block_x,
                    block_y,
                    out,
                );
            }
            TextureFormat::IA8 => {
                encode_ia8(
                    img,
                    img.width(),
                    img.height(),
                    offset,
                    block_x,
                    block_y,
                    out,
                );
            }
            TextureFormat::RGB565 => {
                encode_rgb565(
                    img,
                    img.width(),
                    img.height(),
                    offset,
                    block_x,
                    block_y,
                    out,
                );
            }
            TextureFormat::RGB5A3 => {
                encode_rgb5a3(
                    img,
                    companion.header.alpha,
                    img.width(),
                    img.height(),
                    offset,
                    block_x,
                    block_y,
                    out,
                );
            }
            TextureFormat::RGBA8 => {
                encode_rgba8(img, img.width(), img.height(), block_x, block_y, out);
            }
            TextureFormat::Compressed => {
                encode_cmpr(
                    img,
                    companion.header.alpha,
                    img.width(),
                    img.height(),
                    offset,
                    block_x,
                    block_y,
                    out,
                );
            }
            TextureFormat::C8 => {
                encode_c8(
                    palette_inds,
                    img.width(),
                    img.height(),
                    offset,
                    block_x,
                    block_y,
                    out,
                );
            }
            // todo only the texture formats listed here should be shown as options
            _ => unimplemented!(),
        }
    }

    pub fn write_bti<W: Write + Seek>(&self, w: &mut W) -> Result<(), BtiWriteError> {
        let HeaderOffsets {
            palette_offset,
            tex_data_offset,
        } = self.header.write_bti(w)?;

        if self.palette_data.len() != 0 {
            wpos!(w, palette_offset);
            wraw!(w, self.palette_data)?;
        }

        if self.data.len() != 0 {
            wpos!(w, tex_data_offset);
            wraw!(w, self.data)?;
        }

        Ok(())
    }

    pub fn predicted_bti_size(&self) -> usize {
        self.data.len() + self.palette_data.len() + 32
    }
}
