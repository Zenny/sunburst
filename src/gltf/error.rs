use base64::DecodeError;
use displaydoc::Display;
use thiserror::Error;

#[derive(Display, Error, Debug)]
pub enum AccessorReadError {
    /// glTF referenced invalid accessor
    InvalidAccessor,
    /// Accessor did not reference a buffer view
    NoBufferView,
    /// glTF referenced invalid buffer view
    InvalidBufferView,
    /// glTF referenced invalid buffer
    InvalidBuffer,
    /// Buffer data is not base64 URI
    UnsupportedBufferData,
    /// Failed to decode buffer data: {0}
    DecodeError(#[from] DecodeError),
}
