//! Texture related types

use bitflags::bitflags;
use num_enum::TryFromPrimitive;
use serde::{Deserialize, Serialize};

pub mod color_channel_info;
pub mod gen_info;
pub mod matrix_info;
pub mod post_gen_info;

bitflags! {
    /// Supports 8 lights
    #[derive(Debug, Copy, Clone, Default, PartialEq, Serialize, Deserialize)]
    pub struct LightMask: u8 {
        const LIGHT0 = 0x01;
        const LIGHT1 = 0x02;
        const LIGHT2 = 0x04;
        const LIGHT3 = 0x08;
        const LIGHT4 = 0x10;
        const LIGHT5 = 0x20;
        const LIGHT6 = 0x40;
        const LIGHT7 = 0x80;
    }
}

#[derive(Debug, Copy, Clone, Default, PartialEq, TryFromPrimitive, Serialize, Deserialize)]
#[repr(u8)]
pub enum TexMapId {
    Map0 = 0,
    Map1 = 1,
    Map2 = 2,
    Map3 = 3,
    Map4 = 4,
    Map5 = 5,
    Map6 = 6,
    Map7 = 7,

    #[default]
    Null = 0xFF,
}

#[derive(Debug, Copy, Clone, Default, PartialEq, TryFromPrimitive, Serialize, Deserialize)]
#[repr(u8)]
pub enum TexCoordId {
    TexCoord0 = 0,
    TexCoord1 = 1,
    TexCoord2 = 2,
    TexCoord3 = 3,
    TexCoord4 = 4,
    TexCoord5 = 5,
    TexCoord6 = 6,
    TexCoord7 = 7,

    #[default]
    Null = 0xFF,
}

// todo are 0 and 1 used?
#[derive(Debug, Copy, Clone, Default, PartialEq, TryFromPrimitive, Serialize, Deserialize)]
#[repr(u8)]
pub enum TexGenKind {
    Matrix3x4 = 0,
    #[default]
    Matrix2x4 = 1,
    // these supposedly map to a light id equiv to the bump#
    Bump0 = 2,
    Bump1 = 3,
    Bump2 = 4,
    Bump3 = 5,
    Bump4 = 6,
    Bump5 = 7,
    Bump6 = 8,
    Bump7 = 9,
    /// Map R(ed) and G(reen) components of a color channel to UV coordinates
    SRTG = 10,
}

#[derive(Debug, Copy, Clone, Default, PartialEq, TryFromPrimitive, Serialize, Deserialize)]
#[repr(u8)]
pub enum TexGenSrc {
    Position = 0,
    Normal = 1,
    Binormal = 2,
    Tangent = 3,
    #[default]
    Tex0 = 4,
    Tex1 = 5,
    Tex2 = 6,
    Tex3 = 7,
    Tex4 = 8,
    Tex5 = 9,
    Tex6 = 10,
    Tex7 = 11,
    TexCoord0 = 12,
    TexCoord1 = 13,
    TexCoord2 = 14,
    TexCoord3 = 15,
    TexCoord4 = 16,
    TexCoord5 = 17,
    TexCoord6 = 18,
    Color0 = 19,
    Color1 = 20,
}

#[derive(Debug, Copy, Clone, Default, PartialEq, TryFromPrimitive, Serialize, Deserialize)]
#[repr(u8)]
pub enum TexMatrix {
    Identity = 60,
    #[default]
    TexMtx0 = 30,
    TexMtx1 = 33,
    TexMtx2 = 36,
    TexMtx3 = 39,
    TexMtx4 = 42,
    TexMtx5 = 45,
    TexMtx6 = 48,
    TexMtx7 = 51,
    TexMtx8 = 54,
    TexMtx9 = 57,
}

#[derive(Debug, Copy, Clone, Default, PartialEq, TryFromPrimitive, Serialize, Deserialize)]
#[repr(u8)]
pub enum TexMatrixKind {
    #[default]
    Matrix3x4 = 0,
    Matrix2x4 = 1,
}

#[derive(Debug, Copy, Clone, Default, PartialEq, TryFromPrimitive, Serialize, Deserialize)]
#[repr(u8)]
pub enum ColorChannelId {
    Color0 = 0,
    Color1 = 1,
    Alpha0 = 2,
    Alpha1 = 3,
    Color0A0 = 4,
    Color1A1 = 5,
    Zero = 6,
    AlphaBump = 7,
    AlphaBumpN = 8,

    #[default]
    Null = 0xFF,
}

// todo is this numbered right?
#[derive(Debug, Copy, Clone, Default, PartialEq, TryFromPrimitive, Serialize, Deserialize)]
#[repr(u8)]
pub enum AttenuationFn {
    Specular = 0,
    Spotlight = 1,
    #[default]
    None = 2,
    // Necessary for J3D compatibility at the moment.
    // Really we're looking at SpecDisabled/SpotDisabled there (2 adjacent bits in
    // HW field)
    Unknown = 3,
}

#[derive(Debug, Copy, Clone, Default, PartialEq, TryFromPrimitive, Serialize, Deserialize)]
#[repr(u8)]
pub enum DiffuseFn {
    #[default]
    None = 0,
    Signed = 1,
    Clamp = 2,
}

#[derive(Debug, Copy, Clone, Default, PartialEq, TryFromPrimitive, Serialize, Deserialize)]
#[repr(u8)]
pub enum ColorSrc {
    /// Use Register Colors
    #[default]
    Register = 0,
    /// Use Vertex Colors
    Vertex = 1,

    // todo:
    //  ambient is sometimes this. Does it mean None?
    Unknown = 0xFF,
}
