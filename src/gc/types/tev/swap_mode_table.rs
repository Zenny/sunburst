use std::io::{Read, Seek, Write};

use serde::{Deserialize, Serialize};

use crate::{
    bytes::read_u8,
    gc::{bmd::error::BmdWriteError, error::ReadError},
    w,
};

/// GXSetTevSwapModeTable(GXTevSwapSel select, GXTevColorChan red, GXTevColorChan green,
/// GXTevColorChan blue, GXColorChan alpha)
///
/// The swap table allows the rasterized/color colors to be swapped component-wise. An entry
/// into the swap table specifies how input components remap to output components.
///
/// For any given R, G, B, or A it defines the input component which should be mapped to the
/// respective R, G, B, or A output component.
///
/// Example:
///
/// r: 0,
/// g: 1,
/// b: 2,
/// a: 3,
#[derive(Debug, Copy, Clone, Default, PartialEq, Serialize, Deserialize)]
pub struct TevSwapModeTable {
    pub r: u8,
    pub g: u8,
    pub b: u8,
    pub a: u8,
}

impl TevSwapModeTable {
    /// Advances position
    pub fn read<R: Read + Seek>(buf: &mut R) -> Result<Self, ReadError> {
        let r = read_u8(buf)?;
        let g = read_u8(buf)?;
        let b = read_u8(buf)?;
        let a = read_u8(buf)?;

        Ok(Self { r, g, b, a })
    }

    pub fn write_bmd<W: Write + Seek>(&self, w: &mut W) -> Result<(), BmdWriteError> {
        w!(w, self.r)?;
        w!(w, self.g)?;
        w!(w, self.b)?;
        w!(w, self.a)?;

        Ok(())
    }
}
