use egui_dock::TabStyle;

pub mod explorer;
pub mod properties;
pub mod rails;
pub mod scene;
pub mod scene_hierarchy;
pub mod tables;

macro_rules! tabs {
    ($($ident:ident),+) => {
        $(casey::snake!(use $ident::*;);)+
        #[derive(Clone, Debug, PartialEq)]
        pub enum Tabs {
            $( $ident($ident), )+
        }

        impl Tabs {
            $(
            casey::snake!(
                ///Shorthand to create a tab of this type
                pub fn $ident() -> Self {
                    casey::pascal!(
                        Self::$ident($ident)
                    )
                }
            );
            )+

            pub fn title(&self) -> &'static str {
                match self {
                    $( Tabs::$ident(t) => t.title(), )+
                }
            }

            pub fn style_override(&self, global_style: &TabStyle) -> Option<TabStyle> {
                match self {
                    $( Tabs::$ident(t) => t.style_override(global_style), )+
                }
            }
        }
    }
}

tabs!(Scene, SceneHierarchy, Explorer, Properties, Rails, Tables);

pub trait Tab {
    fn title(&self) -> &'static str;
    fn style_override(&self, _global_style: &TabStyle) -> Option<TabStyle> {
        None
    }
}
