use std::io::{Read, Seek, SeekFrom, Write};

use serde::{Deserialize, Serialize};

use crate::{
    bytes::read_u8,
    gc::{
        bmd::error::BmdWriteError,
        error::ReadError,
        types::tex::{TexGenKind, TexGenSrc, TexMatrix},
    },
    try_from_repr, w, wraw,
};

// todo: this might need these fields? https://libogc.devkitpro.org/gx_8h.html#a7d3139b693ace5587c3224e7df2d8245
//  	u32  	normalize,
// 		u32  	postmtx
//  https://github.com/riidefi/RiiStudio/blob/master/source/librii/gx/TexGen.hpp#L41
//  never not 0xFF in sunshine so maybe was not implemented right yet?
#[derive(Debug, Copy, Clone, Default, PartialEq, Serialize, Deserialize)]
pub struct PostTexGenInfo {
    pub kind: TexGenKind,
    pub src: TexGenSrc,
    pub matrix: TexMatrix,
}

impl PostTexGenInfo {
    /// Advances position
    pub fn read<R: Read + Seek>(buf: &mut R) -> Result<Self, ReadError> {
        let kind = read_u8(buf)?;
        let src = read_u8(buf)?;
        let matrix = read_u8(buf)?;

        // todo: this seems like it's padded to the nearest 8 bytes,
        //  is that true or do all the elements have 0xFF potential?
        //  every single one that has more than 0 post tex gens is
        //  like this. they all have 1 posttexgen so far. reg tex gens
        //  are 2 or 3 at these times. reg tex gen itself can be 1 without
        //  this weird padding
        // Skip padding
        buf.seek(SeekFrom::Current(5))?;

        Ok(Self {
            kind: try_from_repr!(kind => TexGenKind)?,
            src: try_from_repr!(src => TexGenSrc)?,
            matrix: try_from_repr!(matrix => TexMatrix)?,
        })
    }

    pub fn write_bmd<W: Write + Seek>(&self, w: &mut W) -> Result<(), BmdWriteError> {
        w!(w, self.kind as u8)?;
        w!(w, self.src as u8)?;
        w!(w, self.matrix as u8)?;

        wraw!(w, [0xFF, 0xFF, 0xFF, 0xFF, 0xFF])?;

        Ok(())
    }
}
