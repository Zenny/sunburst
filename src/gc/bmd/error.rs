use std::{io, path::PathBuf};

use displaydoc::Display;
use thiserror::Error;

use crate::{
    companion::error::CompanionError,
    gc::{
        bti::error::{BtiWriteError, PngReadError},
        types::{
            error::AttributeParseError,
            mesh::{GxAttribute, GxComponentType, GxDataType},
        },
    },
    gltf::{accessor_reader::BufferType, error::AccessorReadError},
};

#[derive(Display, Error, Debug)]
pub enum BmdWriteError {
    /// Failed to write file: {0}
    Io(#[from] io::Error),
    /// File has invalid name: {0}
    InvalidFileName(PathBuf),
    /// No node found: {0}
    InvalidInf1Node(u16),
    /// No valid GxComponentType for pair: {0:?}
    NoKnownGxComponentType((GxAttribute, GxComponentType)),
    /// No valid GxDataType for pair: {0:?}
    NoKnownGxDataType((GxAttribute, GxDataType)),
    /// Failed to cast data as {0}
    FailedCast(&'static str),
    /// Data type {0:?} is unsupported for {1}
    UnsupportedAttributeDataCombo(GxDataType, &'static str),
    /// Failed to write BTI in TEX1: {0}
    BtiWriteError(#[from] BtiWriteError),
}

#[derive(Display, Error, Debug)]
pub enum GltfReadError {
    /// Failed to read file: {0}
    Io(#[from] io::Error),
    /// Error parsing gltf file: {0}
    SerdeJson(#[from] serde_json::Error),
    /// Companion file error: {0}
    CompanionError(#[from] CompanionError),
    /// Model does not have position data.
    NoPosData,
    /// No objects are present in the model.
    NoObjects,
    /// Mesh primitive #{0} has no indices in shape {1:?}.
    NoIndices(usize, String),
    /**
    More than one object found in model. This is currently unsupported. You should parent it
    all into a single object.
     */
    UnsupportedNumObjects,
    /// Mesh has an unsupported amount of nodes.
    UnsupportedNumNodes,
    /// Mesh has an unsupported amount of bone influences.
    UnsupportedNumBoneInfluences,
    /// Mesh has an unsupported amount of bone influences.
    UnsupportedNumWeights,
    /// Mesh has an unsupported type for indices.
    UnsupportedIndsType,
    /// Model contains a mesh with no primitives.
    NoPrimitives,
    /// Failed to parse attribute: {0}
    AttributeParse(#[from] AttributeParseError),
    /// Failed to read accessor: {0}
    AccessorRead(#[from] AccessorReadError),
    /// Model contains too many bones
    TooManyBones,
    /// Invalid billboard value on mesh: {0}
    InvalidBillboard(String),
    /// Invalid inverse bind matrix type: {0:?}
    InvalidInvBindType(BufferType),
    /// EVP1 did not contain matrix at index {0}
    InvalidEvp1Matrix(usize),
    /// JNT1 did not contain bone at index {0}
    InvalidJnt1Bone(usize),
    /// Invalid bone ind. Could not remap bone {0}
    InvalidBoneRemap(u16),
    /// Invalid bone ind from weights. Could not find bone {0}
    InvalidWeightBone(usize),
    /// No skins were found in the model. Every model must contain 1 skinned mesh.
    NoSkins,
    /// No scenes were found in the model. Every model must contain 1 scene.
    NoScenes,
    /// Model's scene cannot be empty.
    EmptyScene,
    /// Could not read model: Invalid path {0:?}
    InvalidGltfPath(PathBuf),
    /// Failed to read png texture: {0}
    PngReadError(#[from] PngReadError),
    /// Invalid texture extension: {0:?}
    InvalidTexExt(String),
}
