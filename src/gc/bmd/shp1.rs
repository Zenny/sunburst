//! Based heavily on RiiStudio's SHP1 implementation

use std::{
    collections::HashMap,
    fmt::Debug,
    io::{Cursor, Read, Seek, SeekFrom, Write},
};

use bevy::math::Vec3;
use itertools::Itertools;
use mugltf::MeshPrimitive;
use num_enum::TryFromPrimitive;

use crate::{
    bytes::{read_f32, read_u16, read_u32, read_u8},
    gc::{
        align::{write_align, AlignType},
        bmd::{error::BmdWriteError, jnt1::create_remap_table},
        error::ReadError,
        types::mesh::{GxAttribute, GxAttributeKind, IndexedPrimitive, Mode, VertexDescriptor},
    },
    gltf::gltf_builder::GltfBuilder,
    util::{decode_draw_prim_cmd, encode_draw_prim_cmd},
    w, wat, wraw,
};

const SHP1: u32 = 0x53485031;

#[derive(Debug, Clone, PartialEq)]
pub struct Shape {
    pub mode: Mode,

    pub mat_prims: Vec<MatrixData>,
    pub vertex_descriptor: VertexDescriptor,

    pub bounding_sphere_radius: f32,
    pub bounding_box_min: Vec3,
    pub bounding_box_max: Vec3,
}

impl Shape {
    pub fn generate_prims(
        &self,
        ind: u16,
        builder: &mut GltfBuilder,
        cur_mat: Option<usize>,
    ) -> Vec<MeshPrimitive> {
        let mut out = Vec::with_capacity(2);
        // todo don't crash
        let pos_accessor_id = builder
            .find_accessor(GxAttribute::Position.to_string())
            .expect("Didn't set up position buffer");
        let norm_accessor_id = builder.find_accessor(GxAttribute::Normal.to_string());
        let weight_accessor_id = builder
            .find_accessor(format!("{}_WEIGHT", GxAttribute::PositionNormalMatrixInd))
            .expect("Didn't set up weight buffer");
        let bone_accessor_id = builder
            .find_accessor(format!("{}_BONES", GxAttribute::PositionNormalMatrixInd))
            .expect("Didn't set up bone buffer");

        let mut uv_accessors = Vec::with_capacity(8);
        for chan in 0..8 {
            let attr =
                GxAttribute::try_from_primitive(GxAttribute::Tex0 as u8 + chan as u8).unwrap();
            let Some(accessor_id) = builder.find_accessor(attr.to_string()) else {
                continue;
            };

            uv_accessors.push((attr, accessor_id));
        }

        let mut color_accessors = Vec::with_capacity(8);
        for chan in 0..2 {
            let attr =
                GxAttribute::try_from_primitive(GxAttribute::Color0 as u8 + chan as u8).unwrap();
            let Some(accessor_id) = builder.find_accessor(attr.to_string()) else {
                continue;
            };

            color_accessors.push((attr, accessor_id));
        }

        for i in 0..self.mat_prims.len() {
            let pos_vert_ind_acc = builder
                .find_accessor(format!("INDS{:02}{:02}", ind, i))
                .expect("pos vert ind acc is not accessible");
            let mut prim = MeshPrimitive {
                attributes: Default::default(),
                indices: Some(pos_vert_ind_acc),
                material: cur_mat,
                mode: Default::default(),
                targets: vec![],
                extras: Default::default(),
            };

            prim.attributes
                .insert(GxAttribute::Position.to_string(), pos_accessor_id);

            // Don't add the accessors if the shape isn't using them
            if self.vertex_descriptor.is_set(GxAttribute::Normal) {
                if let Some(id) = norm_accessor_id {
                    prim.attributes.insert(GxAttribute::Normal.to_string(), id);
                }
            }

            // We can detect not needing weight attributes later (All weights of 1 etc).
            // We can't detect it now because without skinning on each shape, things will
            // be wrong
            prim.attributes
                .insert("JOINTS_0".to_string(), bone_accessor_id);
            prim.attributes
                .insert("WEIGHTS_0".to_string(), weight_accessor_id);

            for (attr, id) in &uv_accessors {
                if self.vertex_descriptor.is_set(*attr) {
                    prim.attributes.insert(attr.to_string(), *id);
                }
            }

            for (attr, id) in &color_accessors {
                if self.vertex_descriptor.is_set(*attr) {
                    prim.attributes.insert(attr.to_string(), *id);
                }
            }

            out.push(prim);
        }

        out
    }

    pub fn get_bounding_box_dims(&self) -> Vec3 {
        self.bounding_box_max - self.bounding_box_min
    }

    pub fn find_drw1_ind(&self, mut mat_prim_ind: usize, v_pnmi: usize) -> usize {
        let mut drw1_ind = u16::MAX;
        while drw1_ind == u16::MAX {
            drw1_ind = if self.mode == Mode::SkinnedMesh {
                // use inds to get index
                self.mat_prims[mat_prim_ind].inds[v_pnmi]
            } else {
                // use cur to get index
                self.mat_prims[mat_prim_ind].cur
            };

            mat_prim_ind = mat_prim_ind.wrapping_sub(1);
        }
        return drw1_ind as usize;
    }
}

/// Each matrix data holds some amount of triangle strips (primitives) until it is full. Full being
/// that it reaches 10 unique Drw1 inds. Each MatrixData is a draw call
#[derive(Debug, Clone, PartialEq)]
pub struct MatrixData {
    /// A value in the inds array. Oftentimes, this is the first matrix in the set, but not always.
    /// If this is FFFF, that probably means the related entry in inds was also FFFF.
    ///
    /// This value seems irrelevant for skinned mesh modes?
    pub cur: u16,
    /// List of matrix indexes in drw1. When read in or written out, duplicate entries of the
    /// previous values are FFFF
    pub inds: Vec<u16>,
    /// List of indexed primitives. Each one tends to represent a triangle strip.
    pub prims: Vec<IndexedPrimitive>,
}

impl MatrixData {
    pub fn read_mats<R: Read + Seek>(
        buf: &mut R,
        data_header_offset: u32,
        drw_mat_ind_table_offset: u32,
        mat_list_last: &mut [u16; 10],

        // for reading prims todo maybe refactor
        prim_offset: u32,
        prim_size: u32,
        descriptor: &VertexDescriptor,
    ) -> Result<Self, ReadError> {
        buf.seek(SeekFrom::Start(data_header_offset as u64))?;

        let cur = read_u16(buf)?;
        let count = read_u16(buf)? as usize;
        let start_index = read_u32(buf)?;
        let mut inds = Vec::with_capacity(count);

        buf.seek(SeekFrom::Start(
            (drw_mat_ind_table_offset + (start_index * 0x2)) as u64,
        ))?;

        for i in 0..count {
            let mut ind = read_u16(buf)?;
            // We should grab from last register val
            if ind == 0xFFFF {
                // so grab it
                ind = mat_list_last[i];
            } else {
                // otherwise, we have a new value for the register
                mat_list_last[i] = ind;
            }
            inds.push(ind);
        }

        Ok(Self {
            cur,
            inds,
            prims: Self::read_prims(buf, prim_offset, prim_size, descriptor)?,
        })
    }

    /// Writes primitive data to prim_data. Writes header to matrix_data.
    /// The mat_list_last here differs from read. Here we use 0xFFFF as defaults, because
    /// if we used 0, then sometimes 0 would match and result in a 0xFFFF in a bad spot.
    /// 0xFFFF should technically never match up and if it did, nothing would happen.
    pub fn write_mats<W: Write + Seek>(
        &self,
        matrix_data: &mut W,
        prim_data: &mut W,
        drw_mat_ind_table: &mut Vec<Vec<u16>>,
        drw_mat_ind_table_offsets: &mut Vec<u32>,
        highest_ind_table_offset: &mut u32,
        mat_list_last: &mut [u16; 10],
        descriptor: &VertexDescriptor,
    ) -> Result<(), BmdWriteError> {
        w!(matrix_data, self.cur)?;
        w!(matrix_data, self.inds.len() as u16)?;

        let mut new_start_ind = *highest_ind_table_offset;
        let mut tmp_ind_table = Vec::with_capacity(self.inds.len());

        for (i, ind) in self.inds.iter().enumerate() {
            let mut val = *ind;
            // If our val is the same as the last register val
            if val == mat_list_last[i] {
                // use 0xFFFF to grab from last register val and keep register val the same
                val = 0xFFFF;
            } else {
                // otherwise, we have a new value for the register
                mat_list_last[i] = val;
            }

            tmp_ind_table.push(val);
            new_start_ind += 1;
        }

        let prev_table = drw_mat_ind_table.len().wrapping_sub(1);
        let start_ind = if drw_mat_ind_table.len() > 0
            && drw_mat_ind_table[prev_table].len() == tmp_ind_table.len()
            && tmp_ind_table.iter().all(|v| v == &0xFFFF)
        {
            // Get the offset to the previous table and don't insert our new table because it's a dupe
            drw_mat_ind_table_offsets[prev_table]
        } else {
            // Reuse pos if it already exists
            if let Some(ind) = drw_mat_ind_table.iter().position(|v| v == &tmp_ind_table) {
                drw_mat_ind_table_offsets[ind]
            } else {
                drw_mat_ind_table.push(tmp_ind_table);

                // Get the pos before we inserted this data (The start of the data)
                let pos = *highest_ind_table_offset;

                drw_mat_ind_table_offsets.push(pos);

                // Update the offset to include the new data
                *highest_ind_table_offset = new_start_ind;

                pos
            }
        };

        w!(matrix_data, start_ind)?;

        self.write_prims(prim_data, descriptor)?;

        Ok(())
    }

    fn est_prim_size(&self) -> usize {
        let mut size = 0;
        for p in self.prims.iter() {
            let est_attrs = 8;
            size += 2 * est_attrs * p.verts.len();
        }
        size
    }

    fn read_prims<R: Read + Seek>(
        buf: &mut R,
        prim_offset: u32,
        size: u32,
        descriptor: &VertexDescriptor,
    ) -> Result<Vec<IndexedPrimitive>, ReadError> {
        let mut out = Vec::with_capacity(16);

        buf.seek(SeekFrom::Start(prim_offset as u64))?;

        let end = buf.stream_position()? + size as u64;
        loop {
            if buf.stream_position()? >= end {
                break;
            }

            let tag = read_u8(buf)?;

            // NOOP - Just padding at the end to reach 32 byte align
            if tag == 0 {
                break;
            }

            if tag & 0x80 == 0 {
                return Err(ReadError::UnknownMDLCommand(tag));
            }

            let verts = read_u16(buf)?;

            let mut prim = IndexedPrimitive::new(decode_draw_prim_cmd(tag)?, verts as usize);
            let attrs = descriptor.get_supported();

            for (v, vert) in prim.verts.iter_mut().enumerate() {
                for a in attrs.iter() {
                    let val;
                    match descriptor.attributes.get(a) {
                        Some(GxAttributeKind::None) => {
                            val = 0;
                        }
                        Some(GxAttributeKind::Direct) => {
                            if !a.supports_direct() {
                                // todo error;
                                panic!("Attribute does not support direct");
                            }
                            val = if *a == GxAttribute::PositionNormalMatrixInd {
                                // This attr is basically the only one that uses direct
                                read_u8(buf)? as u16 / 3
                            } else {
                                // no clue if this is right
                                read_u8(buf)? as u16
                            };
                            if val == 0xFF {
                                panic!("{:?} ind {} val 0xFF", a, v);
                            }
                        }
                        Some(GxAttributeKind::Byte) => {
                            val = read_u8(buf)? as u16;
                            if val == 0xFF {
                                panic!("{:?} ind {} val 0xFF", a, v);
                            }
                        }
                        Some(GxAttributeKind::Short) => {
                            val = read_u16(buf)?;

                            if val == 0xFFFF {
                                log::warn!("{:?} ind {} val 0xFFFF", a, v);
                            }
                        }
                        None => unreachable!("Read: Somehow no matching attr in descriptor"),
                    }
                    vert.set(*a, val);
                }
            }

            out.push(prim);
        }

        Ok(out)
    }

    fn write_prims<W: Write + Seek>(
        &self,
        w: &mut W,
        descriptor: &VertexDescriptor,
    ) -> Result<(), BmdWriteError> {
        for prim in self.prims.iter() {
            let kind = encode_draw_prim_cmd(prim.kind);
            w!(w, kind)?;
            w!(w, prim.verts.len() as u16)?;

            let attrs = descriptor.get_supported();

            for vert in prim.verts.iter() {
                for a in attrs.iter() {
                    match descriptor.attributes.get(a) {
                        Some(GxAttributeKind::Direct) => {
                            if !a.supports_direct() {
                                // todo error;
                                panic!("Attribute does not support direct");
                            }
                            if *a == GxAttribute::PositionNormalMatrixInd {
                                w!(w, (vert.get(*a) * 3) as u8)?;
                            } else {
                                w!(w, vert.get(*a) as u8)?;
                            }
                        }
                        Some(GxAttributeKind::Byte) => {
                            w!(w, vert.get(*a) as u8)?;
                        }
                        Some(GxAttributeKind::Short) => {
                            w!(w, vert.get(*a))?;
                        }
                        Some(GxAttributeKind::None) => {}
                        None => unreachable!("Write: Somehow no matching attr in descriptor"),
                    }
                }
            }
        }

        write_align(w, 32, AlignType::Zero)?;

        Ok(())
    }

    pub fn can_fit(&self, inds: &Vec<u16>) -> bool {
        // Number of new elements that would be added
        let new = inds
            .iter()
            .unique()
            .filter(|&x| !self.inds.contains(x))
            .count();

        self.inds.len() + new <= 10
    }

    pub fn merge_inds(&mut self, inds: Vec<u16>, out_inds: &mut Vec<u16>) {
        // Check if we already have some inds and insert the index if so
        for (ind, i1) in self.inds.iter().enumerate() {
            for i in 0..inds.len() {
                if *i1 == inds[i] {
                    // The out_ind at the same pos should return the ind of the slot
                    // we're merging into
                    out_inds[i] = ind as u16;
                }
            }
        }

        // Now we have to insert the inds that are remaining in the list but relative to the
        // end of the list we're adding them into
        for i in 0..inds.len() {
            // 0xFFFF means our ind was not processed yet (not merged, new)
            if out_inds[i] == 0xFFFF {
                // Check if something we inserted in this loop now exists (duplicate ind from
                // prev iteration)
                if let Some(pos) = self.inds.iter().position(|v| v == &inds[i]) {
                    out_inds[i] = pos as u16;
                } else {
                    out_inds[i] = self.inds.len() as u16;
                    self.inds.push(inds[i]);
                }
            }
        }
    }
}

#[derive(Debug, Clone)]
pub struct Shp1 {
    pub len: u32,
    pub shapes: Vec<Shape>,
}

impl Shp1 {
    pub fn new<R: Read + Seek>(buf: &mut R, shp1_offset: u32) -> Result<Self, ReadError> {
        let shp1 = read_u32(buf)?;
        // SHP1
        if shp1 != SHP1 {
            return Err(ReadError::ReadCheck("SHP1"));
        }

        let shp1_len = read_u32(buf)?;
        // num of envelope matrices
        let data_count = read_u16(buf)? as usize;

        // Skip padding
        buf.seek(SeekFrom::Current(2))?;

        let shape_data_offset = read_u32(buf)?;
        let remap_table_offset = read_u32(buf)?;
        let _name_table_offset = read_u32(buf)?;
        let attr_table_offset = read_u32(buf)?;
        // ushorts which map from the MatrixData indexes to the DRW1Data array indexes
        let drw_mat_ind_table_offset = read_u32(buf)?;
        let primitive_data_offset = read_u32(buf)?;
        let matrix_data_offset = read_u32(buf)?;
        let mat_prim_table_offset = read_u32(buf)?;

        buf.seek(SeekFrom::Start((remap_table_offset + shp1_offset) as u64))?;

        let mut remaps = Vec::with_capacity(data_count);
        for _ in 0..data_count {
            remaps.push(read_u16(buf)?);
        }

        let mut shapes = Vec::new();

        let mut mat_list_last = [0; 10];
        for i in 0..data_count {
            // relative to remaps
            buf.seek(SeekFrom::Start(
                (shape_data_offset + shp1_offset + (remaps[i] as u32 * 0x28)) as u64,
            ))?;
            let mode = read_u8(buf)?;
            let mode =
                Mode::try_from_primitive(mode).map_err(|_| ReadError::InvalidShapeMode(mode))?;

            // Skip padding 98 00 06 03
            buf.seek(SeekFrom::Current(1))?;

            let num_mat_prims = read_u16(buf)? as usize;

            let attr_offset = read_u16(buf)?;

            let first_mat_data_ind = read_u16(buf)? as u32;
            let first_mat_prim_ind = read_u16(buf)? as u32;

            // Skip padding
            buf.seek(SeekFrom::Current(2))?;

            let bounding_sphere_radius = read_f32(buf)?;
            let bounding_box_min = Vec3::new(read_f32(buf)?, read_f32(buf)?, read_f32(buf)?);
            let bounding_box_max = Vec3::new(read_f32(buf)?, read_f32(buf)?, read_f32(buf)?);

            // Move to our desired attr
            buf.seek(SeekFrom::Start(
                (attr_table_offset + shp1_offset + attr_offset as u32) as u64,
            ))?;

            let vertex_descriptor = VertexDescriptor::read(buf)?;

            let mut mat_prims = Vec::with_capacity(num_mat_prims);

            for i in 0..num_mat_prims as u32 {
                let ind = first_mat_prim_ind + i;
                buf.seek(SeekFrom::Start(
                    (mat_prim_table_offset + shp1_offset + (ind * 0x8)) as u64,
                ))?;

                let mat_prim_size = read_u32(buf)?;
                let mat_prim_offset = read_u32(buf)?;

                // Assumption: Indexed by raw index *not* potentially remapped ID
                let mat = MatrixData::read_mats(
                    buf,
                    matrix_data_offset + shp1_offset + ((first_mat_data_ind + i) * 0x8),
                    drw_mat_ind_table_offset + shp1_offset,
                    &mut mat_list_last,
                    primitive_data_offset + shp1_offset + mat_prim_offset,
                    mat_prim_size,
                    &vertex_descriptor,
                )?;
                mat_prims.push(mat);
            }

            shapes.push(Shape {
                mode,
                mat_prims,
                vertex_descriptor,
                bounding_sphere_radius,
                bounding_box_min,
                bounding_box_max,
            });
        }

        Ok(Self {
            len: shp1_len,
            shapes,
        })
    }

    pub fn write_bmd<W: Write + Seek>(&self, w: &mut W) -> Result<(), BmdWriteError> {
        let start = w.stream_position()?;
        w!(w, SHP1)?;

        let length_offset = w.stream_position()?;
        w!(w, 0u32)?;

        w!(w, self.shapes.len() as u16)?;

        w!(w, 0xFFFFu16)?;

        // shape_data_offset - always 44
        w!(w, 44u32)?;
        let remap_table_offset = w.stream_position()?;
        w!(w, 0u32)?;
        // unused name table address
        w!(w, 0u32)?;
        let attr_table_offset = w.stream_position()?;
        w!(w, 0u32)?;
        let drw_mat_ind_table_offset = w.stream_position()?;
        w!(w, 0u32)?;
        let primitive_data_offset = w.stream_position()?;
        w!(w, 0u32)?;
        let matrix_data_offset = w.stream_position()?;
        w!(w, 0u32)?;
        let mat_prim_table_offset = w.stream_position()?;
        w!(w, 0u32)?;

        let items = self.shapes.iter().collect::<Vec<_>>();
        let (remaps, dedup) = create_remap_table(items);
        // 8 bytes per entry and 8 for the null entry
        let mut attr_bytes_table = Vec::with_capacity(
            8 * self
                .shapes
                .iter()
                .fold(0, |a, s| a + s.vertex_descriptor.attributes.len())
                + 8,
        );
        let mut cur_mat_ind: u16 = 0;
        let mut mat_prim_table =
            Vec::with_capacity(8 * self.shapes.iter().fold(0, |a, s| a + s.mat_prims.len()));
        // relates to matrix_data_offset (header info of MatrixData)
        let mut matrix_data_table =
            Vec::with_capacity(8 * self.shapes.iter().fold(0, |a, s| a + s.mat_prims.len()));
        let mut matrix_data_cursor = Cursor::new(&mut matrix_data_table);

        let mut primitive_data_table = Vec::with_capacity(self.shapes.iter().fold(0, |a, s| {
            a + s.mat_prims.iter().fold(0, |b, m| b + m.est_prim_size())
        }));
        let mut primitive_data_cursor = Cursor::new(&mut primitive_data_table);

        // todo slap this all into a struct together to make more sense
        let mut drw_mat_ind_table =
            Vec::with_capacity(self.shapes.iter().fold(0, |a, s| a + s.mat_prims.len()));
        let mut drw_mat_ind_table_offsets =
            Vec::with_capacity(self.shapes.iter().fold(0, |a, s| a + s.mat_prims.len()));
        let mut highest_ind_table_offset = 0;

        let mut mat_prim_cursor = Cursor::new(&mut mat_prim_table);
        let mut attr_cursor = Cursor::new(&mut attr_bytes_table);
        let mut attr_done = HashMap::with_capacity(dedup.len());

        let mut mat_list_last = [0xFFFF; 10];
        for shape in dedup {
            w!(w, shape.mode as u8)?;
            w!(w, 0xFFu8)?;
            w!(w, shape.mat_prims.len() as u16)?;

            let attr_pos = if let Some(pos) = attr_done.get(&shape.vertex_descriptor) {
                *pos
            } else {
                let pos = attr_cursor.position() as u16;
                shape.vertex_descriptor.write_bmd(&mut attr_cursor)?;
                attr_done.insert(shape.vertex_descriptor.clone(), pos);

                pos
            };
            w!(w, attr_pos)?;

            // THEORETICALLY these inds could serve different indexes and maybe compress data,
            // but it doesn't seem like SMS ever does this. So for simplicity we make it the same.
            // It should read the same whether it's compressed or not (I hope)

            // mat data (header)
            w!(w, cur_mat_ind)?;

            // mat prim
            w!(w, cur_mat_ind)?;
            cur_mat_ind += shape.mat_prims.len() as u16;

            w!(w, 0xFFFFu16)?;

            w!(w, shape.bounding_sphere_radius)?;
            w!(w, shape.bounding_box_min.x)?;
            w!(w, shape.bounding_box_min.y)?;
            w!(w, shape.bounding_box_min.z)?;
            w!(w, shape.bounding_box_max.x)?;
            w!(w, shape.bounding_box_max.y)?;
            w!(w, shape.bounding_box_max.z)?;

            for mat in shape.mat_prims.iter() {
                let last_primitive_len = primitive_data_cursor.position();
                mat.write_mats(
                    &mut matrix_data_cursor,
                    &mut primitive_data_cursor,
                    &mut drw_mat_ind_table,
                    &mut drw_mat_ind_table_offsets,
                    &mut highest_ind_table_offset,
                    &mut mat_list_last,
                    &shape.vertex_descriptor,
                )?;
                let primitive_size = primitive_data_cursor.position() - last_primitive_len;
                w!(mat_prim_cursor, primitive_size as u32)?;
                w!(mat_prim_cursor, last_primitive_len as u32)?;
            }
        }

        let here = w.stream_position()?;
        wat!(w, (here - start) as u32, remap_table_offset);

        for r in remaps {
            w!(w, r)?;
        }

        write_align(w, 32, AlignType::String)?;

        let here = w.stream_position()?;
        wat!(w, (here - start) as u32, attr_table_offset);

        wraw!(w, &attr_bytes_table)?;

        let here = w.stream_position()?;
        wat!(w, (here - start) as u32, drw_mat_ind_table_offset);

        wraw!(
            w,
            &drw_mat_ind_table
                .into_iter()
                .flatten()
                .map(|v| v.to_be_bytes())
                .flatten()
                .collect::<Vec<_>>()
        )?;

        write_align(w, 32, AlignType::String)?;

        let here = w.stream_position()?;
        wat!(w, (here - start) as u32, primitive_data_offset);

        wraw!(w, &primitive_data_table)?;

        let here = w.stream_position()?;
        wat!(w, (here - start) as u32, matrix_data_offset);

        wraw!(w, &matrix_data_table)?;

        let here = w.stream_position()?;
        wat!(w, (here - start) as u32, mat_prim_table_offset);
        wraw!(w, &mat_prim_table)?;

        write_align(w, 32, AlignType::String)?;

        let len = (w.stream_position()? - start) as u32;
        wat!(w, len, length_offset);

        Ok(())
    }
}
