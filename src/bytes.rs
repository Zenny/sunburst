use std::io::{self, BufReader, Read, Seek, SeekFrom};

use bytes::{Buf, BytesMut};

/// Helper to read a f32 BE from a buf
#[inline]
pub fn read_f32<R: Read>(buf: &mut R) -> io::Result<f32> {
    let mut bytes = BytesMut::zeroed(4);
    buf.read_exact(&mut bytes)?;
    Ok(bytes.get_f32())
}

/// Helper to read a u32 BE from a buf
#[inline]
pub fn read_u32<R: Read>(buf: &mut R) -> io::Result<u32> {
    let mut bytes = BytesMut::zeroed(4);
    buf.read_exact(&mut bytes)?;
    Ok(bytes.get_u32())
}

/// Helper to read a u16 BE from a buf
#[inline]
pub fn read_u16<R: Read>(buf: &mut R) -> io::Result<u16> {
    let mut bytes = BytesMut::zeroed(2);
    buf.read_exact(&mut bytes)?;
    Ok(bytes.get_u16())
}

/// Helper to read a u16 LE from a buf
#[inline]
pub fn read_u16_le<R: Read>(buf: &mut R) -> io::Result<u16> {
    let mut bytes = BytesMut::zeroed(2);
    buf.read_exact(&mut bytes)?;
    Ok(bytes.get_u16_le())
}

/// Helper to read a i16 BE from a buf
#[inline]
pub fn read_i16<R: Read>(buf: &mut R) -> io::Result<i16> {
    let mut bytes = BytesMut::zeroed(2);
    buf.read_exact(&mut bytes)?;
    Ok(bytes.get_i16())
}

/// Helper to read a u8 from a buf
#[inline]
pub fn read_u8<R: Read>(buf: &mut R) -> io::Result<u8> {
    let mut bytes = [0];
    buf.read_exact(&mut bytes)?;
    Ok(bytes[0])
}

/// Helper to peek a u8 from a buf and go back
#[inline]
pub fn peek_u8<R: Read + Seek>(buf: &mut R) -> io::Result<u8> {
    let byte = read_u8(buf)?;
    buf.seek(SeekFrom::Current(-1))?;
    Ok(byte)
}

/// Seek to a known near location, won't flush like seek will, useful for if you don't
/// think a flush is always necessarily going to happen
#[inline]
pub fn seek_near<R: Seek>(buf: &mut BufReader<R>, pos: i64) -> io::Result<()> {
    let cur = buf.stream_position()? as i64;
    buf.seek_relative(pos - cur)
}

/// Writes the current pos to w at a position and seeks back
#[macro_export]
macro_rules! wpos {
    ($w:expr, $pos:expr) => {{
        let pos = $w.stream_position()?;
        $w.seek(std::io::SeekFrom::Start($pos))?;
        crate::w!($w, pos as u32)?;
        $w.seek(std::io::SeekFrom::Start(pos))?;
    }};
}

/// Writes (1) BE bytes to (0) w at a (2) position and seeks back
#[macro_export]
macro_rules! wat {
    ($w:expr, $b:expr, $pos:expr) => {{
        let pos = $w.stream_position()?;
        $w.seek(std::io::SeekFrom::Start($pos))?;
        crate::w!($w, $b)?;
        $w.seek(std::io::SeekFrom::Start(pos))?;
    }};
}

/// Writes BE bytes to w
#[macro_export]
macro_rules! w {
    ($w:expr, $b:expr) => {
        crate::wraw!($w, $b.to_be_bytes())
    };
}

/// Writes a byte array to w
#[macro_export]
macro_rules! wraw {
    ($w:expr, $b:expr) => {
        $w.write_all(&$b)
    };
}

/// Writes a bool as u8 to w
#[macro_export]
macro_rules! wbool {
    ($w:expr, $b:expr) => {
        crate::w!($w, if $b { 1u8 } else { 0 })
    };
}
