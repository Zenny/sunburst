use crate::{companion, companion::rarc_entry::RarcEntryCompanion};

companion!(RarcCompanion {
    #[serde(skip_serializing_if = "Option::is_none", default)]
    rarc_data: Option<RarcEntryCompanion>,
    root_name: String,
    sync_ids: bool,
    next_id: u64
});
