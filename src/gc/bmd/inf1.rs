use std::{
    collections::HashMap,
    fmt::{Display, Formatter},
    io::{Read, Seek, Write},
};

use num_enum::TryFromPrimitive;

use crate::{
    bytes::{read_u16, read_u32},
    errify,
    gc::{
        align::{write_align, AlignType},
        bmd::error::BmdWriteError,
        error::ReadError,
    },
    w, wat,
};

const INF1: u32 = 0x494E4631;

#[derive(Debug, Copy, Clone, PartialEq, TryFromPrimitive)]
#[repr(u16)]
pub enum NodeKind {
    Terminator = 0,
    OpenChild = 1,
    CloseChild = 2,
    Joint = 16,
    Material = 17,
    Shape = 18,
}

impl NodeKind {
    pub fn is_material(&self) -> bool {
        match self {
            NodeKind::Material => true,
            _ => false,
        }
    }
}

// todo: this hierarchy is not super stable, probably needs access limitations, but
//  since we're just converting back and forth it's okay for now
#[derive(Debug, Clone)]
pub struct Node {
    pub id: u16,
    /// The actual index to the relevant data in other places (Like shapes)
    pub ind: u16,
    /// Helps us know where the index is pointing. At this point it should only be Joint, Material,
    /// or Shape
    pub kind: NodeKind,
    pub parent: Option<u16>,
    pub children: Vec<u16>,
}

impl Node {
    pub fn new<R: Read + Seek>(
        buf: &mut R,
        id: u16,
        parent: Option<u16>,
    ) -> Result<Self, ReadError> {
        let kind_num = read_u16(buf)?;
        let kind = NodeKind::try_from_primitive(kind_num)
            .map_err(|_| ReadError::InvalidNodeKind(kind_num))?;
        let ind = read_u16(buf)?;

        Ok(Self {
            id,
            ind,
            kind,
            parent,
            children: Vec::new(),
        })
    }

    pub fn write_bmd<W: Write + Seek>(
        &self,
        w: &mut W,
        hierarchy: &HashMap<u16, Node>,
    ) -> Result<(), BmdWriteError> {
        w!(w, self.kind as u16)?;
        w!(w, self.ind)?;

        if self.children.len() > 0 {
            w!(w, NodeKind::OpenChild as u16)?;
            w!(w, 0u16)?;

            for id in self.children.iter() {
                let node = errify!(hierarchy.get(id), BmdWriteError::InvalidInf1Node(*id))?;
                node.write_bmd(w, hierarchy)?;
            }

            w!(w, NodeKind::CloseChild as u16)?;
            w!(w, 0u16)?;
        }

        Ok(())
    }
}

impl Display for Node {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}#{} ({})", self.kind, self.ind, self.id)
    }
}

#[derive(Debug, Clone)]
pub struct Hierarchy {
    pub map: HashMap<u16, Node>,
}

impl Hierarchy {
    pub fn new() -> Self {
        Self {
            map: Default::default(),
        }
    }

    pub fn insert(&mut self, k: u16, v: Node) -> Option<Node> {
        self.map.insert(k, v)
    }

    pub fn get_mut(&mut self, k: &u16) -> Option<&mut Node> {
        self.map.get_mut(k)
    }

    pub fn get(&self, k: &u16) -> Option<&Node> {
        self.map.get(k)
    }

    pub fn next_id(&self) -> u16 {
        self.map.len() as u16
    }

    pub fn write_bmd<W: Write + Seek>(&self, w: &mut W) -> Result<(), BmdWriteError> {
        let root = self.map.get(&0).expect("There should always be a root");
        root.write_bmd(w, &self.map)?;

        w!(w, NodeKind::Terminator as u16)?;
        w!(w, 0u16)?;

        Ok(())
    }

    pub fn display_children(
        &self,
        node: &Node,
        level: usize,
        f: &mut Formatter<'_>,
    ) -> std::fmt::Result {
        writeln!(f, "{}", node)?;
        for cid in node.children.iter() {
            let c = self.get(cid).unwrap();
            write!(f, "{: <level$}+-- ", "")?;
            self.display_children(c, level + 2, f)?;
        }

        Ok(())
    }
}

impl Display for Hierarchy {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let root = self.get(&0).unwrap();
        self.display_children(root, 1, f)?;

        Ok(())
    }
}

#[derive(Debug, Clone)]
pub struct Inf1 {
    pub len: u32,
    /// 0 for BDL, 1 for BMD
    pub flags: u16,
    /// Num matrices in SHP1 in total (for each shape added together)
    pub num_packets: u32,
    /// Num verts in VTX1
    pub num_verts: u32,
    /// Offset location relative to beginning of section (typically location 0x18)
    pub hierarchy_offset: u32,
    pub hierarchy: Hierarchy,
}

impl Inf1 {
    pub fn new<R: Read + Seek>(buf: &mut R) -> Result<Self, ReadError> {
        let inf1 = read_u32(buf)?;
        // INF1
        if inf1 != INF1 {
            return Err(ReadError::ReadCheck("INF1"));
        }

        let inf1_len = read_u32(buf)?;
        let flags = read_u16(buf)?;
        let _pad = read_u16(buf)?;
        let num_packets = read_u32(buf)?;
        let num_verts = read_u32(buf)?;
        let hierarchy_offset = read_u32(buf)?;

        let mut parent_stack = vec![];
        let mut hierarchy = Hierarchy::new();
        let mut last = hierarchy.next_id();
        let root_id = hierarchy.next_id();
        let root = Node::new(buf, root_id, None)?;

        hierarchy.insert(root_id, root);

        loop {
            let parent = parent_stack.last().map(|v| *v).unwrap_or(root_id);
            let id = hierarchy.next_id();
            let node = Node::new(buf, id, Some(parent))?;
            if node.kind == NodeKind::Terminator {
                break;
            }

            // todo: opens child of PREVIOUS node? Lots of confusion in diff implementations
            if node.kind == NodeKind::OpenChild {
                parent_stack.push(last);
            } else if node.kind == NodeKind::CloseChild {
                parent_stack.pop().expect("bmd parent_stack popped too far");
            } else {
                last = id;
                // If insert returns a value, that's bad (we overwrote another ID)
                hierarchy
                    .insert(id, node)
                    .map_or(Ok(()), |_| Err(ReadError::NodeIndDup))?;
                if let Some(pn) = hierarchy.get_mut(&parent) {
                    pn.children.push(last);
                }
            }
        }

        Ok(Self {
            len: inf1_len,
            flags,
            num_packets,
            num_verts,
            hierarchy_offset,
            hierarchy,
        })
    }

    pub fn write_bmd<W: Write + Seek>(&self, w: &mut W) -> Result<(), BmdWriteError> {
        let start = w.stream_position()?;
        w!(w, INF1)?;
        let len_offset = w.stream_position()?;
        // Length written later
        w!(w, 0u32)?;
        w!(w, self.flags)?;
        w!(w, 0xFFFFu16)?;
        w!(w, self.num_packets)?;
        w!(w, self.num_verts)?;
        let hierarchy_offset = w.stream_position()? + 4 - start;
        w!(w, hierarchy_offset as u32)?;

        self.hierarchy.write_bmd(w)?;

        // Align to 32 byte address
        write_align(w, 32, AlignType::String)?;

        let len = w.stream_position()? - start;
        wat!(w, len as u32, len_offset);

        Ok(())
    }
}
