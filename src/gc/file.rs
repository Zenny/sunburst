use std::{ffi::OsString, fs, io::Cursor, path::PathBuf};

use yaz0::Yaz0Archive;

use crate::{
    companion::{generic::GenericCompanion, rarc::RarcCompanion, rarc_entry::RarcEntryCompanion},
    errify,
    gc::{
        bmd::Bmd,
        bti::Bti,
        error::{ReadError, WriteError},
        rarc::Rarc,
    },
};

#[derive(Debug, Default, Clone)]
pub struct UiFile {
    pub path: PathBuf,
    pub hovered: bool,
    pub focused: Focused,
}

#[derive(Debug, Clone)]
pub enum GcFile {
    /// A compressed RARC archive
    Rarc(Rarc),
    /// A directory
    Folder(GenericFile),
    /// A model file
    Bmd(Bmd),
    /// A texture file
    Bti(Bti),
    Unimplemented(GenericFile),
}

impl GcFile {
    pub fn new(
        path: PathBuf,
        data: Vec<u8>,
        rarc_data: Option<RarcEntryCompanion>,
    ) -> Result<Self, ReadError> {
        let ext = path.extension().map(|v| v.to_ascii_lowercase());
        Ok(
            match ext.unwrap_or_else(|| OsString::new()).to_str().unwrap() {
                // todo arc?
                "szs" => {
                    let buf = Cursor::new(data);
                    let decomp = Yaz0Archive::new(buf)?.decompress()?;
                    Self::Rarc(Rarc::read_szs(path, decomp, rarc_data)?)
                }
                "bmd" => {
                    let mut buf = Cursor::new(&data);
                    match Bmd::new(path.clone(), &mut buf, rarc_data.clone()) {
                        Ok(b) => Self::Bmd(b),
                        Err(e) => {
                            log::error!("unimplemented bmd: {:?} Err: {}", path, e);
                            Self::Unimplemented(GenericFile::new(path, data, rarc_data))
                        }
                    }
                }
                "bti" => {
                    let mut buf = Cursor::new(&data);
                    // header should be at the top
                    let b = Bti::new(path.clone(), &mut buf, 0, rarc_data.clone());
                    match b {
                        Ok(b) => Self::Bti(b),
                        Err(e) => {
                            log::error!("unimplemented bti: {:?} Err: {}", path, e);
                            Self::Unimplemented(GenericFile::new(path, data, rarc_data))
                        }
                    }
                }
                "" => Self::Folder(GenericFile::new(path, data, rarc_data)),
                _ => Self::Unimplemented(GenericFile::new(path, data, rarc_data)),
            },
        )
    }

    pub fn create_relative(&self, proj_dir: &PathBuf) -> Result<(), WriteError> {
        match self {
            GcFile::Rarc(f) => {
                let folder = proj_dir.join(&f.path);
                fs::create_dir(&folder)?;
                RarcCompanion::new(
                    f.rarc_data.clone(),
                    f.root_name.clone(),
                    f.info.sync_ids,
                    f.info.next_id as u64,
                )
                .write(&folder)?;
                for file in f.files.iter() {
                    file.create_relative(proj_dir)?;
                }
            }
            GcFile::Folder(f) => {
                fs::create_dir(proj_dir.join(&f.path))?;
            }
            GcFile::Unimplemented(f) => {
                let file_path = proj_dir.join(&f.path);

                if let Some(rarc_flags) = &f.rarc_data {
                    GenericCompanion::new(Some(rarc_flags.clone())).write(&file_path)?;
                }

                fs::write(file_path, &f.data)?;
            }
            GcFile::Bmd(f) => {
                let cur_path = errify!(f.path.parent(), WriteError::NoParentPath(f.path.clone()))?;
                let path = proj_dir.join(cur_path);
                match f.write_gltf(&path) {
                    Ok(_) => {}
                    Err(e) => {
                        // Write as bti if something failed
                        log::warn!("Failed to convert BMD {:?}: {}", f.path, e);

                        let mut bmd_vec = Vec::with_capacity(10_000);
                        let mut cursor = Cursor::new(&mut bmd_vec);
                        f.write_bmd(&mut cursor)?;
                        let file_path = proj_dir.join(&f.path.with_extension("bmd"));

                        if let Some(rarc_flags) = &f.rarc_data {
                            GenericCompanion::new(Some(rarc_flags.clone())).write(&file_path)?;
                        }

                        fs::write(file_path, bmd_vec)?;
                    }
                }
            }
            GcFile::Bti(f) => {
                let cur_path = errify!(f.path.parent(), WriteError::NoParentPath(f.path.clone()))?;
                let path = proj_dir.join(cur_path);
                match f.write_png(&path) {
                    Ok(_) => {}
                    Err(e) => {
                        // Write as bti if something failed
                        log::warn!("Failed to convert BTI {:?}: {}", f.path, e);

                        let mut bti_vec = Vec::with_capacity(f.predicted_bti_size());
                        let mut cursor = Cursor::new(&mut bti_vec);
                        f.write_bti(&mut cursor)?;
                        let file_path = proj_dir.join(&f.path.with_extension("bti"));

                        if let Some(rarc_flags) = &f.rarc_data {
                            GenericCompanion::new(Some(rarc_flags.clone())).write(&file_path)?;
                        }

                        fs::write(file_path, bti_vec)?;
                    }
                }
            }
        }
        Ok(())
    }

    pub fn path(&self) -> &PathBuf {
        match self {
            GcFile::Rarc(a) => &a.path,
            GcFile::Folder(a) => &a.path,
            GcFile::Bmd(a) => &a.path,
            GcFile::Bti(a) => &a.path,
            GcFile::Unimplemented(a) => &a.path,
        }
    }
}

#[derive(Debug, Clone)]
pub struct GenericFile {
    pub path: PathBuf,
    pub data: Vec<u8>,
    /// Rarc datas are only available if the file is stored within a Rarc file
    pub rarc_data: Option<RarcEntryCompanion>,
}

impl GenericFile {
    pub fn new(path: PathBuf, data: Vec<u8>, rarc_flags: Option<RarcEntryCompanion>) -> Self {
        Self {
            path,
            data,
            rarc_data: rarc_flags,
        }
    }
}

#[derive(Debug, Clone, PartialEq, Default)]
pub enum Focused {
    Yes,
    #[default]
    No,
    Maybe,
}
