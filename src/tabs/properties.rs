use bevy_egui::egui::Ui;

use crate::tabs::Tab;

pub const TITLE: &str = "Properties";

#[derive(Default, Debug, Clone, PartialEq)]
pub struct Properties;

impl Properties {
    pub fn ui(&mut self, _ui: &mut Ui, _res: ()) {}
}

impl Tab for Properties {
    fn title(&self) -> &'static str {
        TITLE
    }
}
