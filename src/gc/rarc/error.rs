use crate::companion::error::CompanionError;
use displaydoc::Display;
use std::io;
use std::path::PathBuf;
use thiserror::Error;

#[derive(Display, Error, Debug)]
pub enum RarcCompileError {
    /// Failed to write file: {0}
    Io(#[from] io::Error),
    /// Error reading companion file: {0}
    CompanionError(#[from] CompanionError),
    /** Rarc file is too large. The max is 4.29GB, but no game can hold more than 1.46GB anyway.
    What are you doing? */
    RarcTooBig,
    /// File entry ID out of range: {0}. Max is 65,535
    EntryIdOutOfRange(u64),
    /// Next ID out of range: {0}. Max is 65,535
    NextIdOutOfRange(u64),
    /// ID is not synced, wanted {0} but got {1}
    NotSynced(usize, u16),
    /// Failed to compress with Yaz0: {0}
    Yaz0Error(#[from] yaz0::Error),
    /// Found RARC entry with invalid metadata: {0}
    EntryWithNoMeta(PathBuf),
}
