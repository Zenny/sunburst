use serde::{Deserialize, Serialize};
use std::io::{Read, Seek, SeekFrom, Write};

use crate::{
    bytes::read_u8,
    gc::{bmd::error::BmdWriteError, error::ReadError, types::indirect::IndirectScale},
    try_from_repr, w,
};

#[derive(Debug, Copy, Clone, Default, Serialize, Deserialize)]
pub struct IndirectTexScale {
    /// Scale value for S (U)
    pub s: IndirectScale,
    /// Scale value for T (V)
    pub t: IndirectScale,
}

impl IndirectTexScale {
    /// Advances position
    pub fn read<R: Read + Seek>(buf: &mut R) -> Result<Self, ReadError> {
        let s = read_u8(buf)?;
        let t = read_u8(buf)?;

        // Skip padding
        buf.seek(SeekFrom::Current(2))?;

        Ok(Self {
            s: try_from_repr!(s => IndirectScale)?,
            t: try_from_repr!(t => IndirectScale)?,
        })
    }

    pub fn write_bmd<W: Write + Seek>(&self, w: &mut W) -> Result<(), BmdWriteError> {
        w!(w, self.s as u8)?;
        w!(w, self.t as u8)?;

        w!(w, 0xFFFFu16)?;

        Ok(())
    }
}
