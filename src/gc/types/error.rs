use displaydoc::Display;
use thiserror::Error;

#[derive(Display, Error, Debug)]
pub enum AttributeParseError {
    /// Invalid attribute: {0}
    AttributeNotFound(String),
}
