use serde::{Deserialize, Serialize};
use std::io::{Read, Seek, SeekFrom, Write};

use crate::{
    bytes::read_u8,
    gc::{
        bmd::error::BmdWriteError,
        error::ReadError,
        types::indirect::{
            tev_order::IndirectTevOrder, tev_stage::IndirectTevStage,
            tex_matrix::IndirectTexMatrix, tex_scale::IndirectTexScale,
        },
    },
    w, wbool,
};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct IndirectTexInfo {
    /// Determines if an indirect texture lookup is to take place
    pub indirect_lookup: bool,
    /// The number of indirect texturing stages to use
    pub num_stages: u8,
    pub tev_orders: [IndirectTevOrder; 4],
    /// The dynamic 2x3 matrices to use when transforming the texture coordinates
    pub matrices: [IndirectTexMatrix; 3],
    /// U and V scales to use when transforming the texture coordinates
    pub scales: [IndirectTexScale; 4],
    /// Instructions for setting up the specified TEV stage for lookup operations
    // todo this could be a vec I guess, num_stages outlines it decently
    pub stages: [IndirectTevStage; 16],
}

impl IndirectTexInfo {
    /// Advances position
    pub fn read<R: Read + Seek>(buf: &mut R) -> Result<Self, ReadError> {
        let indirect_lookup = read_u8(buf)? != 0;
        let num_stages = read_u8(buf)?;

        // Skip padding
        buf.seek(SeekFrom::Current(2))?;

        let mut tev_orders = [Default::default(); 4];
        for i in 0..tev_orders.len() {
            tev_orders[i] = IndirectTevOrder::read(buf)?;
        }

        let mut matrices = [Default::default(); 3];
        for i in 0..matrices.len() {
            matrices[i] = IndirectTexMatrix::read(buf)?;
        }

        let mut scales = [Default::default(); 4];
        for i in 0..scales.len() {
            scales[i] = IndirectTexScale::read(buf)?;
        }

        let mut stages = [Default::default(); 16];
        for i in 0..stages.len() {
            stages[i] = IndirectTevStage::read(buf)?;
        }

        Ok(Self {
            indirect_lookup,
            num_stages,
            tev_orders,
            matrices,
            scales,
            stages,
        })
    }

    pub fn write_bmd<W: Write + Seek>(&self, w: &mut W) -> Result<(), BmdWriteError> {
        wbool!(w, self.indirect_lookup)?;
        w!(w, self.num_stages)?;

        w!(w, 0xFFFFu16)?;

        for order in self.tev_orders.iter() {
            order.write_bmd(w)?;
        }

        for mat in self.matrices.iter() {
            mat.write_bmd(w)?;
        }

        for scale in self.scales.iter() {
            scale.write_bmd(w)?;
        }

        for stage in self.stages.iter() {
            stage.write_bmd(w)?;
        }

        Ok(())
    }
}
