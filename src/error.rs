use crate::gc::bmd::error::{BmdWriteError, GltfReadError};
use crate::gc::bti::error::{BtiWriteError, PngReadError};
use crate::gc::error::IsoWriteError;
use crate::{
    companion::error::CompanionError,
    gc::{
        bti::error::PngWriteError,
        error::ReadError,
        types::mesh::{GxAttribute, PrimitiveKind},
    },
};
use displaydoc::Display;
use num_enum::TryFromPrimitiveError;
use std::{io, path::PathBuf};
use thiserror::Error;

#[derive(Display, Error, Debug)]
pub enum GltfError {
    /// Failed to read: {0}
    ReadError(#[from] ReadError),
    /// Failed to save texture: {0}
    PngError(#[from] PngWriteError),
    /// Failed to serialize: {0}
    SerdeError(#[from] serde_json::Error),
    /// Failed to save file: {0}
    IoError(#[from] io::Error),
    /// Failed to write companion file: {0}
    CompanionError(#[from] CompanionError),
    /// File has invalid name: {0}
    InvalidFileName(PathBuf),
    /// DRW1 did not contain matrix at index {0}
    InvalidDrw1Matrix(usize),
    /// EVP1 did not contain weights at index {0}
    InvalidEvp1Weights(u16),
    /// EVP1 did not contain matrix at index {0}
    InvalidEvp1Matrix(usize),
    /// JNT1 did not contain bone at index {0}
    InvalidJnt1Bone(usize),
    /// TEX1 did not contain texture at index {0} for material {1}
    InvalidMaterialTex(usize, String),
    /// Attribute not found: {0}
    NoAttribute(&'static str),
    /// No buffer view to replace at index {0}
    NoBufferViewById(usize),
    /// No buffer view to replace by name {0}
    NoBufferViewByName(String),
    /// Buffer view {0} reserved with length {1} but needs {2}
    BufferViewReservedWrong(String, usize, usize),
    /// No cached vert found with key: {0:?}
    NoProcessedVert((usize, usize)),
    /// Tried to access insane Tex channel while setting coords
    InvalidTexChanForCoords(TryFromPrimitiveError<GxAttribute>),
    /// Tried to access insane Tex channel while creating accessors
    InvalidTexChanForAccessors(TryFromPrimitiveError<GxAttribute>),
    /// Tried to access insane Color channel while creating accessors
    InvalidColorChan(TryFromPrimitiveError<GxAttribute>),
    /// Encountered invalid data type for {0}: expected {1} got {2}
    InvalidDataVec(&'static str, &'static str, String),
    /// Root did not generate a node_id
    RootNodeFailed,
    /// Could not retrieve node in INF1 at index {0}
    NoInf1Node(u16),
    /// Could not retrieve node in JNT1 at index {0}
    NoJnt1Node(u16),
    /// Could not retrieve node in SHP1 at index {0}
    NoShp1Node(u16),
    /// Could not retrieve node in MAT3 at index {0}
    NoMat3Node(u16),
    /// Unimplemented triangle type found: {0:?}
    UnimplementedTriType(PrimitiveKind),
}

#[derive(Display, Error, Debug)]
pub enum BuildError {
    /// IO Error: file: {0}, equivalent: {1}, error: {2}
    IoError(PathBuf, PathBuf, io::Error),
    /// Failed to build BMD file from glTF: {0}
    GltfReadError(#[from] GltfReadError),
    /// Failed to write BMD: {0}
    BmdWriteError(#[from] BmdWriteError),
    /// Failed to build BTI file from PNG: {0}
    PngReadError(#[from] PngReadError),
    /// Failed to write BTI: {0}
    BtiWriteError(#[from] BtiWriteError),
    /// Failed to write RARC metadata: {0}
    CompanionError(#[from] CompanionError),
    /// Failed to write ISO: {0}
    IsoWriteError(#[from] IsoWriteError),
}
