use bevy_egui::egui::Ui;

use crate::tabs::Tab;

pub const TITLE: &str = "Rails";

#[derive(Default, Debug, Clone, PartialEq)]
pub struct Rails;

impl Rails {
    pub fn ui(&mut self, _ui: &mut Ui, _res: ()) {}
}

impl Tab for Rails {
    fn title(&self) -> &'static str {
        TITLE
    }
}
