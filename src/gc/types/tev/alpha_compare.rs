use std::io::{Read, Seek, SeekFrom, Write};

use serde::{Deserialize, Serialize};

use crate::{
    bytes::read_u8,
    gc::{
        bmd::error::BmdWriteError,
        error::ReadError,
        types::tev::{CompareKind, CompareOp},
    },
    try_from_repr, w, wraw,
};

/// GX_SetAlphaCompare - Sets the parameters for the alpha compare function which uses the alpha
/// output from the last active TEV stage.
///
/// The alpha compare operation is:
///     alpha_pass = (alpha_src(comp0)value0) (op) (alpha_src(comp1)value1)
///
/// where alpha_src is the alpha from the last active TEV stage.
#[derive(Debug, Copy, Clone, Default, PartialEq, Serialize, Deserialize)]
pub struct AlphaCompare {
    pub comp_0: CompareKind,
    pub value_0: u8,
    pub op: CompareOp,
    pub comp_1: CompareKind,
    pub value_1: u8,
}

impl AlphaCompare {
    /// Advances position
    pub fn read<R: Read + Seek>(buf: &mut R) -> Result<Self, ReadError> {
        let comp_0 = read_u8(buf)?;
        let value_0 = read_u8(buf)?;
        let op = read_u8(buf)?;
        let comp_1 = read_u8(buf)?;
        let value_1 = read_u8(buf)?;

        // Skip padding (probably byte + short)
        buf.seek(SeekFrom::Current(3))?;

        Ok(Self {
            comp_0: try_from_repr!(comp_0 => CompareKind)?,
            value_0,
            op: try_from_repr!(op => CompareOp)?,
            comp_1: try_from_repr!(comp_1 => CompareKind)?,
            value_1,
        })
    }

    pub fn write_bmd<W: Write + Seek>(&self, w: &mut W) -> Result<(), BmdWriteError> {
        w!(w, self.comp_0 as u8)?;
        w!(w, self.value_0)?;
        w!(w, self.op as u8)?;
        w!(w, self.comp_1 as u8)?;
        w!(w, self.value_1)?;

        wraw!(w, [0xFF, 0xFF, 0xFF])?;

        Ok(())
    }
}
