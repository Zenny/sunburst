use std::io::{self, BufRead, Read, Seek, SeekFrom, Write};

use bevy::math::{EulerRot, Quat, Vec3};
use num_enum::TryFromPrimitive;

use crate::{
    bytes::{read_f32, read_i16, read_u16, read_u32, read_u8},
    gc::{
        align::{write_align, AlignType},
        bmd::error::BmdWriteError,
        error::ReadError,
    },
    util::TotalEq,
    util::{DEG180_TO_I16ROT, I16ROT_TO_DEG180},
    w, wat, wbool, wraw,
};

const JNT1: u32 = 0x4A4E5431;

#[derive(Debug, Copy, Clone, TryFromPrimitive, PartialEq)]
#[repr(u16)]
pub enum MatrixKind {
    Standard = 0,
    // todo assumed this is XY
    BillboardXY = 1,
    BillboardY = 2,
}

#[derive(Debug, Clone)]
pub struct BoneData {
    pub name: String,
    pub config: Config,
}

#[derive(Debug, Clone)]
pub struct Config {
    pub matrix_kind: MatrixKind,
    // Could be some kind of matrixkind, but it often relates to the name of the bones and looks
    // like some kind of bone type (M_bone = 0, eff_bone = 2, chn_bone = 2, jnt_bone = 1)
    // todo implement a bit of an automatic system around this for reading from gltf
    pub flags: u8,
    /// If this is true, child bones are scaled 1/x when editing
    pub compensate_scale: bool,
    pub scale: Vec3,
    pub rotation: Quat,
    pub translate: Vec3,
    pub bounding_sphere_radius: f32,
    pub bounding_box_min: Vec3,
    pub bounding_box_max: Vec3,
}

impl Config {
    pub fn new<R: Read + Seek>(buf: &mut R, offset: u64) -> Result<Self, ReadError> {
        buf.seek(SeekFrom::Start(offset))?;

        // flags also contain matrix type, so we split it
        let flags_and_type = read_u16(buf)?;
        let flags = (flags_and_type & 0x0F) as u8;

        let potential_kind = flags_and_type >> 4;
        let matrix_kind = MatrixKind::try_from_primitive(potential_kind)
            .map_err(|_| ReadError::InvalidMatrixKind(potential_kind))?;

        let compensate_scale = read_u8(buf)?;

        // Skip padding
        buf.seek(SeekFrom::Current(1))?;

        let scale = Vec3::new(read_f32(buf)?, read_f32(buf)?, read_f32(buf)?);

        let x = (read_i16(buf)? as f32 * I16ROT_TO_DEG180).to_radians();
        let y = (read_i16(buf)? as f32 * I16ROT_TO_DEG180).to_radians();
        let z = (read_i16(buf)? as f32 * I16ROT_TO_DEG180).to_radians();

        let rotation = Quat::from_axis_angle(Vec3::new(0.0, 0.0, 1.0), z)
            * Quat::from_axis_angle(Vec3::new(0.0, 1.0, 0.0), y)
            * Quat::from_axis_angle(Vec3::new(1.0, 0.0, 0.0), x);

        // Skip padding
        buf.seek(SeekFrom::Current(2))?;

        let translation = Vec3::new(read_f32(buf)?, read_f32(buf)?, read_f32(buf)?);

        let bounding_sphere_radius = read_f32(buf)?;
        let bounding_box_min = Vec3::new(read_f32(buf)?, read_f32(buf)?, read_f32(buf)?);
        let bounding_box_max = Vec3::new(read_f32(buf)?, read_f32(buf)?, read_f32(buf)?);

        Ok(Self {
            matrix_kind,
            flags,
            compensate_scale: if compensate_scale == 0xff {
                false
            } else {
                compensate_scale != 0
            },
            scale,
            rotation,
            translate: translation,
            bounding_sphere_radius,
            bounding_box_min,
            bounding_box_max,
        })
    }

    pub fn write_bmd<W: Write + Seek>(&self, w: &mut W) -> Result<(), BmdWriteError> {
        let kind_flags = (self.matrix_kind as u16) << 4 | self.flags as u16;
        w!(w, kind_flags)?;

        wbool!(w, self.compensate_scale)?;

        w!(w, 0xFFu8)?;

        w!(w, self.scale.x)?;
        w!(w, self.scale.y)?;
        w!(w, self.scale.z)?;

        let (z, y, x) = self.rotation.to_euler(EulerRot::ZYX);
        let (x, y, z) = (
            x.to_degrees().clamp(-180.0, 179.9999) * DEG180_TO_I16ROT,
            y.to_degrees().clamp(-180.0, 179.9999) * DEG180_TO_I16ROT,
            z.to_degrees().clamp(-180.0, 179.9999) * DEG180_TO_I16ROT,
        );
        w!(w, x as i16)?;
        w!(w, y as i16)?;
        w!(w, z as i16)?;

        w!(w, 0xFFFFu16)?;

        w!(w, self.translate.x)?;
        w!(w, self.translate.y)?;
        w!(w, self.translate.z)?;

        w!(w, self.bounding_sphere_radius)?;

        w!(w, self.bounding_box_min.x)?;
        w!(w, self.bounding_box_min.y)?;
        w!(w, self.bounding_box_min.z)?;

        w!(w, self.bounding_box_max.x)?;
        w!(w, self.bounding_box_max.y)?;
        w!(w, self.bounding_box_max.z)?;

        Ok(())
    }

    pub fn get_bounding_box_dims(&self) -> Vec3 {
        self.bounding_box_max - self.bounding_box_min
    }
}

impl PartialEq for Config {
    fn eq(&self, other: &Self) -> bool {
        self.compensate_scale == other.compensate_scale
            && self
                .bounding_sphere_radius
                .total_eq(&other.bounding_sphere_radius)
            && self.rotation.total_eq(&other.rotation)
            && self.flags == other.flags
            && self.scale.total_eq(&other.scale)
            && self.bounding_box_max.total_eq(&other.bounding_box_max)
            && self.bounding_box_min.total_eq(&other.bounding_box_min)
            && self.matrix_kind == other.matrix_kind
            && self.translate.total_eq(&other.translate)
    }
}

/// This seeks, so you should re-seek to where you want to go after
pub fn read_name_table<R: Read + Seek + BufRead>(
    buf: &mut R,
    offset: u64,
) -> Result<Vec<String>, ReadError> {
    buf.seek(SeekFrom::Start(offset))?;

    let count = read_u16(buf)? as usize;
    // Skip padding
    buf.seek(SeekFrom::Current(2))?;

    let mut out = Vec::with_capacity(count);

    for _ in 0..count {
        let _hash = read_u16(buf)?;
        let name_offset = read_u16(buf)?;
        let pos = buf.stream_position()?;

        buf.seek(SeekFrom::Start(offset + name_offset as u64))?;

        let mut name = Vec::new();
        buf.read_until(0, &mut name)?;
        // pop zero off the end
        name.pop();
        // todo check if changing the name breaks anything on recompile
        out.push(String::from_utf8_lossy(&name).replace("�", "#"));

        buf.seek(SeekFrom::Start(pos))?;
    }

    Ok(out)
}

pub fn write_name_table<W: Write + Seek>(w: &mut W, names: Vec<String>) -> Result<(), io::Error> {
    let start = w.stream_position()?;
    w!(w, names.len() as u16)?;

    w!(w, 0xFFFFu16)?;

    let table_pos = start + 4;
    for _ in 0..names.len() {
        // Reserve space for each string's table entry (4 bytes (hash + offset))
        w!(w, 0u32)?;
    }

    for (i, mut s) in names.into_iter().enumerate() {
        let offset = (w.stream_position()? - start) as u16;
        // Calc hash before adding 0
        let hash = hash_name(&s);

        // add trailing 0
        s.push('\0');
        wraw!(w, s.as_bytes())?;

        let our_table_pos = table_pos + (i as u64 * 4);
        wat!(w, hash, our_table_pos);
        wat!(w, offset, our_table_pos + 2);
    }

    Ok(())
}

pub fn hash_name(s: &'_ str) -> u16 {
    let mut hash: u16 = 0;

    for c in s.chars() {
        hash = hash.wrapping_mul(3);
        hash = hash.wrapping_add(c as u16);
    }

    hash
}

// todo verify remap table impl
/// Returns a deduplicated vec of items that the remaps point to.
/// The items argument should be the original set of items.
pub fn create_remap_table<T: Clone + PartialEq>(items: Vec<&T>) -> (Vec<u16>, Vec<T>) {
    let mut dedup: Vec<T> = Vec::with_capacity(items.len());
    let mut remaps = Vec::with_capacity(items.len());

    //  create dedup array
    for item in items.into_iter() {
        if let Some(pos) = dedup.iter().position(|it| it == item) {
            // If it already exists, we dish out the remapped pos
            remaps.push(pos as u16);
        } else {
            // Otherwise, we add the new entry and write its pos
            remaps.push(dedup.len() as u16);
            dedup.push(item.clone());
        }
    }

    (remaps, dedup)
}

#[derive(Debug, Clone)]
pub struct Jnt1 {
    pub len: u32,
    pub bone_data: Vec<BoneData>,
}

impl Jnt1 {
    pub fn new<R: Read + Seek + BufRead>(buf: &mut R, jnt1_offset: u32) -> Result<Self, ReadError> {
        let jnt1 = read_u32(buf)?;
        // JNT1
        if jnt1 != JNT1 {
            return Err(ReadError::ReadCheck("JNT1"));
        }

        let jnt1_len = read_u32(buf)?;
        // num of envelope matrices
        let data_count = read_u16(buf)? as usize;

        // Skip padding
        buf.seek(SeekFrom::Current(2))?;

        let config_table_offset = read_u32(buf)?;
        let remap_table_offset = read_u32(buf)?;
        let name_table_offset = read_u32(buf)?;

        let names = read_name_table(buf, (name_table_offset + jnt1_offset) as u64)?;

        buf.seek(SeekFrom::Start((remap_table_offset + jnt1_offset) as u64))?;

        // remaps seem to act as a form of compression,
        // if the settings are the same (except the names)
        let mut remaps = Vec::with_capacity(data_count);
        for _ in 0..data_count {
            remaps.push(read_u16(buf)?);
        }

        let mut bone_data = Vec::with_capacity(data_count);

        for i in 0..data_count {
            let bd = BoneData {
                name: names[i].clone(),
                config: Config::new(
                    buf,
                    (jnt1_offset + config_table_offset + (remaps[i] as u32 * 0x40)) as u64,
                )?,
            };

            // The remaps tell us which config to look at
            bone_data.push(bd);
        }

        Ok(Self {
            len: jnt1_len,
            bone_data,
        })
    }

    pub fn write_bmd<W: Write + Seek>(&self, w: &mut W) -> Result<(), BmdWriteError> {
        let start = w.stream_position()?;
        w!(w, JNT1)?;

        let length_offset = w.stream_position()?;
        w!(w, 0u32)?;

        w!(w, self.bone_data.len() as u16)?;

        w!(w, 0xFFFFu16)?;

        let config_table_offset = w.stream_position()?;
        w!(w, 0u32)?;
        let remap_table_offset = w.stream_position()?;
        w!(w, 0u32)?;
        let name_table_offset = w.stream_position()?;
        w!(w, 0u32)?;

        let configs = self.bone_data.iter().map(|b| &b.config).collect::<Vec<_>>();
        let (remaps, dedup) = create_remap_table(configs);

        // This is always 24
        wat!(w, 24u32, config_table_offset);

        for item in dedup {
            item.write_bmd(w)?;
        }

        let here = w.stream_position()?;
        wat!(w, (here - start) as u32, remap_table_offset);

        for r in remaps {
            w!(w, r)?;
        }

        write_align(w, 4, AlignType::String)?;

        let here = w.stream_position()?;
        wat!(w, (here - start) as u32, name_table_offset);

        let names = self
            .bone_data
            .iter()
            .map(|b| b.name.clone())
            .collect::<Vec<_>>();
        write_name_table(w, names)?;

        write_align(w, 32, AlignType::String)?;

        let len = (w.stream_position()? - start) as u32;
        wat!(w, len, length_offset);

        Ok(())
    }
}
