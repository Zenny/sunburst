#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")] // hide console window on Windows in release

use std::{fs, fs::write, path::PathBuf};
use std::fs::{create_dir_all, read};
use std::io::Cursor;

use bevy::{prelude::*, window::PresentMode, winit::WinitWindows};
use bevy_egui::{
    egui::{self, epaint::Shadow, Color32, Grid, Margin, Pos2, Rounding, Ui},
    EguiContexts, EguiPlugin,
};
use egui_dock::{DockArea, NodeIndex, TabStyle, Tree};
use flexi_logger::{Cleanup, Criterion, Duplicate, FileSpec, Naming};
use winit::window::Icon;
use yaz0::Yaz0Archive;

use crate::companion::rarc::RarcCompanion;
use crate::{
    gc::iso::IsoWorker,
    project::Project,
    tabs::{explorer, properties, rails, scene, scene_hierarchy, tables, Tabs},
    ui_data::{BlockingAction, UiData, LOGO_KEY},
};
use crate::gc::rarc::Rarc;

pub mod bytes;
pub mod companion;
pub mod error;
pub mod gc;
pub mod gltf;
pub mod project;
pub mod tabs;
pub mod types;
pub mod ui_data;
pub mod util;

pub const PADDING: f32 = 5.0;
pub const TWOPAD: f32 = PADDING * 2.0;
pub const ALIGN_SPACE: &str =
    "                                                                                 ";
pub const PROJECT_FILE: &str = "_project.sbp";
const LICENSE_NOTICE: &str = r#"Sunburst Copyright (C) 2023 Zenny3D
                This program comes with ABSOLUTELY NO WARRANTY.
                This is free software, and you are welcome to redistribute it
                under the conditions of the GNU GPLv3 license."#;

#[derive(Resource)]
struct TabTree {
    tree: Tree<Tabs>,
}

impl Default for TabTree {
    fn default() -> Self {
        let mut tree = Tree::new(vec![Tabs::scene()]);

        let [a, _] = tree.split_below(NodeIndex::root(), 0.65, vec![Tabs::explorer()]);
        let [a, _] = tree.split_left(
            a,
            0.19,
            vec![Tabs::scene_hierarchy(), Tabs::rails(), Tabs::tables()],
        );
        let [_, _] = tree.split_right(a, 0.65, vec![Tabs::properties()]);

        Self { tree }
    }
}

fn set_window_icon(
    // we have to use `NonSend` here
    windows: NonSend<WinitWindows>,
) {
    // here we use the `image` crate to load our icon data from a png file
    // this is not a very bevy-native solution, but it will do
    let (icon_rgba, icon_width, icon_height) = {
        let image = image::open("resources/blueprint_rocket_sm.png")
            .expect("Failed to open icon path")
            .into_rgba8();
        let (width, height) = image.dimensions();
        let rgba = image.into_raw();
        (rgba, width, height)
    };

    let icon = Icon::from_rgba(icon_rgba, icon_width, icon_height).unwrap();
    for (_, window) in windows.windows.iter() {
        window.set_window_icon(Some(icon.clone()));
    }
}

fn main() {
    flexi_logger::Logger::try_with_env_or_str("error,sunburst=debug")
        .expect("Failed to setup logger")
        .log_to_file(FileSpec::default().directory("logs"))
        .rotate(
            Criterion::Size(10 * 1024 * 1024), // Rotate after 10MB
            Naming::Timestamps,
            Cleanup::KeepLogAndCompressedFiles(3, 5), // Keep up to 3 log files and 5 compressed
        )
        .duplicate_to_stderr(Duplicate::All)
        .start()
        .expect("Failed to start logger");

    log::info!("{}", LICENSE_NOTICE);
    let path = PathBuf::from("tmp/airport0.szs");
    let t = PathBuf::from("tmp/test");
    let data = read(&path).unwrap();
    let buf = Cursor::new(data);
    let decomp = Yaz0Archive::new(buf).unwrap().decompress().unwrap();
    create_dir_all(t.join("tmp/airport0.szs")).unwrap();
    let ap0 = Rarc::read_szs(path, decomp, None).unwrap();
    for file in ap0.files.iter() {
        log::info!("CREATING {:?}", file.path());
        file.create_relative(&t).unwrap();
    }


    /*let path = PathBuf::from("tmp/model/sun_lensfx.bmd");
    let m = fs::read(&path).expect("m");
    let gltf = path.parent().unwrap();
    let mut c = Cursor::new(m);
    let time = std::time::Instant::now();
    let b = Bmd::new(path.clone(), &mut c, None).expect("b");
    let stop = time.elapsed();
    log::info!("parse took {}", stop.as_secs_f64());
    let time = std::time::Instant::now();
    b.write_gltf(&gltf.to_path_buf()).expect("oops");
    let stop = time.elapsed();
    log::info!("write took {}", stop.as_secs_f64());

    let gltf_file = gltf.join("sun_lensfx.gltf");
    let g_in = Bmd::read_gltf(gltf_file).expect("big err");
    let outbmd = PathBuf::from("tmp/model/sun_lensfx_new.bmd");
    let mut wbmd = Vec::with_capacity(20480);
    let mut cbmd = Cursor::new(&mut wbmd);
    g_in.write_bmd(&mut cbmd).unwrap();
    write(&outbmd, wbmd).unwrap();*/

    /*let m = fs::read(&outbmd).expect("mm");
    let mut c = Cursor::new(m);
    let b2 = Bmd::new(outbmd.clone(), &mut c, None).expect("b2");
    b2.write_gltf(&gltf.to_path_buf()).unwrap();*/

    /*
        let outbti = PathBuf::from("H:\\#Projects\\#SMS\\editor\\sunburst\\tmp\\model\\_yoshi_model_tex\\H_yoshi_eye01_cc_16rgba.bti");
        let r = std::fs::read(&outbti).expect("sdfbhdtfsdoj");
        let mut ccc = Cursor::new(r);
        let scn = Bti::new(outbti.clone(), &mut ccc, 0).expect("ghjyiuyht");
        let outagain = scn
            .write_png(&outbti.parent().unwrap().to_path_buf())
            .expect("56ydiut");

        let test_img = PathBuf::from(
            "H:\\#Projects\\#SMS\\editor\\sunburst\\tmp\\model\\_yoshi_model_tex\\H_yoshi_eye01_cc_16rgba.png",
        );
        let mut p = Bti::read_png(test_img).expect("oop");
        p.path.set_file_name("NEW_H_yoshi_eye01_cc_16rgba.bti");
        let outbti =
            PathBuf::from("H:\\#Projects\\#SMS\\editor\\sunburst\\tmp\\model\\_yoshi_model_tex\\");
        p.write_bti(&outbti).expect("agbd");
        let r = std::fs::read(&p.path).expect("aruhbsdok");
        let mut ccc = Cursor::new(r);
        let scn = Bti::new(p.path.clone(), &mut ccc, 0).expect("eaiuhf");
        let outagain = scn.write_png(&outbti).expect("asfiuh");
    */
    App::new()
        .add_plugins(DefaultPlugins.set(WindowPlugin {
            primary_window: Some(Window {
                title: format!("Sunburst v{}", clap::crate_version!()),
                resolution: (1366.0, 768.0).into(),
                present_mode: PresentMode::AutoVsync,
                // Tells wasm to resize the window according to the available canvas
                fit_canvas_to_parent: true,
                // Tells wasm not to override default event handling, like F5, Ctrl+R etc.
                prevent_default_event_handling: false,
                focused: true,
                resizable: true,
                ..default()
            }),
            ..default()
        }))
        .add_plugins(EguiPlugin)
        .init_resource::<UiData>()
        .init_resource::<TabTree>()
        .add_systems(Startup, set_window_icon)
        .add_systems(Update, ui)
        .run();
}

fn ui(
    mut contexts: EguiContexts,
    time: Res<Time>,
    mut ui_data: ResMut<UiData>,
    mut tab_tree: ResMut<TabTree>,
) {
    let ctx = contexts.ctx_mut();
    ui_data.init_textures(ctx);

    // Get current context style
    let mut style = (*ctx.style()).clone();

    style.visuals.menu_rounding = Rounding::same(1.0);
    style.visuals.window_rounding = Rounding::same(1.0);
    style.visuals.window_shadow = Shadow {
        extrusion: 6.0,
        color: Color32::from_black_alpha(96),
    };
    style.visuals.popup_shadow = Shadow {
        extrusion: 6.0,
        color: Color32::from_black_alpha(96),
    };
    style.visuals.widgets.noninteractive.bg_fill = Color32::from_gray(48);
    style.visuals.widgets.noninteractive.bg_stroke.color = Color32::from_gray(16);
    style.visuals.widgets.noninteractive.bg_stroke.width = 0.5;
    style.visuals.window_fill = style.visuals.widgets.noninteractive.bg_fill;
    style.visuals.window_stroke = style.visuals.widgets.noninteractive.bg_stroke;
    style.spacing.menu_margin = Margin::same(3.0);
    style.spacing.item_spacing = egui::emath::Vec2::new(4.0, 4.0);
    style.spacing.button_padding = egui::emath::Vec2::new(5.0, 5.0);
    style.spacing.window_margin.right = 100.0;

    let top_frame = egui::containers::Frame {
        inner_margin: Margin::same(0.0),
        outer_margin: Default::default(),
        rounding: Rounding::none(),
        shadow: Shadow::NONE,
        fill: style.visuals.widgets.noninteractive.bg_fill,
        stroke: style.visuals.widgets.noninteractive.bg_stroke,
    };

    ctx.set_style(style.clone());

    if ui_data.show_about {
        let about_frame = egui::containers::Frame {
            inner_margin: Margin::symmetric(0.0, 10.0),
            outer_margin: Default::default(),
            rounding: Rounding::none(),
            shadow: Shadow {
                extrusion: 6.0,
                color: Color32::from_black_alpha(96),
            },
            fill: style.visuals.widgets.noninteractive.bg_fill,
            stroke: style.visuals.widgets.noninteractive.bg_stroke,
        };
        egui::Window::new("About")
            .frame(about_frame)
            .resizable(false)
            .collapsible(false)
            .title_bar(true)
            .show(ctx, |ui| {
                ui.vertical_centered(|ui| {
                    let texture = ui_data.textures.get(LOGO_KEY).unwrap();
                    ui.image(texture, texture.size_vec2());
                    ui.heading(format!("Sunburst v{}", clap::crate_version!()));
                    ui.label("Super Mario Sunshine Edtior");
                    ui.label("🔆 Zenny (c) 2023");
                    ui.separator();
                    ui.heading("Thank You!");
                    ui.label("bsv, fuzziqersoftware, impiaaa, MasterF0x,");
                    ui.label("and everyone else who wrote GCN or SMS tooling!");
                    ui.separator();
                    if ui.button("Close").clicked() {
                        ui_data.show_about = false;
                    }
                });
            });
    }

    // Above any block that sets ui_data.blocking so it doesn't set it off a frame early
    if let Some(block) = ui_data.blocking.action.clone() {
        let new_frame = egui::containers::Frame {
            inner_margin: Margin::symmetric(10.0, 10.0),
            outer_margin: Default::default(),
            rounding: Rounding::none(),
            shadow: Shadow {
                extrusion: 6.0,
                color: Color32::from_black_alpha(96),
            },
            fill: style.visuals.widgets.noninteractive.bg_fill,
            stroke: style.visuals.widgets.noninteractive.bg_stroke,
        };
        egui::Window::new("Loading...")
            .frame(new_frame)
            .resizable(false)
            .collapsible(false)
            .title_bar(true)
            .fixed_pos(Pos2::new(20.0, 20.0))
            .movable(false)
            .show(ctx, |ui| {
                ui.vertical_centered(|ui| {
                    let s = format!("{}", block);
                    let sel_iso = ui_data.sel_iso.clone();
                    let sel_folder = ui_data.sel_folder.clone();
                    if ui_data
                        .blocking
                        .handle
                        .get_or_insert_with(move || {
                            std::thread::spawn(move || match block {
                                BlockingAction::CreateProj => {
                                    let unpacked = IsoWorker::read(&sel_iso).expect("read failed");
                                    for file in unpacked.files {
                                        file.create_relative(&sel_folder).expect("iso dump bad");
                                    }

                                    write(sel_folder.join(PROJECT_FILE), "{}")
                                        .expect("failed write project file");
                                }
                            })
                        })
                        .is_finished()
                    {
                        ui_data.proj = Some(Project::new(ui_data.sel_folder.clone()));
                        ui_data.blocking.action = None;
                        ui_data.blocking.handle = None;
                        ui_data.sel_folder = PathBuf::new();
                        ui_data.rendered_path = Default::default();
                    };
                    ui_data.sel_iso = PathBuf::new();
                    ui.label(s);
                });
            });
    }

    if ui_data.show_new {
        let new_frame = egui::containers::Frame {
            inner_margin: Margin::symmetric(10.0, 10.0),
            outer_margin: Default::default(),
            rounding: Rounding::none(),
            shadow: Shadow {
                extrusion: 6.0,
                color: Color32::from_black_alpha(96),
            },
            fill: style.visuals.widgets.noninteractive.bg_fill,
            stroke: style.visuals.widgets.noninteractive.bg_stroke,
        };
        egui::Window::new("New Project")
            .frame(new_frame)
            .resizable(false)
            .collapsible(false)
            .title_bar(true)
            .fixed_pos(Pos2::new(20.0, 20.0))
            .movable(false)
            .show(ctx, |ui| {
                Grid::new("np_grid").show(ui, |ui| {
                    ui.label("ISO File: ");
                    let s = ui_data.sel_iso.to_string_lossy();
                    let cs = s.split_at(s.len().saturating_sub(40)).1;
                    ui.label(if cs.len() > 0 {
                        format!("{}{}", if s.len() > 40 { "..." } else { "" }, cs)
                    } else {
                        String::new()
                    });
                    if ui.button("Select...").clicked() {
                        if let Some(file) = rfd::FileDialog::new()
                            .add_filter("ISO File", &["iso", "nkit.iso"])
                            .pick_file()
                        {
                            ui_data.sel_iso = file;
                        }
                    }
                    ui.end_row();

                    ui.label("Project Folder: ");
                    let s = ui_data.sel_folder.to_string_lossy();
                    let cs = s.split_at(s.len().saturating_sub(40)).1;
                    ui.label(if cs.len() > 0 {
                        format!("{}{}", if s.len() > 40 { "..." } else { "" }, cs)
                    } else {
                        ALIGN_SPACE.to_string()
                    });
                    if ui.button("Select...").clicked() {
                        if let Some(folder) = rfd::FileDialog::new().pick_folder() {
                            ui_data.sel_folder = folder;
                        }
                    }
                    ui.end_row();
                    ui.end_row();

                    if ui.button("Create").clicked() {
                        if !ui_data.sel_folder.exists() {
                            // todo should be user err string
                            fs::create_dir_all(&ui_data.sel_folder)
                                .expect("Failed create dir somehow");
                        }
                        if !ui_data.sel_iso.exists() {
                            // todo should be user err string
                            unimplemented!("iso");
                        }

                        ui_data.proj = None;
                        ui_data.blocking.action = Some(BlockingAction::CreateProj);
                        ui_data.show_new = false;
                    }
                    ui.label("");
                    if ui.button("Cancel").clicked() {
                        ui_data.show_new = false;
                        ui_data.sel_iso = PathBuf::new();
                        ui_data.sel_folder = PathBuf::new();
                    }
                });
            });
    }

    egui::panel::TopBottomPanel::top("top_panel")
        .frame(top_frame)
        .show(ctx, |ui| {
            // The top panel is often a good place for a menu bar:
            egui::menu::bar(ui, |ui| {
                let mut s = style.clone();
                s.visuals.widgets.inactive.weak_bg_fill = Color32::from_gray(48);
                s.visuals.widgets.inactive.bg_fill = Color32::from_gray(48);
                s.visuals.widgets.active.bg_stroke.width = 0.0;
                s.visuals.widgets.hovered.bg_stroke.width = 0.0;
                s.visuals.widgets.hovered.rounding = Rounding::none();
                s.visuals.widgets.active.rounding = Rounding::none();
                s.visuals.widgets.inactive.rounding = Rounding::none();
                ctx.set_style(s);
                egui::menu::menu_button(ui, "File", |ui| {
                    if ui.button("New Project...").clicked() {
                        ui_data.show_new = true;
                        ui.close_menu();
                    }
                    if ui.button("Open Project...").clicked() {
                        if let Some(file) = rfd::FileDialog::new()
                            .add_filter("SBP File", &["sbp"])
                            .pick_file()
                        {
                            ui_data.rendered_path = Default::default();
                            ui_data.proj = Some(Project::new(file.parent().unwrap().to_path_buf()));
                        }
                        ui.close_menu();
                    }
                    if ui.button("Save").clicked() {}
                    if ui.button("Save as...").clicked() {}
                    if ui.button("Export Patch...").clicked() {
                        // export SPF file
                    }
                    if ui.button("Quit").clicked() {
                        std::process::exit(0);
                    }
                });
                egui::menu::menu_button(ui, "Edit", |ui| {
                    if ui.button("Undo").clicked() {}
                    if ui.button("Redo").clicked() {}
                });
                egui::menu::menu_button(ui, "Project", |ui| {
                    if ui.button("Build").clicked() {
                        if let Some(proj) = &ui_data.proj {
                            proj.build().expect("uh oh");
                            log::info!("Build complete");
                        }
                    }
                    if ui.button("Play").clicked() {}
                    if ui.button("Play From Current Level").clicked() {}
                });
                egui::menu::menu_button(ui, "View", |ui| {
                    ui.menu_button("Window", |ui| {
                        if ui.button(explorer::TITLE).clicked() {
                            try_add_tab(&mut tab_tree.tree, Tabs::explorer());
                            ui.close_menu();
                        }
                        if ui.button(properties::TITLE).clicked() {
                            try_add_tab(&mut tab_tree.tree, Tabs::properties());
                            ui.close_menu();
                        }
                        if ui.button(scene::TITLE).clicked() {
                            try_add_tab(&mut tab_tree.tree, Tabs::scene());
                            ui.close_menu();
                        }
                        if ui.button(scene_hierarchy::TITLE).clicked() {
                            try_add_tab(&mut tab_tree.tree, Tabs::scene_hierarchy());
                            ui.close_menu();
                        }
                        if ui.button(rails::TITLE).clicked() {
                            try_add_tab(&mut tab_tree.tree, Tabs::rails());
                            ui.close_menu();
                        }
                        if ui.button(tables::TITLE).clicked() {
                            try_add_tab(&mut tab_tree.tree, Tabs::tables());
                            ui.close_menu();
                        }
                    });

                    if ui
                        .button(if ui_data.show_fps {
                            "Hide FPS"
                        } else {
                            "Show FPS"
                        })
                        .clicked()
                    {
                        ui_data.show_fps = !ui_data.show_fps;
                        ui.close_menu();
                    }
                });
                egui::menu::menu_button(ui, "Help", |ui| {
                    if ui.button("About").clicked() {
                        ui_data.show_about = true;
                        ui.close_menu();
                    }
                });
                ctx.set_style(style);
            });
        });

    let time = if ui_data.show_fps { Some(time) } else { None };

    let tabs_open = tab_tree.tree.num_tabs();

    DockArea::new(&mut tab_tree.tree)
        .style(egui_dock::Style::from_egui(ctx.style().as_ref()))
        .show(
            ctx,
            &mut TabViewer {
                time,
                tabs_open,
                ui_data: ui_data.as_mut(),
            },
        );
}

fn try_add_tab(tree: &mut Tree<Tabs>, tab: Tabs) {
    if !tree.tabs().any(|t| *t == tab) {
        tree.push_to_focused_leaf(tab);
    }
}

struct TabViewer<'a> {
    time: Option<Res<'a, Time>>,
    tabs_open: usize,
    ui_data: &'a mut UiData,
}

impl<'a> egui_dock::TabViewer for TabViewer<'a> {
    type Tab = Tabs;

    fn ui(&mut self, ui: &mut Ui, tab: &mut Self::Tab) {
        match tab {
            Tabs::Scene(t) => t.ui(ui, self.time.as_ref().map(|time| time.delta_seconds_f64())),
            Tabs::SceneHierarchy(t) => t.ui(ui, ()),
            Tabs::Explorer(t) => t.ui(ui, self.ui_data),
            Tabs::Properties(t) => t.ui(ui, ()),
            Tabs::Rails(t) => t.ui(ui, ()),
            Tabs::Tables(t) => t.ui(ui, ()),
        };
    }

    fn title(&mut self, tab: &mut Self::Tab) -> egui::WidgetText {
        tab.title().into()
    }

    fn on_close(&mut self, _tab: &mut Self::Tab) -> bool {
        self.tabs_open > 1
    }

    fn tab_style_override(&self, tab: &Self::Tab, global_style: &TabStyle) -> Option<TabStyle> {
        tab.style_override(global_style)
    }
}
