//! TEV related types

use num_enum::TryFromPrimitive;
use serde::{Deserialize, Serialize};

pub mod alpha_compare;
pub mod order;
pub mod stage;
pub mod swap_mode;
pub mod swap_mode_table;

#[derive(Debug, Copy, Clone, Default, TryFromPrimitive, Serialize, Deserialize)]
#[repr(u8)]
pub enum TevStageId {
    #[default]
    TevStage0 = 0,
    TevStage1 = 1,
    TevStage2 = 2,
    TevStage3 = 3,
    TevStage4 = 4,
    TevStage5 = 5,
    TevStage6 = 6,
    TevStage7 = 7,
    TevStage8 = 8,
    TevStage9 = 9,
    TevStage10 = 10,
    TevStage11 = 11,
    TevStage12 = 12,
    TevStage13 = 13,
    TevStage14 = 14,
    TevStage15 = 15,
}

#[derive(Debug, Copy, Clone, Default, PartialEq, TryFromPrimitive, Serialize, Deserialize)]
#[repr(u8)]
pub enum TevOp {
    #[default]
    Add = 0,
    Sub = 1,
    CompR8Gt = 8,
    CompR8Eq = 9,
    CompGr16Gt = 10,
    CompGr16Eq = 11,
    CompBgr24Gt = 12,
    CompBgr24Eq = 13,
    CompRgb8OrA8Gt = 14,
    CompRgb8OrA8Eq = 15,
}

#[derive(Debug, Copy, Clone, Default, PartialEq, TryFromPrimitive, Serialize, Deserialize)]
#[repr(u8)]
pub enum TevBias {
    #[default]
    Zero = 0,
    AddHalf = 1,
    SubHalf = 2,
    // todo: used by various models, such as toad's body data\\scene\\dolpic6.szs\\kinopio\\kinopio_body.bmd
    Unknown = 3,
}

#[derive(Debug, Copy, Clone, Default, PartialEq, TryFromPrimitive, Serialize, Deserialize)]
#[repr(u8)]
pub enum TevScale {
    #[default]
    Scale1 = 0,
    Scale2 = 1,
    Scale4 = 2,
    Divide2 = 3,
}

#[derive(Debug, Copy, Clone, Default, PartialEq, TryFromPrimitive, Serialize, Deserialize)]
#[repr(u8)]
pub enum TevRegisterId {
    /// Use value from previous TEV stage (?)
    #[default]
    TevPrev = 0,
    TevReg0 = 1,
    TevReg1 = 2,
    TevReg2 = 3,
}

#[derive(Debug, Copy, Clone, Default, PartialEq, TryFromPrimitive, Serialize, Deserialize)]
#[repr(u8)]
pub enum CombineColorInput {
    /// Use Color Value from previous TEV stage
    ColorPrev = 0,
    /// Use Alpha Value from previous TEV stage
    AlphaPrev = 1,
    /// Use the Color Value from the Color/Output Register 0
    C0 = 2,
    /// Use the Alpha value from the Color/Output Register 0
    A0 = 3,
    /// Use the Color Value from the Color/Output Register 1
    C1 = 4,
    /// Use the Alpha value from the Color/Output Register 1
    A1 = 5,
    /// Use the Color Value from the Color/Output Register 2
    C2 = 6,
    /// Use the Alpha value from the Color/Output Register 2
    A2 = 7,
    /// Use the Color value from Texture
    #[default]
    TexColor = 8,
    /// Use the Alpha value from Texture
    TexAlpha = 9,
    /// Use the color value from rasterizer
    RasColor = 10,
    /// Use the alpha value from rasterizer
    RasAlpha = 11,
    One = 12,
    Half = 13,
    Konst = 14,
    Zero = 15,
}

#[derive(Debug, Copy, Clone, Default, PartialEq, TryFromPrimitive, Serialize, Deserialize)]
#[repr(u8)]
pub enum CombineAlphaInput {
    /// Use the Alpha value form the previous TEV stage
    AlphaPrev = 0,
    /// Use the Alpha value from the Color/Output Register 0
    A0 = 1,
    /// Use the Alpha value from the Color/Output Register 1
    A1 = 2,
    /// Use the Alpha value from the Color/Output Register 2
    A2 = 3,
    /// Use the Alpha value from the Texture
    TexAlpha = 4,
    /// Use the Alpha value from the rasterizer
    RasAlpha = 5,
    Konst = 6,
    #[default]
    Zero = 7,
}

#[derive(Debug, Copy, Clone, Default, PartialEq, TryFromPrimitive, Serialize, Deserialize)]
#[repr(u8)]
pub enum CompareKind {
    #[default]
    Never = 0,
    /// <
    #[serde(rename = "<")]
    Lt = 1,
    /// ==
    #[serde(rename = "==")]
    Eq = 2,
    /// <=
    #[serde(rename = "<=")]
    Le = 3,
    /// >
    #[serde(rename = ">")]
    Gt = 4,
    /// !=
    #[serde(rename = "!=")]
    Ne = 5,
    /// >=
    #[serde(rename = ">=")]
    Ge = 6,
    Always = 7,
}

#[derive(Debug, Copy, Clone, Default, PartialEq, TryFromPrimitive, Serialize, Deserialize)]
#[repr(u8)]
pub enum CompareOp {
    #[default]
    AND = 0,
    OR = 1,
    XOR = 2,
    XNOR = 3,
}
