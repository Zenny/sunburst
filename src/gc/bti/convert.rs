use image::{ColorType, Rgba};

use crate::{gc::types::bti::JUTTransparency, util::*};

#[derive(Debug, Copy, Clone)]
pub enum ColorData {
    Rgba([u8; 4]),
    Rgb([u8; 3]),
    Gray(u8),
    GrayAlpha([u8; 2]),
}

impl ColorData {
    pub fn conversion_type(&self) -> ColorType {
        match self {
            ColorData::Rgba(_) => ColorType::Rgba8,
            ColorData::Rgb(_) => ColorType::Rgb8,
            ColorData::Gray(_) => ColorType::L8,
            ColorData::GrayAlpha(_) => ColorType::La8,
        }
    }

    /// Ignore alpha and compare equality of rgb equivalents
    pub fn loose_eq(&self, other: &Self) -> bool {
        // ignore alpha
        let o = other.into_rgb();
        let us = self.into_rgb();

        us == o
    }

    pub fn from_rgb5a3(rgb5a3: u16) -> Self {
        // Format depends on the most significant bit. Two possible formats:
        // Top bit is 0: 0AAARRRRGGGGBBBB
        // Top bit is 1: 1RRRRRGGGGGBBBBB (Alpha set to 1)
        if rgb5a3 & 0x8000 == 0 {
            let a = (((rgb5a3 >> 12) & 0x7) as f32 * BIT3_TO_COLOR) * COLOR_TO_BYTE;
            let r = (((rgb5a3 >> 8) & 0xF) as f32 * BIT4_TO_COLOR) * COLOR_TO_BYTE;
            let g = (((rgb5a3 >> 4) & 0xF) as f32 * BIT4_TO_COLOR) * COLOR_TO_BYTE;
            let b = ((rgb5a3 & 0xF) as f32 * BIT4_TO_COLOR) * COLOR_TO_BYTE;
            ColorData::Rgba([r as u8, g as u8, b as u8, a as u8])
        } else {
            let r = (((rgb5a3 >> 10) & 0x1F) as f32 * BIT5_TO_COLOR) * COLOR_TO_BYTE;
            let g = (((rgb5a3 >> 5) & 0x1F) as f32 * BIT5_TO_COLOR) * COLOR_TO_BYTE;
            let b = ((rgb5a3 & 0x1F) as f32 * BIT5_TO_COLOR) * COLOR_TO_BYTE;
            ColorData::Rgba([r as u8, g as u8, b as u8, 255])
        }
    }

    pub fn to_rgb5a3(self, alpha: JUTTransparency) -> u16 {
        let [r, g, b, a] = self.into_rgba();
        // If we aren't planning to support alpha, we don't need it ever
        // todo support clip
        if a == 255 || alpha == JUTTransparency::Opaque {
            let [r, g, b] = self.into_rgb();
            let r = ((r as f32 * BYTE_TO_COLOR) * COLOR_TO_BIT5) as u16;
            let g = ((g as f32 * BYTE_TO_COLOR) * COLOR_TO_BIT5) as u16;
            let b = ((b as f32 * BYTE_TO_COLOR) * COLOR_TO_BIT5) as u16;
            let mut out = 0x8000;
            out |= r << 10;
            out |= g << 5;
            out |= b;
            out
        } else {
            let a = ((a as f32 * BYTE_TO_COLOR) * COLOR_TO_BIT3) as u16;
            let r = ((r as f32 * BYTE_TO_COLOR) * COLOR_TO_BIT4) as u16;
            let g = ((g as f32 * BYTE_TO_COLOR) * COLOR_TO_BIT4) as u16;
            let b = ((b as f32 * BYTE_TO_COLOR) * COLOR_TO_BIT4) as u16;
            let mut out = a << 12;
            out |= r << 8;
            out |= g << 4;
            out |= b;
            out
        }
    }

    pub fn from_rgb565(rgb565: u16) -> Self {
        // RRRR_RGGG_GGGB_BBBB
        let r = ((rgb565 >> 11) as f32 * BIT5_TO_COLOR) * COLOR_TO_BYTE;
        let g = (((rgb565 >> 5) & 0x3F) as f32 * BIT6_TO_COLOR) * COLOR_TO_BYTE;
        let b = ((rgb565 & 0x1F) as f32 * BIT5_TO_COLOR) * COLOR_TO_BYTE;
        ColorData::Rgb([r as u8, g as u8, b as u8])
    }

    pub fn to_rgb565(self) -> u16 {
        let [r, g, b] = self.into_rgb();

        let r = ((r as f32 * BYTE_TO_COLOR) * COLOR_TO_BIT5) as u16;
        let g = ((g as f32 * BYTE_TO_COLOR) * COLOR_TO_BIT6) as u16;
        let b = ((b as f32 * BYTE_TO_COLOR) * COLOR_TO_BIT5) as u16;

        let mut out = r << 11;
        out |= g << 5;
        out |= b;
        out
    }

    /// Grayscale
    /// Takes in 0x0F bits as a color and returns a grayscale byte (i8)
    pub fn from_i4(i4: u8) -> Self {
        // This format packs 2 colors into single bytes (0xAB) so here we accept one color (0x0A)
        // at a time and process it
        let i = convert_i8(i4);
        ColorData::Gray(i)
    }

    /// Returns 0x0F filled in with an approximation of self which is a single channel color,
    /// for colordatas of larger sizes, the red channel is taken
    /// Note that this is not compressed from 2 colors, you need to compress it after
    pub fn to_i4(self) -> u8 {
        convert_i4(self.get_i())
    }

    pub fn from_i8(i8: u8) -> Self {
        ColorData::Gray(i8)
    }

    pub fn to_i8(self) -> u8 {
        self.get_i()
    }

    pub fn from_ia4(ia4: u8) -> Self {
        // this format packs the grayscale i and a into one byte (0xAI)
        let i = ia4 & 0xF;
        let i8 = convert_i8(i);
        let a = convert_i8(ia4 >> 4);

        ColorData::GrayAlpha([i8, a])
    }

    /// Takes the grayscale channel and the A channel (i8) and converts them to ia4, for
    /// larger colordatas, it will take the red channel
    pub fn to_ia4(self) -> u8 {
        let (i, a) = self.get_ia();
        let i = convert_i4(i);
        let a = convert_i4(a);
        i | (a << 4)
    }

    /// Flips the bytes because they're already in the right format for grayscale alpha
    pub fn from_ia8(ia8: u16) -> Self {
        let i = ia8 & 0xFF;
        let a = (ia8 >> 8) & 0xFF;
        ColorData::GrayAlpha([i as u8, a as u8])
    }

    /// For larger colordatas, uses red channel
    pub fn to_ia8(self) -> [u8; 2] {
        let (i, a) = self.get_ia();
        // alpha goes first (?)
        [a, i]
    }

    /// Returns red or grayscale channel
    pub fn get_i(self) -> u8 {
        match self {
            ColorData::Rgba([r, _, _, _]) => r,
            ColorData::Rgb([r, _, _]) => r,
            ColorData::Gray(i) => i,
            ColorData::GrayAlpha([i, _]) => i,
        }
    }

    /// Does a best effort to get ia for grayalpha, using the red channel if necessary,
    /// and setting alpha to 255 if necessary
    pub fn get_ia(self) -> (u8, u8) {
        match self {
            ColorData::Rgba([r, _, _, a]) => (r, a),
            ColorData::Rgb([r, _, _]) => (r, 0xFF),
            ColorData::Gray(i) => (i, 0xFF),
            ColorData::GrayAlpha([i, a]) => (i, a),
        }
    }

    pub fn into_rgb(self) -> [u8; 3] {
        match self {
            ColorData::Rgba([r, g, b, _]) => [r, g, b],
            ColorData::Rgb(v) => v,
            ColorData::Gray(i) => [i, i, i],
            ColorData::GrayAlpha([i, _]) => [i, i, i],
        }
    }

    pub fn into_rgba(self) -> [u8; 4] {
        match self {
            ColorData::Rgba(v) => v,
            ColorData::Rgb([r, g, b]) => [r, g, b, 0xFF],
            ColorData::Gray(i) => [i, i, i, 0xFF],
            ColorData::GrayAlpha([i, a]) => [i, i, i, a],
        }
    }
}

impl From<Rgba<u8>> for ColorData {
    fn from(value: Rgba<u8>) -> Self {
        Self::Rgba(value.0)
    }
}

/// Convert a byte to i4 format (0x0F)
pub fn convert_i4(i: u8) -> u8 {
    ((i as f32 * BYTE_TO_COLOR) * COLOR_TO_BIT4) as u8
}

/// Converts an i4 format to i8
pub fn convert_i8(i: u8) -> u8 {
    ((i as f32 * BIT4_TO_COLOR) * COLOR_TO_BYTE) as u8
}

struct InterpData {
    /// \[0] = r \[1] = g \[2] = b
    pub c0: [u8; 3],
    /// \[0] = r \[1] = g \[2] = b
    pub c1: [u8; 3],
}

impl InterpData {
    pub fn new(rgb565_0: u16, rgb565_1: u16) -> Self {
        let c0 = ColorData::from_rgb565(rgb565_0).into_rgb();
        let c1 = ColorData::from_rgb565(rgb565_1).into_rgb();

        Self { c0, c1 }
    }
}

// todo there's more to understand about alpha here, see H_yoshi_main_cc_s3tc
/// Ported from https://github.com/LagoLunatic/wwrando with changes
pub fn get_interpolated_cmpr_colors_alpha(rgb565_0: u16, rgb565_1: u16) -> [ColorData; 4] {
    let info = InterpData::new(rgb565_0, rgb565_1);
    //let a0 = if rgb565_0 > rgb565_1 && rgb565_0 == 0 { 0 } else { 0xFF };
    //let a1 = if rgb565_0 > rgb565_1 && rgb565_1 == 0 { 0 } else { 0xFF };

    // The if here checks if we are worthy of alpha (Otherwise random other colors in the image
    // would be transparent)
    let (c2, c3) = if rgb565_0 > rgb565_1 {
        //let grayscale = rgb565_0 == 65535 && rgb565_1 == 0;
        (
            [
                (((2 * info.c0[0] as u16) + info.c1[0] as u16) / 3) as u8,
                (((2 * info.c0[1] as u16) + info.c1[1] as u16) / 3) as u8,
                (((2 * info.c0[2] as u16) + info.c1[2] as u16) / 3) as u8,
                //if grayscale { (((2 * a0 as u16) + a1 as u16) / 3) as u8 } else { 0xFF },
                0xFF,
            ],
            [
                ((info.c0[0] as u16 + (2 * info.c1[0] as u16)) / 3) as u8,
                ((info.c0[1] as u16 + (2 * info.c1[1] as u16)) / 3) as u8,
                ((info.c0[2] as u16 + (2 * info.c1[2] as u16)) / 3) as u8,
                //if grayscale { ((a0 as u16 + (2 * a1 as u16)) / 3) as u8 } else { 0xFF },
                0xFF,
            ],
        )
    } else {
        // We're at least worthy of alpha, so the last array element is always the alpha channel
        (
            [
                ((info.c0[0] as u16 + info.c1[0] as u16) / 2) as u8,
                ((info.c0[1] as u16 + info.c1[1] as u16) / 2) as u8,
                ((info.c0[2] as u16 + info.c1[2] as u16) / 2) as u8,
                //((a0 as u16 + a1 as u16) / 2) as u8,
                0xFF,
            ],
            [0, 0, 0, 0x00],
        )
    };

    [
        ColorData::Rgba([info.c0[0], info.c0[1], info.c0[2], 0xFF]), // a0
        ColorData::Rgba([info.c1[0], info.c1[1], info.c1[2], 0xFF]), // a1
        ColorData::Rgba(c2),
        ColorData::Rgba(c3),
    ]
}

/// Ported from https://github.com/LagoLunatic/wwrando with changes
pub fn get_interpolated_cmpr_colors(rgb565_0: u16, rgb565_1: u16) -> [ColorData; 4] {
    let info = InterpData::new(rgb565_0, rgb565_1);

    let (c2, c3) = if rgb565_0 > rgb565_1 {
        (
            [
                (((2 * info.c0[0] as u16) + info.c1[0] as u16) / 3) as u8,
                (((2 * info.c0[1] as u16) + info.c1[1] as u16) / 3) as u8,
                (((2 * info.c0[2] as u16) + info.c1[2] as u16) / 3) as u8,
            ],
            [
                ((info.c0[0] as u16 + (2 * info.c1[0] as u16)) / 3) as u8,
                ((info.c0[1] as u16 + (2 * info.c1[1] as u16)) / 3) as u8,
                ((info.c0[2] as u16 + (2 * info.c1[2] as u16)) / 3) as u8,
            ],
        )
    } else {
        (
            [
                ((info.c0[0] as u16 + info.c1[0] as u16) / 2) as u8,
                ((info.c0[1] as u16 + info.c1[1] as u16) / 2) as u8,
                ((info.c0[2] as u16 + info.c1[2] as u16) / 2) as u8,
            ],
            [
                ((info.c0[0] as u16 + (2 * info.c1[0] as u16)) / 3) as u8,
                ((info.c0[1] as u16 + (2 * info.c1[1] as u16)) / 3) as u8,
                ((info.c0[2] as u16 + (2 * info.c1[2] as u16)) / 3) as u8,
            ],
        )
    };

    [
        ColorData::Rgb(info.c0),
        ColorData::Rgb(info.c1),
        ColorData::Rgb(c2),
        ColorData::Rgb(c3),
    ]
}

pub fn get_nearest_color_index_fast(color: ColorData, colors: &[ColorData; 4]) -> usize {
    let [_, _, _, a] = color.into_rgba();

    if a < 16 {
        // Return the transparency color
        return 3;
    }

    let mut min_dist = i32::MAX;
    let mut pick = 0;

    unsafe {
        for i in 0..colors.len() {
            let dist =
                get_color_dist_fast(&color.into_rgba(), &colors.get_unchecked(i).into_rgba());

            // Same color so return
            if dist == 0 {
                return i;
            }

            if dist < min_dist {
                min_dist = dist;
                pick = i;
            }
        }
    }

    pick
}

/// Ported from https://github.com/LagoLunatic/PyFastBTI
pub fn get_color_dist_fast(c1: &[u8; 4], c2: &[u8; 4]) -> i32 {
    (c1[0] as i32 - c2[0] as i32).abs()
        + (c1[1] as i32 - c2[1] as i32).abs()
        + (c1[2] as i32 - c2[2] as i32).abs()
}

/// Ported from https://github.com/LagoLunatic/PyFastBTI but with some changes
///
/// Finds the most distant colors in a set. Perceptually this could have flaws, but it's simple
pub fn get_best_cmpr_key_colors(colors: Vec<[u8; 4]>) -> (ColorData, ColorData) {
    let mut max_dist = -1;
    // The values loaded here will be supplied if max_dist never changes, or if
    // we're all transparent (no colors supplied)
    let mut store1 = &[0, 0, 0, 0xFF];
    let mut store2 = &[0, 0, 0, 0xFF];

    'outer: for (i, c1) in colors.iter().enumerate() {
        for (j, c2) in colors.iter().enumerate() {
            if i == j {
                // no point comparing the same colors right?
                continue;
            }
            let dist = get_color_dist_fast(c1, c2);

            if dist > max_dist {
                store1 = c1;
                store2 = c2;
                max_dist = dist;

                // if our dist is this big we found black and white, no point in doing more
                if max_dist >= 765 {
                    break 'outer;
                }
            }
        }
    }

    // todo is pyfastbti's flag shuffling to check black/white really necessary here?
    (
        ColorData::Rgba(store1.clone()),
        ColorData::Rgba(store2.clone()),
    )
}
